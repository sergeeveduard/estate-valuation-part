package ru.domclick.valuation.authorization.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_auth.view.*
import kotlinx.android.synthetic.main.fragment_restore_password.*
import ru.domclick.core.extentions.*
import ru.domclick.valuation.authorization.ui.viewmodel.PhoneViewModel
import ru.domclick.valuation.authorization.ui.viewmodel.RestorePasswordViewModel
import ru.domclick.valuation.common.analytic.Events
import ru.domclick.valuation.common.analytic.Screens
import ru.domclick.valuation.common.di.scope.FragmentScope
import ru.domclick.valuation.common.ui.BaseFragment
import ru.domclick.valuation.common.ui.ErrorNotifier
import ru.domclick.valuation.lib.R
import javax.inject.Inject

@FragmentScope
class RestorePasswordFragment : BaseFragment() {

    @Inject
    lateinit var phoneViewModel: PhoneViewModel

    @Inject
    lateinit var restorePasswordViewModel: RestorePasswordViewModel

    private lateinit var navigation: AuthNavigation

    companion object {
        const val TAG = "RestorePasswordFragment"
        private const val ARG_PHONE = "arg_phone"

        fun newInstance(phone: String): RestorePasswordFragment {
            return RestorePasswordFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PHONE, phone)
                }
            }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        navigation = context as AuthNavigation
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_restore_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        analytics.logScreen(activity!!, Screens.RESTORE_PASSWORD)

        toolbar?.run {
            setTitle(R.string.auth_restore_password_title)
            setNavigationIcon(R.drawable.ic_back_ab)
            setNavigationOnClickListener { navigation.gotoBack() }
        }

        fragmentRestorePasswordSendCodeButton.setOnClickListener(::onSendCodeButtonClicked)

        fragmentRestorePasswordPhoneText.addOnTextChanged {
            phoneViewModel.onTextChanged(fragmentRestorePasswordTextInputLayoutPhone.getRawPhoneNumber())
        }
        fragmentRestorePasswordPhoneText.setOnFocusChangeListener { _, hasFocus ->
            phoneViewModel.onFocusChanged(hasFocus)
        }

        phoneViewModel.error
                .subscribe { fragmentRestorePasswordTextInputLayoutPhone.error = it }
                .disposeOnDestroyView(this)

        restorePasswordViewModel.isLoading
                .subscribe { loadingView.visibility = if (it) View.VISIBLE else View.GONE }
                .disposeOnDestroyView(this)

        restorePasswordViewModel.onWrongPhone
                .subscribe { showSnackbar(activityView!!.root, R.string.auth_error_restore_password) }
                .disposeOnDestroyView(this)

        restorePasswordViewModel.onError
                .subscribe { ErrorNotifier.showError(activityView!!.root, it) }
                .disposeOnDestroyView(this)

        restorePasswordViewModel.onSuccess
                .subscribe {
                    navigation.gotoSmsCode(
                            fragmentRestorePasswordTextInputLayoutPhone.getRawPhoneNumber(),
                            fragmentRestorePasswordTextInputLayoutPhone.editText!!.text.toString()
                    )
                }
                .disposeOnDestroyView(this)

        fragmentRestorePasswordPhoneText.setText(arguments!!.getString(ARG_PHONE))
    }

    private fun onSendCodeButtonClicked(button: View) {
        analytics.logEvent(Events.CLICK_SEND_SMS_CODE)
        button.hideKeyboard()

        if (phoneViewModel.validate()) {
            restorePasswordViewModel.restore(fragmentRestorePasswordTextInputLayoutPhone.getRawPhoneNumber())
        }
    }
}