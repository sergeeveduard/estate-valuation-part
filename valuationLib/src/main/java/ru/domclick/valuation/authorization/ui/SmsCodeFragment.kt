package ru.domclick.valuation.authorization.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_auth.view.*
import kotlinx.android.synthetic.main.fragment_sms_code.*
import ru.domclick.core.extentions.activityView
import ru.domclick.core.extentions.disposeOnDestroyView
import ru.domclick.core.ui.widget.SmsCodeView
import ru.domclick.valuation.authorization.ui.viewmodel.SmsCodeViewModel
import ru.domclick.valuation.common.analytic.Screens
import ru.domclick.valuation.common.ui.BaseFragment
import ru.domclick.valuation.common.ui.ErrorNotifier
import ru.domclick.valuation.lib.R
import javax.inject.Inject

class SmsCodeFragment : BaseFragment() {

    @Inject
    lateinit var smsCodeViewModel: SmsCodeViewModel

    private val rawPhoneNumber: String by lazy { arguments!!.getString(ARG_RAW_PHONE_NUMBER) }
    private lateinit var navigation: AuthNavigation

    companion object {
        const val TAG = "SmsCodeFragment"
        private const val ARG_RAW_PHONE_NUMBER = "arg_raw_phone_number"
        private const val ARG_FORMATTED_PHONE_NUMBER = "arg_formatted_phone_number"

        fun newInstance(rawPhoneNumber: String, formattedPhoneNumber: String): SmsCodeFragment {
            return SmsCodeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_RAW_PHONE_NUMBER, rawPhoneNumber)
                    putString(ARG_FORMATTED_PHONE_NUMBER, formattedPhoneNumber)
                }
            }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        navigation = context as AuthNavigation
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sms_code, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        analytics.logScreen(activity!!, Screens.RESTORE_PASSWORD_SMS_CODE)

        toolbar?.run {
            setTitle(R.string.auth_sms_code_title)
            setNavigationIcon(R.drawable.ic_back_ab)
            setNavigationOnClickListener { navigation.gotoBack() }
        }

        fragmentSmsCodeInput.requestFocus()
        fragmentSmsCodeHint.text = getString(R.string.auth_sms_code_hint, arguments!!.getString(ARG_FORMATTED_PHONE_NUMBER))

        fragmentSmsCodeInput.setOnCodeCompleteListener(onCodeChanged)
        fragmentSmsCodeSendAgainButton.setOnClickListener { smsCodeViewModel.sendCode(rawPhoneNumber) }

        smsCodeViewModel.waiting
                .subscribe { updateSendCodeAgainText(it) }
                .disposeOnDestroyView(this)

        smsCodeViewModel.isLoading
                .subscribe { fragmentSmsCodeLoading.visibility = if (it) View.VISIBLE else View.GONE }
                .disposeOnDestroyView(this)

        smsCodeViewModel.onError
                .subscribe { ErrorNotifier.showError(activityView!!.root, it) }
                .disposeOnDestroyView(this)

        smsCodeViewModel.onWrongCode
                .subscribe { fragmentSmsCodeInput.showErrorCode(true) }
                .disposeOnDestroyView(this)

        smsCodeViewModel.onValidCode
                .subscribe { navigation.gotoCreateNewPassword(rawPhoneNumber, it) }
                .disposeOnDestroyView(this)

        smsCodeViewModel.onFragmentStart()
    }

    override fun onResume() {
        super.onResume()
        fragmentSmsCodeInput.showSoftKeyboard()
    }

    private fun updateSendCodeAgainText(waiting: Long) {
        fragmentSmsCodeSendAgainButton.visibility = View.VISIBLE
        if (waiting > 0) {
            fragmentSmsCodeSendAgainButton.text = getString(R.string.auth_sms_code_send_again_in, waiting)
            fragmentSmsCodeSendAgainButton.alpha = 0.5f
        } else {
            fragmentSmsCodeSendAgainButton.text = getString(R.string.auth_sms_code_send_again)
            fragmentSmsCodeSendAgainButton.alpha = 1f
        }
    }

    private val onCodeChanged = object : SmsCodeView.OnCodeChangeListener() {
        override fun OnCodeComplete(isComplete: Boolean) {
            if (isComplete) {
                smsCodeViewModel.checkCode(rawPhoneNumber, fragmentSmsCodeInput.text)
            }
        }
    }
}