package ru.domclick.valuation.authorization.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_auth.view.*
import kotlinx.android.synthetic.main.fragment_login.*
import ru.domclick.core.extentions.*
import ru.domclick.valuation.authorization.ui.viewmodel.HostViewModel
import ru.domclick.valuation.authorization.ui.viewmodel.LoginViewModel
import ru.domclick.valuation.authorization.ui.viewmodel.PasswordViewModel
import ru.domclick.valuation.authorization.ui.viewmodel.PhoneViewModel
import ru.domclick.valuation.common.analytic.Events
import ru.domclick.valuation.common.analytic.Screens
import ru.domclick.valuation.common.ui.BaseFragment
import ru.domclick.valuation.common.ui.ErrorNotifier
import ru.domclick.valuation.lib.R
import javax.inject.Inject

class LoginFragment : BaseFragment() {

    @Inject
    lateinit var loginViewModel: LoginViewModel

    @Inject
    lateinit var phoneViewModel: PhoneViewModel

    @Inject
    lateinit var passwordViewModel: PasswordViewModel

    @Inject
    lateinit var hostViewModel: HostViewModel

    override val shouldShowToolbar: Boolean get() = false

    private lateinit var navigation: AuthNavigation

    companion object {
        const val TAG = "LoginFragment"

        fun newInstance(): LoginFragment = LoginFragment()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        navigation = context as AuthNavigation
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        analytics.logScreen(activity!!, Screens.LOGIN)

        initHostSpinner()
        fragmentLoginButtonLogin.setOnClickListener(::onLoginButtonClicked)
        fragmentLoginForgetPassword.setOnClickListener(::onForgetPasswordClicked)

        fragmentLoginPhoneText.addOnTextChanged {
            phoneViewModel.onTextChanged(fragmentLoginTextInputLayoutPhone.getRawPhoneNumber())
        }
        fragmentLoginPhoneText.setOnFocusChangeListener { _, hasFocus ->
            phoneViewModel.onFocusChanged(hasFocus)
        }

        fragmentLoginPasswordText.addOnTextChanged(passwordViewModel::onTextChanged)
        fragmentLoginPasswordText.setOnFocusChangeListener { _, hasFocus ->
            passwordViewModel.onFocusChanged(hasFocus)
        }

        loginViewModel.isLoading
                .subscribe { loadingView.visibility = if (it) View.VISIBLE else View.GONE }
                .disposeOnDestroyView(this)

        loginViewModel.onAuthorized
                .subscribe { navigation.gotoStartActivity() }
                .disposeOnDestroyView(this)

        loginViewModel.onLoginFailed
                .subscribe { showSnackbar(activityView!!.root, R.string.auth_error_wrong_login_or_password) }
                .disposeOnDestroyView(this)

        loginViewModel.onError
                .subscribe { ErrorNotifier.showError(activityView!!.root, it) }
                .disposeOnDestroyView(this)

        phoneViewModel.error
                .subscribe { fragmentLoginTextInputLayoutPhone.error = it }
                .disposeOnDestroyView(this)

        passwordViewModel.error
                .subscribe { fragmentLoginTextInputLayoutPassword.error = it }
                .disposeOnDestroyView(this)
    }

    private fun onForgetPasswordClicked(button: View) {
        analytics.logEvent(Events.CLICK_FORGET_PWD)
        navigation.gotoRecoverPassword(fragmentLoginPhoneText.text.toString())
    }

    private fun onLoginButtonClicked(button: View) {
        analytics.logEvent(Events.CLICK_LOGIN)
        button.hideKeyboard()

        val validates = listOf(passwordViewModel.validate(), phoneViewModel.validate())

        if (!validates.contains(false)) {
            val phone = fragmentLoginTextInputLayoutPhone.getRawPhoneNumber()
            val password = fragmentLoginTextInputLayoutPassword.editText!!.text.toString()

            loginViewModel.authorize(phone, password)
        }
    }

    private fun initHostSpinner() {
        fragmentLoginHostSpinner.visibility = if (hostViewModel.showSpinner) View.VISIBLE else View.GONE

        if (hostViewModel.showSpinner) {
            fragmentLoginHostSpinner.adapter = ArrayAdapter<String>(
                    context!!,
                    android.R.layout.simple_spinner_item,
                    hostViewModel.hostAliasList
            )

            fragmentLoginHostSpinner.setSelection(hostViewModel.selectedIndex, true)

            fragmentLoginHostSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    hostViewModel.onHostSelected(position)
                }
            }
        }
    }
}