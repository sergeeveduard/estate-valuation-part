package ru.domclick.valuation.authorization.ui.viewmodel

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import ru.domclick.core.extentions.RxNothing
import ru.domclick.core.extentions.addTo
import ru.domclick.valuation.authorization.domain.CreatePasswordUseCase
import ru.domclick.valuation.common.di.scope.FragmentScope
import javax.inject.Inject

@FragmentScope
class CreatePasswordViewModel
@Inject constructor(private val createPasswordUseCase: CreatePasswordUseCase) {

    val isLoading = BehaviorSubject.createDefault(false)!!
    val onSuccess = PublishSubject.create<RxNothing>()!!
    val onError = PublishSubject.create<Throwable>()!!

    private val disposable = CompositeDisposable()

    fun createPassword(phoneNumber: String,
                       ticket: String,
                       password1: String,
                       password2: String) {

        disposable.clear()
        isLoading.onNext(true)
        createPasswordUseCase.execute(phoneNumber, ticket, password1, password2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { isLoading.onNext(false) }
                .subscribe({ onSuccess.onNext(RxNothing) }, { onError.onNext(it) })
                .addTo(disposable)
    }
}