package ru.domclick.valuation.authorization.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_auth.view.*
import kotlinx.android.synthetic.main.fragment_create_new_password.*
import ru.domclick.core.extentions.activityView
import ru.domclick.core.extentions.addOnTextChanged
import ru.domclick.core.extentions.disposeOnDestroyView
import ru.domclick.core.extentions.hideKeyboard
import ru.domclick.valuation.authorization.ui.viewmodel.ConfirmPasswordViewModel
import ru.domclick.valuation.authorization.ui.viewmodel.CreatePasswordViewModel
import ru.domclick.valuation.authorization.ui.viewmodel.PasswordViewModel
import ru.domclick.valuation.common.analytic.Screens
import ru.domclick.valuation.common.ui.BaseFragment
import ru.domclick.valuation.common.ui.ErrorNotifier
import ru.domclick.valuation.lib.R
import javax.inject.Inject

class CreateNewPasswordFragment : BaseFragment() {

    @Inject
    lateinit var passwordViewModel: PasswordViewModel

    @Inject
    lateinit var confirmPasswordViewModel: ConfirmPasswordViewModel

    @Inject
    lateinit var createPasswordViewModel: CreatePasswordViewModel

    private lateinit var navigation: AuthNavigation

    companion object {
        const val TAG = "CreateNewPasswordFragment"
        private const val ARG_RAW_PHONE_NUMBER = "arg_raw_phone_number"
        private const val ARG_TICKET = "arg_ticket"

        fun newInstance(rawPhoneNumber: String, ticket: String): CreateNewPasswordFragment {
            return CreateNewPasswordFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_RAW_PHONE_NUMBER, rawPhoneNumber)
                    putString(ARG_TICKET, ticket)
                }
            }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        navigation = context as AuthNavigation
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_create_new_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        analytics.logScreen(activity!!, Screens.RESTORE_PASSWORD_NEW_PASSWORD)

        initToolbar()

        fragmentCreateNewPasswordTextInputLayoutPassword1.helperText = getString(R.string.auth_create_new_pwd_hint_1)
        fragmentCreateNewPasswordTextInputLayoutPassword2.helperText = getString(R.string.auth_create_new_pwd_hint_2)
        fragmentCreateNewPasswordButton.setOnClickListener(::onCreatePasswordButtonClicked)

        fragmentCreateNewPasswordText1.addOnTextChanged {
            passwordViewModel.onTextChanged(it)
            confirmPasswordViewModel.firstPasswordValue = it
        }
        fragmentCreateNewPasswordText1.setOnFocusChangeListener { _, hasFocus ->
            passwordViewModel.onFocusChanged(hasFocus)
        }

        fragmentCreateNewPasswordText2.addOnTextChanged(confirmPasswordViewModel::onTextChanged)
        fragmentCreateNewPasswordText2.setOnFocusChangeListener { _, hasFocus ->
            confirmPasswordViewModel.onFocusChanged(hasFocus)
        }

        passwordViewModel.setMinimumLength(8)
        passwordViewModel.error
                .subscribe { fragmentCreateNewPasswordTextInputLayoutPassword1.error = it }
                .disposeOnDestroyView(this)

        confirmPasswordViewModel.error
                .subscribe { fragmentCreateNewPasswordTextInputLayoutPassword2.error = it }
                .disposeOnDestroyView(this)

        createPasswordViewModel.isLoading
                .subscribe { loadingView.visibility = if (it) View.VISIBLE else View.GONE }
                .disposeOnDestroyView(this)

        createPasswordViewModel.onError
                .subscribe { ErrorNotifier.showError(activityView!!.root, it) }
                .disposeOnDestroyView(this)

        createPasswordViewModel.onSuccess
                .subscribe { onSuccess() }
                .disposeOnDestroyView(this)
    }

    private fun initToolbar() {
        toolbar?.run {
            setTitle(R.string.auth_create_new_pwd_title)
            setNavigationIcon(R.drawable.ic_back_ab)
            setNavigationOnClickListener { navigation.gotoBack() }
        }
    }

    private fun onCreatePasswordButtonClicked(button: View) {
        button.hideKeyboard()

        val validates = listOf(passwordViewModel.validate(), confirmPasswordViewModel.validate())

        if (!validates.contains(false)) {
            val phone = arguments!!.getString(ARG_RAW_PHONE_NUMBER)!!
            val ticket = arguments!!.getString(ARG_TICKET)!!
            val password1 = fragmentCreateNewPasswordText1.text.toString()
            val password2 = fragmentCreateNewPasswordText2.text.toString()

            createPasswordViewModel.createPassword(phone, ticket, password1, password2)
        }
    }

    private fun onSuccess() {
        AlertDialog.Builder(activity!!)
                .setTitle(R.string.auth_create_new_pwd_success_title)
                .setMessage(R.string.auth_create_new_pwd_success_message)
                .setPositiveButton(R.string.ok) { _, _ -> navigation.gotoLogin() }
                .create()
                .show()
    }
}