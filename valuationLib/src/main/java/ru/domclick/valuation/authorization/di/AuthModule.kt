package ru.domclick.valuation.authorization.di

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import ru.domclick.core.di.qualifier.NoCas
import ru.domclick.valuation.authorization.data.api.AuthApi
import ru.domclick.valuation.authorization.data.api.PasswordApi
import javax.inject.Singleton

@Module
class AuthModule {

    @Provides
    @Singleton
    internal fun provideAuthApi(@NoCas builder: Retrofit.Builder): AuthApi {
        return builder.build().create(AuthApi::class.java)
    }

    @Provides
    @Singleton
    internal fun providePasswordApi(@NoCas builder: Retrofit.Builder): PasswordApi {
        return builder.build().create(PasswordApi::class.java)
    }
}