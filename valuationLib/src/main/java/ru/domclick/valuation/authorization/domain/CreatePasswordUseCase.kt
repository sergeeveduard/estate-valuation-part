package ru.domclick.valuation.authorization.domain

import io.reactivex.Completable
import ru.domclick.valuation.authorization.data.api.PasswordApi
import ru.domclick.valuation.authorization.data.dto.ChangePasswordRequestDto
import ru.domclick.valuation.common.data.converter.ErrorResponseHandler
import ru.domclick.valuation.common.di.scope.FragmentScope
import javax.inject.Inject

@FragmentScope
class CreatePasswordUseCase
@Inject constructor(private val passwordApi: PasswordApi,
                    private val errorResponseHandler: ErrorResponseHandler) {

    fun execute(phoneNumber: String,
                ticket: String,
                password1: String,
                password2: String): Completable {

        return passwordApi.changePassword(ChangePasswordRequestDto(phoneNumber, ticket, password1, password2))
                .map { errorResponseHandler.handle(it) }
                .toCompletable()
    }
}