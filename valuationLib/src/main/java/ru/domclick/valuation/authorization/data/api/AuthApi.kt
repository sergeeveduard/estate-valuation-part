package ru.domclick.valuation.authorization.data.api

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import ru.domclick.valuation.authorization.data.dto.AuthorizeRequestDto
import ru.domclick.valuation.authorization.data.dto.AuthorizeResponseDto

interface AuthApi {

    @POST("api/v1/auth/")
    fun authorize(@Body body: AuthorizeRequestDto): Single<Response<AuthorizeResponseDto>>
}