package ru.domclick.valuation.authorization.ui.viewmodel

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import ru.domclick.core.extentions.RxNothing
import ru.domclick.core.extentions.addTo
import ru.domclick.valuation.authorization.domain.RestorePasswordUseCase
import ru.domclick.valuation.common.di.scope.FragmentScope
import javax.inject.Inject

@FragmentScope
class RestorePasswordViewModel
@Inject constructor(private val restorePasswordUseCase: RestorePasswordUseCase) {

    val isLoading = BehaviorSubject.createDefault(false)!!
    val onSuccess = PublishSubject.create<RxNothing>()!!
    val onWrongPhone = PublishSubject.create<RxNothing>()!!
    val onError = PublishSubject.create<Throwable>()!!

    private val disposable = CompositeDisposable()

    fun restore(phone: String) {
        disposable.clear()
        isLoading.onNext(true)
        restorePasswordUseCase
                .execute(phone)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { isLoading.onNext(false) }
                .subscribe({ onSuccess.onNext(RxNothing) }, ::onError)
                .addTo(disposable)
    }

    private fun onError(e: Throwable) {
        if (e is HttpException && e.code() in 400..499) onWrongPhone.onNext(RxNothing)
        else onError.onNext(e)
    }
}