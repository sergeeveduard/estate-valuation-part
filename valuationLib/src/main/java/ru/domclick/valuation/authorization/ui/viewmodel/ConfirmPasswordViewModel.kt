package ru.domclick.valuation.authorization.ui.viewmodel

import android.content.Context
import ru.domclick.core.extentions.empty
import ru.domclick.valuation.common.di.scope.FragmentScope
import ru.domclick.valuation.common.ui.viewmodel.ValidateTextViewModel
import ru.domclick.valuation.lib.R
import javax.inject.Inject

@FragmentScope
class ConfirmPasswordViewModel
@Inject constructor(private val context: Context) : ValidateTextViewModel() {

    var firstPasswordValue: String = String.empty()

    override fun validate(text: String): Boolean {
        val isValid = text == firstPasswordValue

        val message = if (isValid) {
            String.empty()
        } else {
            context.getString(R.string.auth_create_new_pwd_confirm_error_diffs)
        }
        error.onNext(message)

        return isValid
    }
}