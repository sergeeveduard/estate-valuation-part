package ru.domclick.valuation.authorization.ui.viewmodel

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import ru.domclick.core.extentions.RxNothing
import ru.domclick.valuation.authorization.domain.CheckSmsCodeUseCase
import ru.domclick.valuation.authorization.domain.RestorePasswordUseCase
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

private val WAIT = TimeUnit.SECONDS.toMillis(60)

@Singleton
class SmsCodeViewModel
@Inject constructor(private val checkSmsCodeUseCase: CheckSmsCodeUseCase,
                    private val restorePasswordUseCase: RestorePasswordUseCase) {

    val waiting = BehaviorSubject.createDefault(0L)!!
    val isLoading = BehaviorSubject.createDefault(false)!!
    val onWrongCode = PublishSubject.create<RxNothing>()!!
    val onValidCode = PublishSubject.create<String>()!!
    val onError = PublishSubject.create<Throwable>()!!

    private var prevSendCodeTime = 0L
    private var intervalSubscriber: Disposable? = null
    private var checkCodeDisposable: Disposable? = null
    private var sendCodeDisposable: Disposable? = null

    fun onFragmentStart() {
        if (prevSendCodeTime == 0L) {//first start
            setTimer()
        }
    }

    fun sendCode(phoneNumber: String) {
        if (System.currentTimeMillis() < prevSendCodeTime + WAIT) {
            return
        }
        isLoading.onNext(true)
        sendCodeDisposable?.dispose()
        sendCodeDisposable = restorePasswordUseCase.execute(phoneNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { isLoading.onNext(false) }
                .subscribe({ setTimer() }, onError::onNext)
    }

    fun checkCode(phoneNumber: String, code: String) {
        isLoading.onNext(true)
        checkCodeDisposable?.dispose()
        checkCodeDisposable = checkSmsCodeUseCase.execute(phoneNumber, code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { isLoading.onNext(false) }
                .subscribe(onValidCode::onNext, ::onError)
    }

    private fun setTimer() {
        prevSendCodeTime = System.currentTimeMillis()
        intervalSubscriber?.dispose()
        intervalSubscriber = Observable.interval(1, TimeUnit.SECONDS)
                .takeWhile { it <= WAIT }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    notifyWaiting()
                }
    }

    private fun notifyWaiting() {
        val seconds = TimeUnit.MILLISECONDS.toSeconds(WAIT - System.currentTimeMillis() + prevSendCodeTime)
        waiting.onNext(Math.max(seconds, 0))
    }

    private fun onError(e: Throwable) {
        if (e is HttpException && e.code() == 400) {
            onWrongCode.onNext(RxNothing)
        } else {
            onError.onNext(e)
        }
    }
}