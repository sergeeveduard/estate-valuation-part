package ru.domclick.valuation.authorization.ui

interface AuthNavigation {

    fun gotoStartActivity()

    fun gotoLogin()

    fun gotoRecoverPassword(phoneNumber: String)

    fun gotoSmsCode(rawPhoneNumber: String, formattedPhoneNumber: String)

    fun gotoCreateNewPassword(rawPhoneNumber: String, ticket: String)

    fun gotoBack()
}