package ru.domclick.valuation.authorization.domain

import io.reactivex.Single
import ru.domclick.valuation.authorization.data.service.AuthorizeService
import ru.domclick.valuation.authorization.domain.model.AuthorizedUser
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthorizeUseCase
@Inject constructor(private val authorizeService: AuthorizeService) {

    fun execute(phone: String, password: String): Single<AuthorizedUser> {
        return authorizeService.authorize(phone, password)
                .map(UserConverter::convert)
    }
}