package ru.domclick.valuation.authorization.ui.viewmodel

import android.content.Context
import ru.domclick.core.extentions.empty
import ru.domclick.valuation.common.di.scope.FragmentScope
import ru.domclick.valuation.common.ui.viewmodel.ValidateTextViewModel
import ru.domclick.valuation.lib.R
import javax.inject.Inject

private const val MINIMUM_LENGTH = 4

@FragmentScope
class PasswordViewModel
@Inject constructor(private val context: Context) : ValidateTextViewModel() {

    private var minimumLength = MINIMUM_LENGTH

    override fun validate(text: String): Boolean {
        val isValid = text.length >= minimumLength

        val message = if (isValid) {
            String.empty()
        } else {
            context.getString(R.string.auth_error_invalid_password, minimumLength)
        }
        error.onNext(message)

        return isValid
    }

    fun setMinimumLength(length: Int) {
        this.minimumLength = length
    }
}