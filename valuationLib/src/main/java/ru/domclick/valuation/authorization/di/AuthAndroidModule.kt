package ru.domclick.valuation.authorization.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.domclick.valuation.authorization.ui.*
import ru.domclick.valuation.common.di.scope.ActivityScope
import ru.domclick.valuation.common.di.scope.FragmentScope

@Module
abstract class AuthActivityModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [AuthFragmentModule::class])
    abstract fun contributeAuthActivity(): AuthActivity
}

@Module
abstract class AuthFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): LoginFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeRecoverPasswordFragment(): RestorePasswordFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeSmsCodeFragment(): SmsCodeFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeCreateNewPasswordFragment(): CreateNewPasswordFragment
}