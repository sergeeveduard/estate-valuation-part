package ru.domclick.valuation.authorization.domain

import io.reactivex.Single
import ru.domclick.valuation.authorization.data.api.PasswordApi
import ru.domclick.valuation.authorization.data.dto.CheckSmsCodeRequestDto
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CheckSmsCodeUseCase
@Inject constructor(private val passwordApi: PasswordApi) {

    fun execute(phoneNumber: String, code: String): Single<String> {
        return passwordApi.checkSmsCode(CheckSmsCodeRequestDto(phoneNumber, code))
                .map { it.result!!.ticket }
    }
}