package ru.domclick.valuation.authorization.domain

import io.reactivex.Completable
import ru.domclick.valuation.authorization.data.api.PasswordApi
import ru.domclick.valuation.authorization.data.dto.RestorePasswordRequestDto
import ru.domclick.valuation.common.data.converter.ErrorResponseHandler
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RestorePasswordUseCase
@Inject constructor(private val passwordApi: PasswordApi,
                    private val errorResponseHandler: ErrorResponseHandler) {

    fun execute(phone: String): Completable {
        return passwordApi.restorePassword(RestorePasswordRequestDto(phone))
                .map { errorResponseHandler.handle(it) }
                .toCompletable()
    }
}