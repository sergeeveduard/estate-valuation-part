package ru.domclick.valuation.authorization.data.service

import io.reactivex.Completable
import ru.domclick.valuation.common.data.repo.AccountRepo
import ru.domclick.valuation.common.data.repo.AppPrefs
import ru.domclick.valuation.database.data.DataBase
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LogoutService
@Inject constructor(private val accountRepo: AccountRepo,
                    private val dataBase: DataBase,
                    private val prefs: AppPrefs) {

    fun logout(): Completable {
        return Completable.fromAction {
            dataBase.clearAllTables()
            accountRepo.logout()
            prefs.setCatalogLastUpdate(0)
        }
    }
}