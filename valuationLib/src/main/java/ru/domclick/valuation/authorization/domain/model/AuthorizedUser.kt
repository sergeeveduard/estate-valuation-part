package ru.domclick.valuation.authorization.domain.model

import java.io.Serializable

class AuthorizedUser(
        val id: Long
) : Serializable