package ru.domclick.valuation.authorization.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import ru.domclick.core.extentions.replaceFragment
import ru.domclick.valuation.common.ui.BaseActivity
import ru.domclick.valuation.lib.R

class AuthActivity : BaseActivity(), AuthNavigation {

    companion object {
        fun newIntent(context: Context): Intent = Intent(context, AuthActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        if (savedInstanceState == null) {
            gotoLogin()
        }
    }

    override fun gotoStartActivity() {
        throw NotImplementedError()
    }

    override fun gotoLogin() {
        supportFragmentManager.popBackStack(null, androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE)
        replaceFragment(LoginFragment.newInstance(), LoginFragment.TAG, addBackStack = false)
    }

    override fun gotoRecoverPassword(phoneNumber: String) {
        replaceFragment(RestorePasswordFragment.newInstance(phoneNumber), RestorePasswordFragment.TAG, useSlideAnimation = true)
    }

    override fun gotoSmsCode(rawPhoneNumber: String, formattedPhoneNumber: String) {
        replaceFragment(SmsCodeFragment.newInstance(rawPhoneNumber, formattedPhoneNumber), SmsCodeFragment.TAG, useSlideAnimation = true)
    }

    override fun gotoCreateNewPassword(rawPhoneNumber: String, ticket: String) {
        replaceFragment(CreateNewPasswordFragment.newInstance(rawPhoneNumber, ticket), CreateNewPasswordFragment.TAG, useSlideAnimation = true)
    }

    override fun gotoBack() {
        onBackPressed()
    }
}
