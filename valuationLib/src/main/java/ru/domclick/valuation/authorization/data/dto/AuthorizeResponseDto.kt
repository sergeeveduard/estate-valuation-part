package ru.domclick.valuation.authorization.data.dto

import ru.domclick.valuation.common.data.dto.ResponseDto

class AuthorizeResponseDto : ResponseDto<AuthorizeResultDto>()

data class AuthorizeResultDto(
        val id: Long,
        val lastName: String,
        val middleName: String,
        val firstName: String,
        val active: Boolean,
        val casId: Long
)