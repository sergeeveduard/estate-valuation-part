package ru.domclick.valuation.authorization.data.dto

class AuthorizeRequestDto(
        val username: String,
        val password: String,
        val backend: String = "cas"
)