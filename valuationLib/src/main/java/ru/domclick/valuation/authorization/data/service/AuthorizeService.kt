package ru.domclick.valuation.authorization.data.service

import io.reactivex.Single
import retrofit2.HttpException
import retrofit2.Response
import ru.domclick.valuation.authorization.data.api.AuthApi
import ru.domclick.valuation.authorization.data.dto.AuthorizeRequestDto
import ru.domclick.valuation.authorization.data.dto.AuthorizeResponseDto
import ru.domclick.valuation.common.analytic.Analytics
import ru.domclick.valuation.common.data.repo.AccountRepo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthorizeService
@Inject constructor(private val authApi: AuthApi,
                    private val accountRepo: AccountRepo,
                    private val analytics: Analytics) {

    private val sessionPattern = Regex("(?<=session_id=)(.+)(?=;)")

    fun authorize(phone: String, password: String): Single<AuthorizeResponseDto> {
        return authApi
                .authorize(AuthorizeRequestDto(phone, password))
                .map(::checkResponse)
                .map { extractHeader(phone, password, it) }
    }

    private fun checkResponse(response: Response<AuthorizeResponseDto>): Response<AuthorizeResponseDto> {
        if (response.code() !in 200..299) {
            throw HttpException(response)
        }
        return response
    }

    private fun extractHeader(phone: String, password: String, response: Response<AuthorizeResponseDto>): AuthorizeResponseDto {
        val headers = response.headers().toString()
        val sessionId = getSessionId(headers)
        if (!sessionId.isNullOrEmpty()) {
            accountRepo.putAccount(phone, password, sessionId!!)
        }

        val body = response.body()!!
        analytics.setCurrentUser(body.result!!.id)

        return body
    }

    private fun getSessionId(headers: String): String? = sessionPattern.find(headers)?.value
}