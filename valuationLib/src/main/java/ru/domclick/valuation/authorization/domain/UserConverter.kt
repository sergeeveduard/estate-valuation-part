package ru.domclick.valuation.authorization.domain

import ru.domclick.valuation.authorization.data.dto.AuthorizeResponseDto
import ru.domclick.valuation.authorization.domain.model.AuthorizedUser

object UserConverter {

    fun convert(response: AuthorizeResponseDto): AuthorizedUser {

        return with(response.result!!) {
            AuthorizedUser(
                    id
            )
        }
    }
}