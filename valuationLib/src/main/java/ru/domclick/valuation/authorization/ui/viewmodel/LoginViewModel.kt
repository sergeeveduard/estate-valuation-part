package ru.domclick.valuation.authorization.ui.viewmodel

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import ru.domclick.core.extentions.RxNothing
import ru.domclick.core.extentions.addTo
import ru.domclick.valuation.authorization.domain.AuthorizeUseCase
import ru.domclick.valuation.authorization.domain.model.AuthorizedUser
import ru.domclick.valuation.common.di.scope.FragmentScope
import javax.inject.Inject

@FragmentScope
class LoginViewModel
@Inject constructor(private val authorizeUseCase: AuthorizeUseCase) {

    val isLoading = BehaviorSubject.createDefault(false)!!
    val onAuthorized = PublishSubject.create<AuthorizedUser>()!!
    val onLoginFailed = PublishSubject.create<RxNothing>()!!
    val onError = PublishSubject.create<Throwable>()!!

    private val disposable = CompositeDisposable()

    fun authorize(phone: String, password: String) {
        disposable.clear()
        isLoading.onNext(true)
        authorizeUseCase
                .execute(phone, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { isLoading.onNext(false) }
                .subscribe(onAuthorized::onNext, ::onError)
                .addTo(disposable)
    }

    private fun onError(e: Throwable) {
        if (e is HttpException && e.code() == 401) onLoginFailed.onNext(RxNothing)
        else onError.onNext(e)
    }
}