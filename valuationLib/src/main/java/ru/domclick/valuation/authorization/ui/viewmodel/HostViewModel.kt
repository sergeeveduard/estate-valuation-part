package ru.domclick.valuation.authorization.ui.viewmodel

import ru.domclick.core.repo.HostRepo
import ru.domclick.valuation.common.di.scope.FragmentScope
import javax.inject.Inject

@FragmentScope
class HostViewModel
@Inject constructor(private val hostRepo: HostRepo) {

    val showSpinner = !hostRepo.hostList.isEmpty()
    val hostAliasList: List<String> = hostRepo.hostList.map { it.first }
    val selectedIndex: Int = hostRepo.hostList.indexOfFirst { it.second == hostRepo.currentHost }

    fun onHostSelected(index: Int) {
        hostRepo.currentHost = hostRepo.hostList[index].second
    }
}