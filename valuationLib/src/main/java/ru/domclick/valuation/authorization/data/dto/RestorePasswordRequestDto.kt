package ru.domclick.valuation.authorization.data.dto

class RestorePasswordRequestDto(
        val username: String,
        val backend: String = "cas"
)