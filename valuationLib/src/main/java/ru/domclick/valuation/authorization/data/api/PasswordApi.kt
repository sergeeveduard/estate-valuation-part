package ru.domclick.valuation.authorization.data.api

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import ru.domclick.valuation.authorization.data.dto.ChangePasswordRequestDto
import ru.domclick.valuation.authorization.data.dto.CheckSmsCodeRequestDto
import ru.domclick.valuation.authorization.data.dto.CheckSmsCodeResponseDto
import ru.domclick.valuation.authorization.data.dto.RestorePasswordRequestDto


interface PasswordApi {

    @POST("api/v1/password/restore")
    fun restorePassword(@Body body: RestorePasswordRequestDto): Single<Response<Any>>

    @POST("api/v2/password/check_sms_code/")
    fun checkSmsCode(@Body body: CheckSmsCodeRequestDto): Single<CheckSmsCodeResponseDto>

    @POST("api/v2/password/change/")
    fun changePassword(@Body body: ChangePasswordRequestDto): Single<Response<Any>>
}