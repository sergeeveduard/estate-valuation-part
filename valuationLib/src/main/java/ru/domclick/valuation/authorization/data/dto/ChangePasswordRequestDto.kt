package ru.domclick.valuation.authorization.data.dto

class ChangePasswordRequestDto(
        val username: String,
        val ticket: String,
        val password: String,
        val password2: String,
        val backend: String = "cas"
)