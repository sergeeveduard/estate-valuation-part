package ru.domclick.valuation.authorization.ui.viewmodel

import android.content.Context
import ru.domclick.core.extentions.empty
import ru.domclick.valuation.common.di.scope.FragmentScope
import ru.domclick.valuation.common.ui.viewmodel.ValidateTextViewModel
import ru.domclick.valuation.lib.R
import javax.inject.Inject

private const val REQUIRED_LENGTH = 10

@FragmentScope
class PhoneViewModel
@Inject constructor(private val context: Context) : ValidateTextViewModel() {

    override fun validate(text: String): Boolean {
        val isValid = text.length == REQUIRED_LENGTH

        val message = if (isValid) {
            String.empty()
        } else {
            context.getString(R.string.auth_error_invalid_phone)
        }
        error.onNext(message)

        return isValid
    }
}