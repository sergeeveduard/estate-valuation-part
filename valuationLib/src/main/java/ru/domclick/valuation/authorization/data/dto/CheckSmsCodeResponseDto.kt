package ru.domclick.valuation.authorization.data.dto

import ru.domclick.valuation.common.data.dto.ResponseDto


class CheckSmsCodeResponseDto : ResponseDto<CheckSmsCodeResultDto>()

class CheckSmsCodeResultDto(val ticket: String)