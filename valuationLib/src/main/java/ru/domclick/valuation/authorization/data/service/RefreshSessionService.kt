package ru.domclick.valuation.authorization.data.service

import io.reactivex.Single
import retrofit2.HttpException
import ru.domclick.valuation.common.data.repo.AccountRepo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RefreshSessionService
@Inject constructor(private val authorizeService: AuthorizeService,
                    private val accountRepo: AccountRepo) {

    fun refresh(): Single<Boolean> {
        val account = accountRepo.getAccount() ?: return Single.just(false)

        return authorizeService
                .authorize(account.login, account.password)
                .map { true }
                .onErrorResumeNext(::parseError)
    }

    private fun parseError(e: Throwable): Single<Boolean> {
        return if (e is HttpException && e.code() == 401) {
            Single.just(false)
        } else {
            Single.error(e)
        }
    }
}