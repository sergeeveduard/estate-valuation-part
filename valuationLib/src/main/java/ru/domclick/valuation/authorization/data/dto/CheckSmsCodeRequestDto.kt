package ru.domclick.valuation.authorization.data.dto


class CheckSmsCodeRequestDto(
        val username: String,
        val code: String,
        val backend: String = "cas"
)