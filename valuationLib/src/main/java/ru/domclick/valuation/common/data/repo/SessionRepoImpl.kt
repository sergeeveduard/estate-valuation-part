package ru.domclick.valuation.common.data.repo

import ru.domclick.core.repo.SessionRepo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SessionRepoImpl
@Inject constructor(private val accountRepo: AccountRepo) : SessionRepo {

    override fun getSession(): String? {
        return accountRepo.getAccount()?.sessionId
    }
}