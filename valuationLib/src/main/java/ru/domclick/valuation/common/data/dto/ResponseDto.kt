package ru.domclick.valuation.common.data.dto

open class ResponseDto<R>(
        var result: R?,
        var success: Boolean,
        var errors: List<ErrorDto>
) {
    constructor() : this(
            null,
            true,
            emptyList()
    )
}