package ru.domclick.valuation.common.data.dto

class ErrorDto(
        val message: String,
        val code: String
)