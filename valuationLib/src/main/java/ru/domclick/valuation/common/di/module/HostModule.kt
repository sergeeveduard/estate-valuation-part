package ru.domclick.valuation.common.di.module

import dagger.Binds
import dagger.Module
import ru.domclick.core.repo.HostRepo
import ru.domclick.valuation.common.data.repo.HostRepoImpl
import javax.inject.Singleton

@Module
abstract class HostModule {

    @Binds
    @Singleton
    internal abstract fun provideHostRepo(impl: HostRepoImpl): HostRepo
}
