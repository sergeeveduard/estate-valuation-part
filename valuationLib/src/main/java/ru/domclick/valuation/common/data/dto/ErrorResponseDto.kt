package ru.domclick.valuation.common.data.dto

class ErrorResponseDto(
        var errors: List<ErrorDto>
)