package ru.domclick.valuation.common.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import ru.domclick.core.extentions.DisposableHolder
import javax.inject.Inject

open class BaseActivity : AppCompatActivity(),
        HasSupportFragmentInjector,
        DisposableHolder {

    private val lifeCycleDisposable = CompositeDisposable()

    @Inject
    lateinit var dispatchingFragmentInjector: DispatchingAndroidInjector<androidx.fragment.app.Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun supportFragmentInjector(): AndroidInjector<androidx.fragment.app.Fragment> = dispatchingFragmentInjector

    override fun addDisposable(disposable: Disposable) {
        lifeCycleDisposable.add(disposable)
    }

    override fun onDestroy() {
        lifeCycleDisposable.clear()
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }
}