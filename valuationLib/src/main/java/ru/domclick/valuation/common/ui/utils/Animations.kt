package ru.domclick.valuation.common.ui.utils

import android.animation.ObjectAnimator
import android.view.View
import com.github.aakira.expandablelayout.Utils

object Animations {
    fun createRotateAnimator(target: View, from: Float, to: Float): ObjectAnimator {
        val animator = ObjectAnimator.ofFloat(target, "rotation", from, to)
        animator.duration = 300
        animator.interpolator = Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR)
        return animator
    }
}