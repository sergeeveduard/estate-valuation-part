package ru.domclick.valuation.common.analytic

object Events {
    // auth
    const val CLICK_LOGIN = "click_login"
    const val CLICK_FORGET_PWD = "click_forget_pwd"
    const val CLICK_SEND_SMS_CODE = "click_send_sms_code"

    // task detail
    const val TASK_DETAIL_CLICK_SEND_TASK = "task_detail_click_send_task"
    const val TASK_DETAIL_BUTTON_STATE = "task_detail_button_state"
}