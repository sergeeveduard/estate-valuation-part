package ru.domclick.valuation.common.ui

import android.app.Activity
import android.app.Application
import android.app.Service
import android.content.Context
import android.content.Intent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasServiceInjector
import io.reactivex.Single
import ru.domclick.valuation.common.analytic.Analytics
import ru.domclick.valuation.common.di.component.ApplicationComponent
import ru.domclick.valuation.common.di.component.DaggerApplicationComponent
import ru.domclick.valuation.common.di.module.ApplicationModule
import ru.domclick.valuation.startup.StartupRouter
import javax.inject.Inject


abstract class ValuationApplication : Application(),
        HasActivityInjector,
        HasServiceInjector {

    @Inject
    internal lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    @Inject
    internal lateinit var dispatchingServiceInjector: DispatchingAndroidInjector<Service>

    @Inject
    internal lateinit var startupRouter: StartupRouter

    @Inject
    lateinit var analytics: Analytics

    private val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        DaggerApplicationComponent
                .builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        inject()
    }

    fun getStartActivityIntent(context: Context): Single<Intent> = startupRouter.getStartActivityIntent(context)

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingActivityInjector

    override fun serviceInjector(): AndroidInjector<Service> = dispatchingServiceInjector

    private fun inject() {
        appComponent.inject(this)
    }
}