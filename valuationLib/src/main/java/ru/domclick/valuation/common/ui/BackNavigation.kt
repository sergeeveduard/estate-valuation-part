package ru.domclick.valuation.common.ui

interface BackNavigation {

    fun goBack()
}