package ru.domclick.valuation.common.data.converter

import android.util.Log
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.HttpException
import retrofit2.Response
import ru.domclick.core.exception.HttpWithErrorMessageException
import ru.domclick.core.extentions.fromJson
import ru.domclick.valuation.common.data.dto.ResponseDto
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ErrorResponseHandler
@Inject constructor(private val gson: Gson) {

    fun <T : Any> handle(response: Response<T>): Response<T> {
        if (response.code() == 400) {

            val errorMessage = parse(response.errorBody())
            if (!errorMessage.isNullOrEmpty()) {
                throw HttpWithErrorMessageException(errorMessage!!, response)
            }
        } else if (response.code() !in 200..299) {
            throw HttpException(response)
        }
        return response
    }

    private fun parse(response: ResponseBody?): String? {

        var result: String? = null
        try {
            response?.let {
                val obj: ResponseDto<String> = gson.fromJson(it.charStream())
                result = obj.errors.joinToString { it.message }
            }
        } catch (e: Throwable) {
            Log.e("ErrorResponseParser", "Cannot parse response", e)
        }

        return result
    }
}