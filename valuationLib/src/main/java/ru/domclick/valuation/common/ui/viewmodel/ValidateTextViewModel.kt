package ru.domclick.valuation.common.ui.viewmodel

import io.reactivex.subjects.PublishSubject
import ru.domclick.core.extentions.empty

abstract class ValidateTextViewModel {

    val error = PublishSubject.create<String>()!!

    private var hasFocus = false
    private var isFirstEdit = true
    private var text = String.empty()

    fun onFocusChanged(hasFocus: Boolean) {
        this.hasFocus = hasFocus
        if (!hasFocus) {
            isFirstEdit = false
            validate(text)
        }
    }

    fun onTextChanged(text: String) {
        this.text = text
        if (!isFirstEdit) {
            validate(text)
        }
    }

    fun validate(): Boolean {
        isFirstEdit = false
        return validate(text)
    }

    protected abstract fun validate(text: String): Boolean
}