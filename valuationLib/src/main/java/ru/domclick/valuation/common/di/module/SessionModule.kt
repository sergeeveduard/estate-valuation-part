package ru.domclick.valuation.common.di.module

import dagger.Binds
import dagger.Module
import ru.domclick.core.repo.SessionRepo
import ru.domclick.valuation.common.data.repo.SessionRepoImpl
import javax.inject.Singleton

@Module
abstract class SessionModule {

    @Binds
    @Singleton
    internal abstract fun provideSessionRepo(sessionRepoImpl: SessionRepoImpl): SessionRepo
}