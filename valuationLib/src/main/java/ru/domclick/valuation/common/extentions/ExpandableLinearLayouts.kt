package ru.domclick.valuation.common.extentions

import android.view.View
import android.view.animation.LinearInterpolator
import com.github.aakira.expandablelayout.ExpandableLayout
import com.github.aakira.expandablelayout.ExpandableLayoutListener
import com.github.aakira.expandablelayout.ExpandableLinearLayout
import ru.domclick.valuation.common.ui.utils.Animations

fun ExpandableLinearLayout.initRotateAnimation(toggleView: View) {
    this.setListener(object : ExpandableLayoutListener {
        override fun onAnimationEnd() {
        }

        override fun onOpened() {
        }

        override fun onAnimationStart() {
        }

        override fun onClosed() {
        }

        override fun onPreOpen() {
            Animations.createRotateAnimator(toggleView, 0f, 90f).start()
        }

        override fun onPreClose() {
            Animations.createRotateAnimator(toggleView, 90f, 0f).start()
        }
    })
}

private val linearInterpolator = LinearInterpolator()

fun ExpandableLinearLayout.forceToggle() {
    if (this.isExpanded) {
        this.collapse(ExpandableLayout.DEFAULT_DURATION.toLong(), linearInterpolator)
    } else {
        this.initLayout()
        this.expand(ExpandableLayout.DEFAULT_DURATION.toLong(), linearInterpolator)
    }
}