package ru.domclick.valuation.common.di.component

import dagger.Component
import dagger.android.AndroidInjectionModule
import ru.domclick.core.di.CoreNetworkModule
import ru.domclick.valuation.authorization.di.AuthActivityModule
import ru.domclick.valuation.authorization.di.AuthModule
import ru.domclick.valuation.common.di.module.ApplicationModule
import ru.domclick.valuation.common.di.module.HostModule
import ru.domclick.valuation.common.di.module.SessionModule
import ru.domclick.valuation.common.ui.ValuationApplication
import ru.domclick.valuation.database.di.DataBaseModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    ApplicationModule::class,
    DataBaseModule::class,
    CoreNetworkModule::class,
    SessionModule::class,
    HostModule::class,
    AuthActivityModule::class,
    AuthModule::class
])
interface ApplicationComponent {
    fun inject(app: ValuationApplication)
}