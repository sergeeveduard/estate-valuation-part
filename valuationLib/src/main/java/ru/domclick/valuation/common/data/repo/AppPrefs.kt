package ru.domclick.valuation.common.data.repo

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import ru.domclick.core.extentions.empty
import javax.inject.Inject
import javax.inject.Singleton


private const val PREFS_FILE_NAME = "config"
private const val PREFS_LOGIN = "login"
private const val PREFS_HOST = "host"
private const val PREFS_CATALOG_LAST_UPDATE = "catalog_last_update"


@Singleton
class AppPrefs
@Inject constructor(private val context: Context) {

    private val sharedPreferences: SharedPreferences by lazy { context.getSharedPreferences(PREFS_FILE_NAME, MODE_PRIVATE) }

    fun setLogin(login: String) {
        sharedPreferences.edit().putString(PREFS_LOGIN, login).apply()
    }

    fun getLogin(): String = sharedPreferences.getString(PREFS_LOGIN, String.empty())

    fun setHost(host: String) {
        sharedPreferences.edit().putString(PREFS_HOST, host).apply()
    }

    fun getHost(): String = sharedPreferences.getString(PREFS_HOST, String.empty())

    fun setCatalogLastUpdate(datetime: Long) {
        sharedPreferences.edit().putLong(PREFS_CATALOG_LAST_UPDATE, datetime).apply()
    }

    fun getCatalogLastUpdate(): Long = sharedPreferences.getLong(PREFS_CATALOG_LAST_UPDATE, 0)
}