package ru.domclick.valuation.common.data.dto

/**
 * Модель для элента справочника вида:
 *
 *   "using_type": [
 *   {
 *       "id": 1,
 *       "name": "Жилая недвижимость"
 *   },
 *   {
 *       "id": 2,
 *       "name": "Нежилая недвижимость"
 *   }
 *   ]
 *
 * или
 *
 *   "photo_sub_categories": [
 *   {
 *       "rel": 1,
 *       "id": 1,
 *       "add_info": false,
 *       "name": "Фасад"
 *   }]
 */
data class ReferenceDto(
        val id: Int,
        val rel: Int?,
        val name: String
)
