package ru.domclick.valuation.common.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import ru.domclick.valuation.common.analytic.Analytics
import ru.domclick.valuation.common.analytic.StubAnalytics
import ru.domclick.valuation.common.ui.ValuationApplication
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: ValuationApplication) {

    @Provides
    @Singleton
    internal fun provideApplicationContext(): Context = application

    @Provides
    @Singleton
    internal fun provideAnalytics(): Analytics = StubAnalytics()
}