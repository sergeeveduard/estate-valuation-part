package ru.domclick.valuation.common.analytic

import android.app.Activity
import android.os.Bundle
import javax.inject.Inject
import javax.inject.Singleton

class StubAnalytics : Analytics {
    override fun logException(throwable: Throwable) {
    }

    override fun logScreen(activity: Activity, screen: String) {
    }

    override fun setCurrentUser(userId: Long) {
    }

    override fun logEvent(event: String, params: Bundle?) {
    }
}