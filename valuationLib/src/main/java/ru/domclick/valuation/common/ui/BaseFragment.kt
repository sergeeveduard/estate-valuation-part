package ru.domclick.valuation.common.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import ru.domclick.core.extentions.DisposableHolder
import ru.domclick.core.extentions.activityView
import ru.domclick.core.extentions.visibleOrGone
import ru.domclick.valuation.common.analytic.Analytics
import ru.domclick.valuation.lib.R
import javax.inject.Inject

abstract class BaseFragment : Fragment(),
        DisposableHolder,
        HasSupportFragmentInjector {

    @Inject
    lateinit var analytics: Analytics

    @Inject
    lateinit var dispatchingFragmentInjector: DispatchingAndroidInjector<Fragment>

    protected val toolbar: Toolbar? get() = activityView?.findViewById(R.id.toolbar)

    protected open val shouldShowToolbar: Boolean get() = true

    private val lifeCycleDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar?.run {
            menu.clear()
            navigationIcon = null
            setNavigationOnClickListener { }
            visibleOrGone(shouldShowToolbar)
        }
    }

    override fun onDestroyView() {
        lifeCycleDisposable.clear()
        super.onDestroyView()
    }

    override fun addDisposable(disposable: Disposable) {
        lifeCycleDisposable.add(disposable)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingFragmentInjector
}