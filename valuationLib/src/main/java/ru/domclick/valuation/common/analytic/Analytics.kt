package ru.domclick.valuation.common.analytic

import android.app.Activity
import android.os.Bundle

interface Analytics {

    fun logException(throwable: Throwable)

    fun logScreen(activity: Activity, screen: String)

    fun setCurrentUser(userId: Long)

    fun logEvent(event: String, params: Bundle? = null)
}