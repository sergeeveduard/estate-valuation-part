package ru.domclick.valuation.common.data.dto

enum class OrderStatus(val id: Int) {

    READY(16),
    RESCHEDULED(18),
    FINISHED(19),
    UNKNOWN(-1)
}