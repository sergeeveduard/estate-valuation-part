package ru.domclick.valuation.common.di.scope

@Retention(AnnotationRetention.RUNTIME)
annotation class ServiceScope