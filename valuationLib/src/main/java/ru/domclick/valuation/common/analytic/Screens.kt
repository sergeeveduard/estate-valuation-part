package ru.domclick.valuation.common.analytic

object Screens {

    const val LOGIN = "login"
    const val RESTORE_PASSWORD = "restore_password"
    const val RESTORE_PASSWORD_NEW_PASSWORD = "restore_password_new_password"
    const val RESTORE_PASSWORD_SMS_CODE = "restore_password_sms_code"

    const val CAMERA = "camera"
    const val PREVIEW = "preview"
    const val EXPLAIN_LOCATION_PERMISSION = "explain_location_permission"

    const val TASK_LIST = "task_list"
    const val TASK_DETAIL = "task_detail"
    const val TASK_GALLERY_LIST = "task_gallery_list"
}