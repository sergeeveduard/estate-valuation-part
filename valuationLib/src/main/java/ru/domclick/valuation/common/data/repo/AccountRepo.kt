package ru.domclick.valuation.common.data.repo

import android.content.Context
import ru.domclick.accountmanager.DomClickAccount
import ru.domclick.accountmanager.DomClickAccountManager
import ru.domclick.core.extentions.empty
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AccountRepo
@Inject constructor(context: Context,
                    private val appPrefs: AppPrefs) {

    private var domClickAccountManager = DomClickAccountManager(context)

    fun putAccount(login: String, password: String, sessionId: String) {
        appPrefs.setLogin(login)
        domClickAccountManager.putAccount(login, password, sessionId)
    }

    fun getAccount(): DomClickAccount? {
        val login = appPrefs.getLogin()
        return if (login.isNotEmpty()) {
            domClickAccountManager.getAccounts().firstOrNull { it.login == login }
        } else {
            null
        }
    }

    fun logout() {
        appPrefs.setLogin(String.empty())
    }
}