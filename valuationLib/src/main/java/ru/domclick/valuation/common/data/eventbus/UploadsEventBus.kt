package ru.domclick.valuation.common.data.eventbus

import io.reactivex.Flowable
import io.reactivex.processors.PublishProcessor
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UploadsEventBus
@Inject constructor() {

    private val photoUploadedSubject = PublishProcessor.create<Long>()
    private val orderSentStatusChangedSubject = PublishProcessor.create<Long>()

    val photoUploaded: Flowable<Long> = photoUploadedSubject
    val orderSentStatusChanged: Flowable<Long> = orderSentStatusChangedSubject

    fun postPhotoUploadedEvent(photoId: Long) {
        photoUploadedSubject.onNext(photoId)
    }

    fun postOrderSentStatusChanged(orderId: Long) {
        orderSentStatusChangedSubject.onNext(orderId)
    }
}