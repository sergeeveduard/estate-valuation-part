package ru.domclick.valuation.common.ui

import android.util.Log
import android.view.View
import retrofit2.HttpException
import ru.domclick.core.exception.EmptyDataException
import ru.domclick.core.exception.HttpWithErrorMessageException
import ru.domclick.core.extentions.analytics
import ru.domclick.core.extentions.showSnackbar
import ru.domclick.valuation.lib.R
import java.io.IOException
import javax.net.ssl.SSLException

object ErrorNotifier {

    fun showError(anchor: View, e: Throwable) {
        Log.e("Valuation", "ups", e)
        anchor.analytics.logException(e)
        when (e) {
            is HttpWithErrorMessageException -> showSnackbar(anchor, e.error)
            is HttpException -> onHttpException(anchor, e)
            is IOException -> onIoException(anchor, e)
            is EmptyDataException -> showSnackbar(anchor, R.string.error_empty_data)
            else -> showSnackbar(anchor, R.string.error_unknown)
        }
    }

    private fun onHttpException(anchor: View, e: HttpException) {
        when (e.code()) {
            in 401..401 -> showSnackbar(anchor, R.string.error_server_401)
            in 400..499 -> showSnackbar(anchor, R.string.error_server_4xx)
            in 500..599 -> showSnackbar(anchor, R.string.error_server_5xx)
            else -> showSnackbar(anchor, R.string.error_unknown)
        }
    }

    private fun onIoException(anchor: View, e: IOException) {
        when (e) {
            is SSLException -> showSnackbar(anchor, R.string.error_ssl)
            else -> showSnackbar(anchor, R.string.error_network)
        }
    }
}