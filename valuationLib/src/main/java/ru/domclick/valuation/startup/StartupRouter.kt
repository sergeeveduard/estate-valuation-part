package ru.domclick.valuation.startup

import android.content.Context
import android.content.Intent
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.annotations.TestOnly
import ru.domclick.core.extentions.clearActivityStack
import ru.domclick.valuation.authorization.data.service.RefreshSessionService
import ru.domclick.valuation.authorization.ui.AuthActivity
import ru.domclick.valuation.common.data.repo.AccountRepo
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class StartupRouter
@Inject constructor(private val accountRepo: AccountRepo,
                    private val refreshSessionService: RefreshSessionService) {

    companion object {
        internal const val MAX_WAIT = 5L//seconds
    }

    private var timerScheduler: Scheduler

    init {
        timerScheduler = Schedulers.computation()
    }

    @TestOnly
    internal constructor(accountRepo: AccountRepo,
                         refreshSessionService: RefreshSessionService,
                         timerScheduler: Scheduler)

            : this(accountRepo, refreshSessionService) {

        this.timerScheduler = timerScheduler
    }

    fun getStartActivityIntent(context: Context): Single<Intent> {
        return if (accountRepo.getAccount() == null) {
            Single.just(getAuthIntent(context))
        } else {
            refreshSessionService.refresh()

                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .timeout(MAX_WAIT, TimeUnit.SECONDS, timerScheduler)

                    .onErrorResumeNext { Single.just(true) }
                    .map { if (it) getOrderListIntent(context) else getAuthIntent(context) }
        }
    }

    internal fun getOrderListIntent(context: Context): Intent = throw NotImplementedError()

    internal fun getAuthIntent(context: Context): Intent = AuthActivity.newIntent(context).clearActivityStack()
}