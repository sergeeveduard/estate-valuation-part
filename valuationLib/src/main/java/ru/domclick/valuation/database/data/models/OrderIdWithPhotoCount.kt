package ru.domclick.valuation.database.data.models

import androidx.room.ColumnInfo

class OrderIdWithPhotoCount(
        @ColumnInfo(name = "order_id")
        val orderId: Long,

        @ColumnInfo(name = "photo_count")
        val photoCount: Int
)