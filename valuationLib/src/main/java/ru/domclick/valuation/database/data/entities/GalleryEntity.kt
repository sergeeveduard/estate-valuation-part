package ru.domclick.valuation.database.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
        tableName = "gallery",
        indices = [(Index("album_id", "subcategory_id"))]
)
class GalleryEntity(
        @PrimaryKey(autoGenerate = true)
        var id: Long?,

        @ColumnInfo(name = "album_id")
        var albumId: Long,

        @ColumnInfo(name = "subcategory_id")
        var subcategoryId: Int,

        var title: String
)