package ru.domclick.valuation.database.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Maybe
import ru.domclick.valuation.database.data.entities.CommentEntity

@Dao
interface CommentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(comment: CommentEntity)

    @Query("SELECT * FROM photo_comment WHERE photo_id = :photoId")
    fun getByPhotoId(photoId: Long): Maybe<CommentEntity>

    @Query("DELETE FROM photo_comment WHERE photo_id = :photoId")
    fun deleteByPhotoId(photoId: Long)

    @Query("SELECT * FROM photo_comment WHERE remote_id IS NOT NULL")
    fun getAllWithRemoteIdNotNull(): List<CommentEntity>
}