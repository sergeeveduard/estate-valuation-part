package ru.domclick.valuation.database.data.entities

import androidx.room.*
import ru.domclick.core.extentions.empty
import ru.domclick.valuation.database.data.models.SendStatus

@Entity(
        tableName = "photo",
        indices = [
            Index(value = ["gallery_id"]),
            Index(value = ["status"])
        ],
        foreignKeys = [ForeignKey(
                entity = GalleryEntity::class,
                parentColumns = arrayOf("id"),
                childColumns = arrayOf("gallery_id")
        )]
)
class PhotoEntity(

        @PrimaryKey(autoGenerate = true)
        var id: Long?,

        @ColumnInfo(name = "gallery_id")
        var galleryId: Long,

        var file: String,

        val longitude: Float = 0f,
        val latitude: Float = 0f,
        val height: Float = 0f,

        val hash: String = String.empty(),

        @ColumnInfo(name = "status")
        var sendStatus: SendStatus = SendStatus.DEFAULT,

        @ColumnInfo(name = "remote_id")
        var remoteId: Long = 0
)