package ru.domclick.valuation.database.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import ru.domclick.valuation.database.data.models.SendStatus


@Entity(
        tableName = "order_status",
        indices = [
            Index(value = ["status"]),
            Index(value = ["updated"])
        ]
)
class OrderStatusEntity(

        @PrimaryKey
        @ColumnInfo(name = "order_id")
        var orderId: Long,

        @ColumnInfo(name = "status")
        var sendStatus: SendStatus,

        var updated: Long = System.currentTimeMillis()
)