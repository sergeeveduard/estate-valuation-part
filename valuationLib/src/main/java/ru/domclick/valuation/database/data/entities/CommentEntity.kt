package ru.domclick.valuation.database.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "photo_comment")
class CommentEntity(
        @PrimaryKey
        @ColumnInfo(name = "photo_id")
        var photoId: Long,

        var comment: String,

        @ColumnInfo(name = "remote_id")
        var remoteId: Long?
)