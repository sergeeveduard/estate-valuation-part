package ru.domclick.valuation.database.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single
import ru.domclick.valuation.database.data.entities.ReferenceEntity

@Dao
interface ReferenceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(items: List<ReferenceEntity>)

    @Query("SELECT * FROM reference WHERE type = :type")
    fun getAllByType(type: ReferenceEntity.Type): Single<List<ReferenceEntity>>

    @Query("SELECT * FROM reference WHERE type = :type AND parentId = :parentId")
    fun getAllByTypeAndParentId(type: ReferenceEntity.Type, parentId: Int): Single<List<ReferenceEntity>>

    @Query("DELETE FROM reference WHERE type = :type")
    fun deleteAllByType(type: ReferenceEntity.Type)

    @Query("SELECT COUNT(id) FROM reference WHERE type = :type")
    fun getCountByType(type: ReferenceEntity.Type): Single<Long>
}