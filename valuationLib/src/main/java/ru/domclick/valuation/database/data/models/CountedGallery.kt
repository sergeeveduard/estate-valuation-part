package ru.domclick.valuation.database.data.models

import androidx.room.ColumnInfo
import androidx.room.Embedded
import ru.domclick.valuation.database.data.entities.GalleryEntity

class CountedGallery(
        @ColumnInfo(name = "count")
        var photoCount: Int,

        @Embedded
        var gallery: GalleryEntity
)