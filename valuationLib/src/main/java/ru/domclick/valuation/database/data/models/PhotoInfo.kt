package ru.domclick.valuation.database.data.models

import androidx.room.ColumnInfo

class PhotoInfo(
        @ColumnInfo(name = "photo_id")
        var photoId: Long,

        @ColumnInfo(name = "order_id")
        var orderId: Long,

        @ColumnInfo(name = "category_id")
        var categoryId: Int,

        @ColumnInfo(name = "subcategory_id")
        var subcategoryId: Int,

        var file: String,

        val longitude: Float,
        val latitude: Float,
        val height: Float
)