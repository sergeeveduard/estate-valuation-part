package ru.domclick.valuation.database.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import ru.domclick.valuation.database.data.DataBase
import ru.domclick.valuation.database.data.DataBaseMigrations
import ru.domclick.valuation.database.data.dao.*
import javax.inject.Singleton

@Module
class DataBaseModule {

    @Singleton
    @Provides
    fun provideDataBase(context: Context): DataBase {
        return Room.databaseBuilder(context, DataBase::class.java, "valuation.db")
                .addMigrations(*DataBaseMigrations.migrations)
                .build()
    }

    @Singleton
    @Provides
    fun provideReferenceDao(db: DataBase): ReferenceDao = db.getReferenceDao()

    @Singleton
    @Provides
    fun provideOrderStatusDao(db: DataBase): OrderStatusDao = db.getOrderStatusDao()

    @Singleton
    @Provides
    fun provideAlbumDao(db: DataBase): AlbumDao = db.getAlbumDao()

    @Singleton
    @Provides
    fun provideGalleryDao(db: DataBase): GalleryDao = db.getGalleryDao()

    @Singleton
    @Provides
    fun providePhotoDao(db: DataBase): PhotoDao = db.getPhotoDao()

    @Singleton
    @Provides
    fun provideCommentDao(db: DataBase): CommentDao = db.getCommentDao()

    @Singleton
    @Provides
    fun provideQuestionnaireDao(db: DataBase): QuestionnaireDao = db.getQuestionnaireDao()
}

