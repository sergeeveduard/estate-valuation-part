package ru.domclick.valuation.database.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Single
import ru.domclick.valuation.database.data.entities.AlbumEntity
import ru.domclick.valuation.database.data.models.AlbumPreview

@Dao
interface AlbumDao {

    @Insert
    fun insert(album: AlbumEntity): Long

    @Insert
    fun insert(albums: List<AlbumEntity>)

    @Query("SELECT * FROM album WHERE id = :albumId")
    fun getById(albumId: Long): Single<AlbumEntity>

    @Query("SELECT * FROM album as a WHERE a.order_id = :orderId")
    fun getAllByOrderId(orderId: Long): Single<List<AlbumEntity>>

    @Query("""
        SELECT q.photo_id, q.file, q.albums_id FROM album AS a

        JOIN    (
                SELECT p.id as photo_id, p.file as file, a.id as albums_id FROM photo as p
                    JOIN gallery as g ON g.id = p.gallery_id
                    JOIN album as a ON a.id = g.album_id
                WHERE a.order_id = :orderId ORDER BY p.id ASC
                ) as q

        ON q.albums_id = a.id
        WHERE a.order_id = :orderId
        GROUP BY a.id
    """)
    fun getAlbumPreviewsByOrderId(orderId: Long): Single<List<AlbumPreview>>

    @Query("DELETE FROM album WHERE order_id IN (:orderIds)")
    fun deleteAllByOrderId(orderIds: List<Long>): Int
}