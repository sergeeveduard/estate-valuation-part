package ru.domclick.valuation.database.data.models

import java.io.Serializable

class Gallery(
        var id: Long,
        var title: String
) : Serializable