package ru.domclick.valuation.database.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey


@Entity(
        tableName = "album",
        indices = [(Index("order_id", "category_id", unique = true))]
)
class AlbumEntity(
        @PrimaryKey(autoGenerate = true)
        var id: Long?,

        @ColumnInfo(name = "order_id")
        var orderId: Long,

        @ColumnInfo(name = "category_id")
        var categoryId: Int,

        var title: String
)