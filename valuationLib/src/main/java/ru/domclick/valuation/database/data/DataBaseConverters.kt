package ru.domclick.valuation.database.data

import androidx.room.TypeConverter
import ru.domclick.valuation.database.data.entities.OrderStatusEntity
import ru.domclick.valuation.database.data.entities.ReferenceEntity
import ru.domclick.valuation.database.data.models.SendStatus

class DataBaseConverters {

    /**
     * [ReferenceEntity.Type] to [String]
     */
    @TypeConverter
    fun convertReferenceEntityTypeToString(type: ReferenceEntity.Type): String = type.toString()

    /**
     * [String] to [ReferenceEntity.Type]
     */
    @TypeConverter
    fun convertStringToReferenceEntityType(value: String): ReferenceEntity.Type = ReferenceEntity.Type.valueOf(value)

    /**
     * [OrderStatusEntity.Status] to [Int]
     */
    @TypeConverter
    fun convertOrderStatusToInt(sendStatus: SendStatus): Int = sendStatus.id

    /**
     * [Int] to [OrderStatusEntity.Status]
     */
    @TypeConverter
    fun convertIntToOrderStatusEntityStatus(id: Int): SendStatus = SendStatus.valueOf(id)
}