package ru.domclick.valuation.database.data.models

enum class SendStatus(val id: Int) {
    DEFAULT(0),
    SENDING(1),
    ERROR(2),
    SENT(3);

    companion object {
        fun valueOf(id: Int): SendStatus {
            return SendStatus.values().find { it.id == id }
                    ?: throw ArrayIndexOutOfBoundsException("SendStatus with id=$id not exists")
        }
    }
}