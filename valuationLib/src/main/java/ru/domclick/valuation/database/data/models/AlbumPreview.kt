package ru.domclick.valuation.database.data.models

import androidx.room.ColumnInfo

class AlbumPreview(

        //for test only
        @ColumnInfo(name = "photo_id")
        var photoId: Long,

        @ColumnInfo(name = "albums_id")
        var albumId: Long,

        @ColumnInfo(name = "file")
        var previewFile: String
)