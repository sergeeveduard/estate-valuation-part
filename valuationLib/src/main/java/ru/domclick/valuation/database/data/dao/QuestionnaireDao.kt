package ru.domclick.valuation.database.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Maybe
import ru.domclick.valuation.database.data.entities.QuestionnaireEntity

@Dao
interface QuestionnaireDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: QuestionnaireEntity): Completable

    @Query("SELECT * FROM questionnaire WHERE order_id = :orderId")
    fun getByOrderId(orderId: Long): Maybe<QuestionnaireEntity>

    @Query("DELETE FROM questionnaire WHERE order_id IN (:orderId)")
    fun deleteByOrderId(vararg orderId: Long)
}