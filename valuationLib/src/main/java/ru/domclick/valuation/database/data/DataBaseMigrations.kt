package ru.domclick.valuation.database.data

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

object DataBaseMigrations {

    val migrations: Array<Migration> get() = arrayOf(MIGRATION_1_2, MIGRATION_2_3)


    private val MIGRATION_1_2 = object : Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("CREATE TABLE IF NOT EXISTS `photo_comment` (`photo_id` INTEGER NOT NULL, `comment` TEXT NOT NULL, `remote_id` INTEGER, PRIMARY KEY(`photo_id`))")
        }
    }

    private val MIGRATION_2_3 = object : Migration(2, 3) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("CREATE TABLE IF NOT EXISTS `questionnaire` (`order_id` INTEGER NOT NULL, `questionnaire_json` TEXT NOT NULL, PRIMARY KEY(`order_id`))")
        }
    }


}