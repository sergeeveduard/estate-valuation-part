package ru.domclick.valuation.database.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single
import ru.domclick.valuation.database.data.entities.GalleryEntity
import ru.domclick.valuation.database.data.models.CountedGallery

@Dao
interface GalleryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(gallery: GalleryEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(galleries: List<GalleryEntity>)

    @Query("SELECT * FROM gallery WHERE id = :galleryId")
    fun getById(galleryId: Long): Single<GalleryEntity>

    @Query("SELECT g.*, COUNT(p.id) as count FROM gallery AS g" +
            " JOIN album as a ON a.id = g.album_id" +
            " LEFT JOIN photo AS p ON g.id = p.gallery_id" +
            " WHERE a.order_id = :orderId" +
            " GROUP BY g.id")
    fun getCountedByOrderId(orderId: Long): Single<List<CountedGallery>>

    @Query("SELECT COUNT(id) FROM gallery as g WHERE g.album_id = :albumId")
    fun getGalleriesCountInAlbum(albumId: Long): Single<Long>

    @Query("SELECT * FROM gallery WHERE album_id = :albumId")
    fun getAllByAlbumId(albumId: Long): Single<List<GalleryEntity>>

    @Query("""
        DELETE FROM gallery WHERE id IN (
            SELECT g.id FROM gallery as g
                JOIN album AS a ON a.id = g.album_id
            WHERE a.order_id IN (:orderIds)
        )
    """)
    fun deleteAllByOrderId(orderIds: List<Long>): Int
}