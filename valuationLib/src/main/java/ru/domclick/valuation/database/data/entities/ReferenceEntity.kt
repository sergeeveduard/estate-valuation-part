package ru.domclick.valuation.database.data.entities

import androidx.room.Entity
import androidx.room.Index

/**
 * Модель для элента справочника вида:
 *
 *   "using_type": [
 *   {
 *       "id": 1,
 *       "name": "Жилая недвижимость"
 *   },
 *   {
 *       "id": 2,
 *       "name": "Нежилая недвижимость"
 *   }
 *   ]
 *
 * или
 *
 *   "photo_sub_categories": [
 *   {
 *       "rel": 1,
 *       "id": 1,
 *       "add_info": false,
 *       "name": "Фасад"
 *   }]
 */
@Entity(
        tableName = "reference",
        primaryKeys = ["id", "type"],
        indices = [Index(value = arrayOf("type", "parentId"))]
)
class ReferenceEntity(
        var id: Int,
        var parentId: Int?,
        var name: String,
        var type: Type
) {
    enum class Type {
        OBJECT_KIND,
        PHOTO_CATEGORY,
        PHOTO_SUBCATEGORY,

        /**
         * До метро, ж/д станции, мин
         */
        PLACE_MOVE_TYPE,

        /**
         * Благоустройство
         */
        PLACE_ENTRANCE_SECURITY_LEVEL,

        /**
         * Придомовая территория
         */
        PLACE_HOUSE_YARD,

        /**
         * Тип первого этажа
         */
        PLACE_FIRST_FLOOR_TYPE
    }
}