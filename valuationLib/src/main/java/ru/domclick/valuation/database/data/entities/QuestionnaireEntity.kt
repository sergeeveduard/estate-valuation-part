package ru.domclick.valuation.database.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "questionnaire")
class QuestionnaireEntity(

        @PrimaryKey
        @ColumnInfo(name = "order_id")
        var orderId: Long,

        @ColumnInfo(name = "questionnaire_json")
        var questionnaireJson: String
)