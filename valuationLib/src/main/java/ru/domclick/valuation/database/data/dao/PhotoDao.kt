package ru.domclick.valuation.database.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import io.reactivex.Maybe
import io.reactivex.Single
import ru.domclick.valuation.database.data.entities.PhotoEntity
import ru.domclick.valuation.database.data.models.OrderIdWithPhotoCount
import ru.domclick.valuation.database.data.models.PhotoInfo
import ru.domclick.valuation.database.data.models.SendStatus

@Dao
interface PhotoDao {

    @Insert
    fun insert(photo: PhotoEntity): Long

    @Insert
    fun insert(photos: List<PhotoEntity>)

    @Update
    fun update(photo: PhotoEntity)

    @Query("SELECT * FROM photo WHERE id = :photoId")
    fun getById(photoId: Long): Maybe<PhotoEntity>

    @Query("SELECT * FROM photo WHERE id in (:photos)")
    fun getByIdList(photos: List<Long>): Single<List<PhotoEntity>>

    @Query("""SELECT p.* FROM photo as p
            JOIN gallery as g ON g.id = p.gallery_id
            WHERE g.album_id = :albumId ORDER BY p.id DESC""")
    fun getAllByAlbumId(albumId: Long): Single<List<PhotoEntity>>

    @Query("""SELECT p.* FROM photo as p
            JOIN gallery as g ON g.id = p.gallery_id
            WHERE g.album_id = :albumId ORDER BY p.id DESC LIMIT 1""")
    fun getLastByAlbumId(albumId: Long): Maybe<PhotoEntity>

    @Query("""
        SELECT p.id as photo_id, os.order_id, file, latitude, longitude, height, category_id, subcategory_id FROM order_status as os
            JOIN album as a ON a.order_id = os.order_id
            JOIN gallery as g ON g.album_id = a.id
            JOIN photo as p ON p.gallery_id = g.id AND p.status = :photoStatus
        WHERE os.status = :orderStatus LIMIT 1
    """)
    fun getFirstPhotoByStatusAndOrderStatus(orderStatus: SendStatus,
                                            photoStatus: SendStatus): PhotoInfo?

    @Query("""
        SELECT COUNT(p.id) FROM photo as p
            JOIN gallery as g ON g.id = p.gallery_id
            JOIN album as a ON a.id = g.album_id
        WHERE p.status IN (:photoStatuses) AND a.order_id = :orderId
    """)
    fun getPhotoCountByStatusAndOrderId(orderId: Long, vararg photoStatuses: SendStatus): Long

    @Query("""
        SELECT COUNT(p.id) as photo_count, a.order_id as order_id FROM photo as p
            JOIN gallery as g ON g.id = p.gallery_id
            JOIN album as a ON a.id = g.album_id
        WHERE p.status IN (:photoStatuses) GROUP BY a.order_id
    """)
    fun getPhotoCountByStatus(vararg photoStatuses: SendStatus): Single<List<OrderIdWithPhotoCount>>

    @Query("""
        UPDATE photo SET status = :newPhotoStatus
        WHERE id in (
            SELECT p.id FROM photo as p
                JOIN gallery as g ON g.id = p.gallery_id
                JOIN album as a ON a.id = g.album_id
            WHERE a.order_id = :orderId AND p.status = :currentPhotoStatus
        )
    """)
    fun updateStatusWhereOrderIdAndCurrentStatus(orderId: Long,
                                                 currentPhotoStatus: SendStatus,
                                                 newPhotoStatus: SendStatus)

    @Query("UPDATE photo SET status = :status WHERE id = :photoId")
    fun updateStatus(photoId: Long, status: SendStatus)

    @Query("SELECT * FROM photo")
    fun getAll(): Single<List<PhotoEntity>>

    @Query("DELETE FROM photo WHERE id = :photoId")
    fun delete(photoId: Long): Int

    @Query("DELETE FROM photo WHERE id in (:photos)")
    fun delete(photos: List<Long>): Int

    @Query("""
        DELETE FROM photo WHERE id in (
            SELECT p.id FROM photo as p
                JOIN gallery as g ON g.id = p.gallery_id
                JOIN album as a ON a.id = g.album_id
            WHERE a.order_id in (:orderIds)
        )
    """)
    fun deleteAllByOrderId(orderIds: List<Long>): Int
}