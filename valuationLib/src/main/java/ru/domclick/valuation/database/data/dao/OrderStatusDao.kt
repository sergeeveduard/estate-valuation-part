package ru.domclick.valuation.database.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Maybe
import io.reactivex.Single
import ru.domclick.valuation.database.data.entities.OrderStatusEntity
import ru.domclick.valuation.database.data.models.SendStatus

@Dao
interface OrderStatusDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(status: OrderStatusEntity): Long

    @Query("SELECT * FROM order_status WHERE order_id = :orderId")
    fun getByOrderId(orderId: Long): Maybe<OrderStatusEntity>

    @Query("SELECT * FROM order_status WHERE status in (:sendStatuses)")
    fun getAllWithStatus(vararg sendStatuses: SendStatus): Single<List<OrderStatusEntity>>

    @Query("DELETE FROM order_status WHERE status IN (:sendStatuses) AND updated <= :updated")
    fun deleteWithUpdatedBeforeAndStatusIn(updated: Long, vararg sendStatuses: SendStatus): Int
}