package ru.domclick.valuation.database.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ru.domclick.valuation.database.data.dao.*
import ru.domclick.valuation.database.data.entities.*

@Database(
        version = 3,
        entities = [
            AlbumEntity::class,
            GalleryEntity::class,
            ReferenceEntity::class,
            OrderStatusEntity::class,
            PhotoEntity::class,
            CommentEntity::class,
            QuestionnaireEntity::class
        ]
)
@TypeConverters(DataBaseConverters::class)
abstract class DataBase : RoomDatabase() {

    abstract fun getReferenceDao(): ReferenceDao

    abstract fun getOrderStatusDao(): OrderStatusDao

    abstract fun getAlbumDao(): AlbumDao

    abstract fun getGalleryDao(): GalleryDao

    abstract fun getPhotoDao(): PhotoDao

    abstract fun getCommentDao(): CommentDao

    abstract fun getQuestionnaireDao(): QuestionnaireDao
}