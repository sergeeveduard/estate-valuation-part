package ru.domclick.core.exception

import retrofit2.Response

class HttpWithErrorMessageException(
        val error: String,
        response: Response<out Any>
) : RuntimeException("HTTP " + response.code() + " " + response.message())