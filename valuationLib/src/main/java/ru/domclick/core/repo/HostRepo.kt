package ru.domclick.core.repo

interface HostRepo {

    var currentHost: String

    val hostList: List<Pair<String, String>>
}