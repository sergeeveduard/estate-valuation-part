package ru.domclick.core.repo

interface SessionRepo {

    fun getSession(): String?
}