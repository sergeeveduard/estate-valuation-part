package ru.domclick.core.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import java.io.ByteArrayOutputStream
import java.io.IOException

object BitmapUtils {

    /**
     * Converts bitmap to byte array in JPEG format
     *
     * @param bitmap source bitmap
     * @return result byte array
     */
    fun convertBitmapToByteArray(bitmap: Bitmap): ByteArray {
        var baos: ByteArrayOutputStream? = null
        try {
            baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            return baos.toByteArray()
        } finally {
            baos?.let {
                try {
                    it.close()
                } catch (e: IOException) {
                    Log.e(BitmapUtils::class.java.simpleName, "ByteArrayOutputStream was not closed")
                }
            }
        }
    }

    fun convertByteArrayToBitmap(bytes: ByteArray): Bitmap {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
    }
}