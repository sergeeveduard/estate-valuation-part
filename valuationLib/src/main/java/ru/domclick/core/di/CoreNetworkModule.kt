package ru.domclick.core.di

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.CookieJar
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.domclick.core.di.qualifier.NoCas
import ru.domclick.core.di.qualifier.WithCas
import ru.domclick.core.extentions.applyDebugOnlyLogging
import ru.domclick.core.net.*
import ru.domclick.core.utils.GsonFactory
import javax.inject.Singleton

@Module
internal class CoreNetworkModule {

    @Provides
    @Singleton
    internal fun provideCookieJar(): CookieJar = MemoryCookieJar()

    @Provides
    @Singleton
    internal fun provideOkHttpClientBuilder(cookieJar: CookieJar,
                                            userAgentInterceptor: UserAgentInterceptor
    ): OkHttpClient.Builder {

        return OkHttpClient.Builder()
                .addNetworkInterceptor(userAgentInterceptor)
                .cookieJar(cookieJar)
    }

    @Provides
    @Singleton
    @NoCas
    internal fun provideHttpClientNoCas(builder: OkHttpClient.Builder,
                                        baseUrlInterceptor: BaseUrlInterceptor): OkHttpClient {
        return builder
                .applyDebugOnlyLogging(HttpLoggingInterceptor.Level.BODY)
                .addInterceptor(baseUrlInterceptor)
                .build()
    }

    @Provides
    @Singleton
    @WithCas
    internal fun provideHttpClientWithCas(authInterceptor: AuthInterceptor,
                                          baseUrlInterceptor: BaseUrlInterceptor,
                                          builder: OkHttpClient.Builder
    ): OkHttpClient {

        return builder
                .applyDebugOnlyLogging(HttpLoggingInterceptor.Level.BODY)
                .addInterceptor(baseUrlInterceptor)
                .addInterceptor(authInterceptor)
                .build()
    }

    @Provides
    @Singleton
    @WithCas
    internal fun provideApiBuilderWithCas(gsonConverterFactory: Converter.Factory,
                                          callAdapterFactory: RxJava2CallAdapterFactory,
                                          @WithCas httpClient: OkHttpClient
    ): Retrofit.Builder {

        return internalProvideApiBuilder(gsonConverterFactory, callAdapterFactory, httpClient)
    }

    @Provides
    @Singleton
    @NoCas
    internal fun provideApiBuilderNoCas(gsonConverterFactory: Converter.Factory,
                                        callAdapterFactory: RxJava2CallAdapterFactory,
                                        @NoCas httpClient: OkHttpClient
    ): Retrofit.Builder {

        return internalProvideApiBuilder(gsonConverterFactory, callAdapterFactory, httpClient)
    }

    @Provides
    @Singleton
    internal fun provideRxCallAdapterFactory(): RxJava2CallAdapterFactory {
        return RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())
    }

    @Provides
    @Singleton
    internal fun provideGsonConverterFactory(gson: Gson): Converter.Factory = GsonConverterFactory.create(gson)

    @Provides
    @Singleton
    internal fun provideGson(): Gson = GsonFactory.get()

    private fun internalProvideApiBuilder(gsonConverterFactory: Converter.Factory,
                                          callAdapterFactory: RxJava2CallAdapterFactory,
                                          httpClient: OkHttpClient
    ): Retrofit.Builder {

        return Retrofit.Builder()
                .baseUrl(NetConfig.BASE_URL)
                .client(httpClient)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(callAdapterFactory)
    }
}