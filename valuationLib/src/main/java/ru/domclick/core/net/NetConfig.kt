package ru.domclick.core.net

import ru.domclick.valuation.lib.BuildConfig

object NetConfig {
    const val BASE_URL = BuildConfig.PROD_API_BASE_URL
}