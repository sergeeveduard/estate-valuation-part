package ru.domclick.core.net

import android.os.Build
import okhttp3.Interceptor
import okhttp3.Response
import ru.domclick.core.extentions.limit
import javax.inject.Inject
import javax.inject.Singleton

private const val USER_AGENT = "User-Agent"

/**
 * Copied from domclick
 */
@Singleton
class UserAgentInterceptor
@Inject constructor() : Interceptor {

    private val value = StringBuilder("Android; ")
            .append(Build.VERSION.RELEASE.limit(10)).append("; ")
            .append(Build.MANUFACTURER.limit(10)).append("; ")
            .append(Build.MODEL.limit(20)).append("; ")
            .toString()

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
                .newBuilder()
                .removeHeader(USER_AGENT)
                .addHeader(USER_AGENT, value)
                .build()
        return chain.proceed(request)
    }
}