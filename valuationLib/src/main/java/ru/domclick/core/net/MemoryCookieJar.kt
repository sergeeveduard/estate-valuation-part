package ru.domclick.core.net

import okhttp3.Cookie
import okhttp3.CookieJar
import okhttp3.HttpUrl
import java.util.ArrayList
import java.util.concurrent.ConcurrentHashMap
import kotlin.collections.HashMap
import kotlin.collections.set

/**
 * Copied from domclick
 */
internal class MemoryCookieJar : CookieJar {

    private val cookieStore = ConcurrentHashMap<String, Map<String, Cookie>>()

    override fun saveFromResponse(url: HttpUrl, cookies: List<Cookie>) {
        //todo store only anti-ddos related cookies

        val host = url.host()
        val oldCookie = cookieStore[host]
        val newCookie = if (oldCookie == null) HashMap() else HashMap(oldCookie)
        cookies.forEach {
            newCookie[it.name()] = it
        }
        cookieStore.put(host, newCookie)
    }

    override fun loadForRequest(url: HttpUrl): List<Cookie> {
        return cookieStore[url.host()]?.values?.toList() ?: ArrayList()
    }
}
