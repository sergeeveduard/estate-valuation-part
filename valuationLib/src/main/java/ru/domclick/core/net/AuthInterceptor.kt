package ru.domclick.core.net

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response
import ru.domclick.core.exception.BadTgtException
import ru.domclick.core.repo.SessionRepo
import java.io.IOException
import javax.inject.Inject


/**
 * Copied from domclick
 */
class AuthInterceptor
@Inject constructor(private val sessionRepo: SessionRepo) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {

        val sessionId = sessionRepo.getSession()

        if (sessionId.isNullOrEmpty()) {
            throw BadTgtException()
        }
        Log.d("AuthInterceptor", "Injecting Cookie: $sessionId")

        val request = chain.request().newBuilder()
                .removeHeader("Cookie")
                .addHeader("Cookie", sessionId!!).build()
        val response = chain.proceed(request)

        checkRedirectToCasLogin(response)

        return response
    }

    private fun checkRedirectToCasLogin(response: Response) {
        //todo server should ask relogin via 401 "soon", elsewhere optimize this code - don`t load html login page, reask ST only

        val location = response.priorResponse()?.header("Location") ?: return
        if (location.contains("/cas/login"))
            throw BadTgtException()
    }
}
