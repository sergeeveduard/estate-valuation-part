package ru.domclick.core.net

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Response
import ru.domclick.core.repo.HostRepo
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class BaseUrlInterceptor
@Inject constructor(private val hostRepo: HostRepo) : Interceptor {

    private var host: String? = null
    private var scheme: String? = null

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        val newHost = hostRepo.currentHost
        if (newHost.isNotEmpty()) {
            invalidateHostAndSchema(newHost)
            val newUrl = request.url().newBuilder()
                    .scheme(scheme!!)
                    .host(host!!)
                    .build()
            request = request.newBuilder()
                    .url(newUrl)
                    .build()
        }

        return chain.proceed(request)
    }

    private fun invalidateHostAndSchema(newHost: String) {
        if (newHost != host) {
            val temp = HttpUrl.parse(newHost)!!
            host = temp.host()
            scheme = temp.scheme()
        }
    }
}