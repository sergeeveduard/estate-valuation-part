package ru.domclick.core.ui.widget;

import android.text.TextWatcher;


public abstract class ObservableTextWatcher implements TextWatcher {

    public interface TextListener {
        void update(String value);
    }

    protected TextListener listener;

    public void addTextListener(TextListener listener) {
        this.listener = listener;
    }

}
