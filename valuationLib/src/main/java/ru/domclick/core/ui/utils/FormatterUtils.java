package ru.domclick.core.ui.utils;


import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Pattern;


/**
 * copied from domclick
 */
public class FormatterUtils {

    public static final int CARD_MASK_LENGTH = 4;
    public static final String PHONE_NUMBER_MASK = "+# (###) ###-##-##";
    public static final String PHONE_NUMBER_MASK_MASKED = "+# (###) \'*\'*\'*-##-##";
    public static final String DATE_MASK = "##.##.####";

    public static final String DATE_FORMAT_SERVER = "yyyy-MM-dd";
    public static final String DATE_FORMAT_SERVER_REALTY = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_TIME_FORMAT_SERVER = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String DATE_FORMAT = "dd.MM.yyyy";

    public static final Pattern EMAIL_REGEXP = Pattern.compile("^[a-zA-Z0-9_-]+(?:\\" +
            ".[a-zA-Z0-9_-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9]" +
            "(?:[a-zA-Z0-9-]*[a-zA-Z0-9]){1,}$");
    public static final String COUNTRY_CODE = "+7";
    public static final int MAX_PHONE_NUMBER_LENGTH = 11;
    public static final SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT);
    public static final Gson gson = new GsonBuilder()
            .setDateFormat(FormatterUtils.DATE_FORMAT_SERVER_REALTY)
            .create();
    private static final Pattern INN_REGEXP = Pattern.compile("^[0-9]{10}$");
    private static final Pattern INN_REGEXP_IP = Pattern.compile("^[0-9]{12}$");
    private static final DecimalFormatSymbols decimalSymbols;

    static {
        decimalSymbols = new DecimalFormatSymbols(Locale.FRENCH);
        decimalSymbols.setDecimalSeparator(',');
    }

    public static String keepDigitsOnly(String text) {
        return getNumberValue(text, '\0');
    }

    public static String getNumberValue(String text, char divider) {
        if (TextUtils.isEmpty(text))
            return "";

        StringBuilder sb = new StringBuilder();
        for (int idx = 0, count = text.length(); idx < count; ++idx) {
            char chr = text.charAt(idx);

            if (chr == divider || Character.isDigit(chr)) {
                sb.append(chr);
            } else {
                continue;
            }
        }

        return sb.toString();
    }

    public static String formatDigitsPhone(String fullPhone) {
        String value = FormatterUtils.keepDigitsOnly(fullPhone);
        if (!TextUtils.isEmpty(value)) {
            return value.substring(1, value.length());
        }

        return value;
    }
}
