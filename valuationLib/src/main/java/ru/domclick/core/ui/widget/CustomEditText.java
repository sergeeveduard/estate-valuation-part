package ru.domclick.core.ui.widget;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.widget.AppCompatEditText;


/**
 * Copied from domclick
 */
public class CustomEditText extends AppCompatEditText {
    private Context context;
    private CustomInputConnection inputConnection;

    //private OnKeyEventListener keyEventListener;
    private OnBackspaceEventListener backspaceEventListener;

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setCursorVisible(false);
    }

    public void setBackground(int colorRes) {
        getBackground().setTint(colorRes);
    }

    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        outAttrs.inputType = InputType.TYPE_CLASS_NUMBER;

        inputConnection = new CustomInputConnection(this, false);

        inputConnection.setOnKeyEventListener(new OnKeyEventListener() {
            @Override
            public void OnKeyEvent(KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_DEL) {
                    if (backspaceEventListener != null) {
                        backspaceEventListener.OnBackspaceEvent();
                    }
                }
            }
        });

        inputConnection.setOnBackspaceEventListener(new OnBackspaceEventListener() {
            @Override
            public void OnBackspaceEvent() {
                if (backspaceEventListener != null) {
                    backspaceEventListener.OnBackspaceEvent();
                }
            }
        });

        return inputConnection;
    }

    public void setOnBackspaceEventListener(OnBackspaceEventListener backspaceEventListener) {
        this.backspaceEventListener = backspaceEventListener;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            //noinspection ConstantConditions
            imm.showSoftInput(this, InputMethodManager.SHOW_FORCED);
        }
        return false;
    }


    public static abstract class OnKeyEventListener {
        public abstract void OnKeyEvent(KeyEvent event);
    }

    public static abstract class OnBackspaceEventListener {
        public abstract void OnBackspaceEvent();
    }

    class CustomInputConnection extends BaseInputConnection {

        private OnKeyEventListener keyEventListener;
        private OnBackspaceEventListener backspaceEventListener;

        public CustomInputConnection(View targetView, boolean fullEditor) {
            super(targetView, fullEditor);
        }

        @Override
        public boolean sendKeyEvent(KeyEvent event) {
            if (keyEventListener != null) {
                keyEventListener.OnKeyEvent(event);
            }

            return super.sendKeyEvent(event);
        }

        @Override
        public boolean deleteSurroundingText(int beforeLength, int afterLength) {
            if (backspaceEventListener != null) {
                backspaceEventListener.OnBackspaceEvent();
            }

            return super.deleteSurroundingText(beforeLength, afterLength);
        }

        public void setOnKeyEventListener(OnKeyEventListener keyEventListener) {
            this.keyEventListener = keyEventListener;
        }

        public void setOnBackspaceEventListener(OnBackspaceEventListener backspaceEventListener) {
            this.backspaceEventListener = backspaceEventListener;
        }

    }
}
