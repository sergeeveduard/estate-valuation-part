package ru.domclick.core.ui.widget;

import android.text.Editable;
import android.text.Selection;
import android.view.View;

import java.text.ParseException;

import ru.domclick.core.ui.utils.FormatterUtils;
import ru.domclick.core.ui.utils.MaskFormatter;

public class PhoneNumberWatcher extends ObservableTextWatcher {
    private boolean mAllowFormat = true;
    private boolean isFull = false;

    private View nextView;
    private TextListener listener;
    private boolean emptyOnStart = false;

    public PhoneNumberWatcher(TextListener listener) {
        this.listener = listener;
    }

    public PhoneNumberWatcher(boolean emptyOnStart) {
        this.emptyOnStart = emptyOnStart;
    }

    public void addTextlistener(TextListener listener) {
        this.listener = listener;
    }

    public void setNextView(View nextView) {
        this.nextView = nextView;
    }

    public String getMask() {
        return FormatterUtils.PHONE_NUMBER_MASK;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        isFull = false;
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (!mAllowFormat)
            return;

        mAllowFormat = false;
        {
            String value = FormatterUtils.keepDigitsOnly(s.toString());
            int length = value.length();

            if (!emptyOnStart && s.length() <= 1) {
                s.replace(0, s.length(), FormatterUtils.COUNTRY_CODE);
                Selection.setSelection(s, s.length());
                mAllowFormat = true;
                return;
            }

            if (length > 0 && value.charAt(0) != '7') {
                value = "7" + value;
            } else if (length > FormatterUtils.MAX_PHONE_NUMBER_LENGTH && value.charAt(0) == '7') {
                value = value.substring(0, FormatterUtils.MAX_PHONE_NUMBER_LENGTH);
            }

            isFull = value.length() == FormatterUtils.MAX_PHONE_NUMBER_LENGTH;

            try {
                MaskFormatter formatter = new MaskFormatter(this.getMask());
                formatter.setValueContainsLiteralCharacters(false);

                value = formatter.valueToString(value);
                value = value.substring(0, indexOfLastDigit(value));
            } catch (ParseException e) {
                value = value.substring(0, e.getErrorOffset());
            }

            s.replace(0, s.length(), value);

            if (listener != null) {
                listener.update(value);
            }

            if (isFull) {
                if (nextView != null) {
                    nextView.requestFocus();
                } else {
                    if (emptyOnStart && s.length() <= 1) {
                        s.replace(0, s.length(), FormatterUtils.COUNTRY_CODE);
                        Selection.setSelection(s, s.length());
                        mAllowFormat = true;
                        return;
                    }
                    Selection.setSelection(s, s.length());
                }
            }
        }
        mAllowFormat = true;
    }


    public int indexOfLastDigit(String text) {
        int position = 0;
        for (int idx = text.length() - 1; idx >= 0; --idx) {
            if (Character.isDigit(text.charAt(idx))) {
                position = idx + 1;
                break;
            }
        }
        return position;
    }

    public boolean isFull() {
        return isFull;
    }
}
