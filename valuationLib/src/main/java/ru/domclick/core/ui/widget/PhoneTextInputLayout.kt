package ru.domclick.core.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout
import ru.domclick.core.ui.utils.FormatterUtils

class PhoneTextInputLayout(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
) : TextInputLayout(context, attrs, defStyleAttr) {

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context) : this(context, null)

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        super.addView(child, index, params)
        if (child is EditText) {
            editText!!.addTextChangedListener(PhoneNumberWatcher(true))
        }
    }

    fun getRawPhoneNumber(): String {
        return FormatterUtils.formatDigitsPhone(editText!!.text.toString())
    }
}