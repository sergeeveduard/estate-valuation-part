package ru.domclick.core.ui.widget

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import ru.domclick.valuation.lib.R

/**
 * copied from domclick
 */
class LoadingView(context: Context, val attrs: AttributeSet?) : FrameLayout(context, attrs) {
    constructor(context: Context) : this(context, null)

    init {
        View.inflate(context, R.layout.custom_view_loading, this)

        if (attrs == null) {
            setMessage(null)
        } else {
            val values = context.obtainStyledAttributes(attrs, R.styleable.LoadingView, 0, 0)
            try {
                val message = values.getString(R.styleable.LoadingView_message)
                setMessage(message)
            } finally {
                values.recycle()
            }
        }
    }

    fun setMessage(message: String?) {
        val messageView = findViewById<TextView>(R.id.message) ?: return
        val aroundMessage = findViewById<View>(R.id.loadingLayout) ?: return

        if (message == null) {
            aroundMessage.setBackgroundColor(Color.TRANSPARENT)
            messageView.text = message
            messageView.visibility = View.GONE
        } else {
            aroundMessage.setBackgroundColor(Color.WHITE)
            messageView.text = message
            messageView.visibility = View.VISIBLE
        }
    }
}