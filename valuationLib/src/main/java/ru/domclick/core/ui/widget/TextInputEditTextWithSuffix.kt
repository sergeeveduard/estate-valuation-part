package ru.domclick.core.ui.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.text.TextPaint
import android.util.AttributeSet
import com.google.android.material.textfield.TextInputEditText


class TextInputEditTextWithSuffix(context: Context,
                                  attrs: AttributeSet?,
                                  defStyleAttr: Int)
    : TextInputEditText(context, attrs, defStyleAttr) {

    private var suffixPaint = TextPaint()
    private var suffixValue: String? = null
    private var showSuffixOnEmptyTextValue = false

    var suffix: String?
        get() = suffixValue
        set(value) {
            suffixValue = value
            invalidate()
        }

    var showSuffixOnEmptyText: Boolean
        get() = showSuffixOnEmptyTextValue
        set(value) {
            showSuffixOnEmptyTextValue = value
            invalidate()
        }


    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, com.google.android.material.R.attr.editTextStyle)
    constructor(context: Context) : this(context, null)


    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        if ((showSuffixOnEmptyText || !text.isNullOrEmpty()) && !suffix.isNullOrEmpty()) {

            suffixPaint.color = currentHintTextColor
            suffixPaint.textSize = textSize
            suffixPaint.textAlign = Paint.Align.LEFT

            val mLine0Baseline = getLineBounds(0, null).toFloat()

            val suffixXPos = suffixPaint.measureText(text.toString()) + paddingLeft

            canvas.drawText(suffix!!, suffixXPos, mLine0Baseline, suffixPaint)
        }
    }
}