package ru.domclick.core.ui.widget;


import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.domclick.valuation.lib.R;

/**
 * Copied from domclick
 */
public class SmsCodeView extends LinearLayout {

    CustomEditText smsCode1;
    CustomEditText smsCode2;
    CustomEditText smsCode3;
    CustomEditText smsCode4;
    TextView errorView;
    private Context context;
    private boolean isError = false;

    private OnCodeChangeListener codeCompleteListener;
    private OnFocusChangeListener focusListener = new OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final CustomEditText view = (CustomEditText) v;
            if (hasFocus) {
                view.setText("");
            }
        }
    };

    public SmsCodeView(Context context) {
        super(context);

        this.context = context;
        init();
    }

    public SmsCodeView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        init();
        initListener();
    }

    private void init() {
        inflate(context, R.layout.custom_view_sms, this);

        smsCode1 = findViewById(R.id.sms_code_1);
        smsCode2 = findViewById(R.id.sms_code_2);
        smsCode3 = findViewById(R.id.sms_code_3);
        smsCode4 = findViewById(R.id.sms_code_4);
        errorView = findViewById(R.id.sms_error);

        smsCode1.setFocusableInTouchMode(true);
        setBackground(R.color.colorPrimary);
    }

    private void initListener() {
        smsCode1.addTextChangedListener(new CodeWatcher(smsCode2));
        smsCode2.addTextChangedListener(new CodeWatcher(smsCode3));
        smsCode3.addTextChangedListener(new CodeWatcher(smsCode4));
        smsCode4.addTextChangedListener(new CodeWatcher(smsCode4));

        smsCode1.setOnFocusChangeListener(focusListener);
        smsCode2.setOnFocusChangeListener(focusListener);
        smsCode3.setOnFocusChangeListener(focusListener);
        smsCode4.setOnFocusChangeListener(focusListener);

        smsCode1.setOnBackspaceEventListener(new CustomEditText.OnBackspaceEventListener() {
            @Override
            public void OnBackspaceEvent() {
                smsCode1.setText("");
            }
        });

        smsCode2.setOnBackspaceEventListener(new CustomEditText.OnBackspaceEventListener() {
            @Override
            public void OnBackspaceEvent() {
                if (smsCode2.getText().length() == 0) {
                    smsCode1.requestFocus();
                    smsCode1.setSelection(smsCode1.getText().length());
                }
            }
        });

        smsCode3.setOnBackspaceEventListener(new CustomEditText.OnBackspaceEventListener() {
            @Override
            public void OnBackspaceEvent() {
                if (smsCode3.getText().length() == 0) {
                    smsCode2.requestFocus();
                    smsCode2.setSelection(smsCode2.getText().length());
                }
            }
        });

        smsCode4.setOnBackspaceEventListener(new CustomEditText.OnBackspaceEventListener() {
            @Override
            public void OnBackspaceEvent() {
                if (smsCode4.getText().length() == 0) {
                    smsCode3.requestFocus();
                    smsCode3.setSelection(smsCode3.getText().length());
                } else {
                    smsCode4.setText("");
                    if (isError) {
                        showErrorCode(false);
                    }
                }
            }
        });
    }

    public void showSoftKeyboard() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                int count = getText().length();
                switch (count) {
                    case 0:
                        smsCode1.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                        break;
                    case 1:
                        smsCode2.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                        break;
                    case 2:
                        smsCode3.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                        break;
                    case 3:
                    case 4:
                        smsCode4.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                        break;
                }
            }
        }, 100);
    }

    public void setOnCodeCompleteListener(OnCodeChangeListener numberCompleteListener) {
        this.codeCompleteListener = numberCompleteListener;
    }

    public void showErrorCode(boolean visibility) {
        boolean needAnimate = (isError != visibility);
        isError = visibility;
        if (needAnimate) {
            animationError(visibility);
        }
    }

    public String getText() {
        return smsCode1.getText().toString().trim() +
                smsCode2.getText().toString().trim() +
                smsCode3.getText().toString().trim() +
                smsCode4.getText().toString().trim();
    }

    public void clear() {
        smsCode1.setText("");
        smsCode2.setText("");
        smsCode3.setText("");
        smsCode4.setText("");
        smsCode1.requestFocus();
        setBackground(R.color.colorPrimary);
        errorView.setVisibility(INVISIBLE);
    }

    public boolean isValid() {
        return smsCode1.getText().length() > 0
                && smsCode2.getText().length() > 0
                && smsCode3.getText().length() > 0
                && smsCode4.getText().length() > 0;
    }

    private void setBackground(int color) {
        smsCode1.setBackground(color);
        smsCode2.setBackground(color);
        smsCode3.setBackground(color);
        smsCode4.setBackground(color);
    }

    private void animationError(boolean visibility) {
        final Animation animationFadeOut = new AlphaAnimation(1f, 0f);
        final Animation animationFadeIn = new AlphaAnimation(0f, 1f);

        final int duration = 600;
        animationFadeOut.setDuration(duration);
        animationFadeIn.setDuration(duration);

        if (visibility) {
            errorView.startAnimation(animationFadeIn);
            animationFadeIn.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    setBackground(R.color.orange_pink);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    errorView.setVisibility(VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        } else {
            errorView.startAnimation(animationFadeOut);
            animationFadeOut.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    setBackground(R.color.colorPrimary);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    errorView.setVisibility(INVISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
        }
    }


    public static abstract class OnCodeChangeListener {
        public abstract void OnCodeComplete(boolean isComplete);
    }

    class CodeWatcher implements TextWatcher {
        CustomEditText nextView;

        CodeWatcher(CustomEditText nextView) {
            this.nextView = nextView;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (codeCompleteListener != null) {
                codeCompleteListener.OnCodeComplete(isValid());
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() == 1) {
                nextView.requestFocus();
            }
        }
    }
}
