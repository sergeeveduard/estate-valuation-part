package ru.domclick.core.ui.widget

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.widget.TextView
import com.google.android.material.textfield.TextInputEditText


/**
 * Fix crashes on Meizu phones
 * https://console.firebase.google.com/project/photographer-2599d/crashlytics/app/android:ru.domclick.valuation/issues/5bcf4705f8b88c2963e42718?time=last-seven-days&sessionId=5BD1652A009400012336445B44A6CC4B_DNE_0_v2
 *
 * Thanks [https://github.com/android-in-china/Compatibility/issues/11]
 */
class FixedTextInputEditText(context: Context, attrs: AttributeSet?) : TextInputEditText(context, attrs) {

    override fun getHint(): CharSequence? {
        val manufacturer = Build.MANUFACTURER.toUpperCase()
        return if (!manufacturer.contains("MEIZU") || Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            super.getHint()
        } else {
            try {
                return getSuperHintHack()
            } catch (e: Exception) {
                super.getHint()
            }
        }
    }

    private fun getSuperHintHack(): CharSequence? {
        val f = TextView::class.java.getDeclaredField("mHint")
        f.isAccessible = true
        return f.get(this) as? CharSequence
    }
}