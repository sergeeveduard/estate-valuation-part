package ru.domclick.core.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.view.ViewPropertyAnimator
import android.widget.EditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.TextViewCompat
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import com.google.android.material.textfield.TextInputLayout
import ru.domclick.valuation.lib.R


/**
 * [TextInputLayout] with helper text.
 *
 * Source from https://github.com/Widgetlabs/expedition-extendedtextinputlayout/blob/master/app/src/main/java/everald/github/com/extendedtextinputlayout/ExtendedTextInputLayout.kt
 */
class TextInputLayoutWithHelper : TextInputLayout {

    private var _errorTextEnabled = false
    private var _helperText: CharSequence? = ""
    private var _helperTextAnimator: ViewPropertyAnimator? = null
    private var _helperTextAppearance = R.style.HelperTextAppearance
    private var _helperTextEnabled = false
    private var _helperTextView = AppCompatTextView(context).apply {
        TextViewCompat.setTextAppearance(this, _helperTextAppearance)
        this@TextInputLayoutWithHelper.addView(this)
    }

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun setHelperText(value: CharSequence?) {
        if (_helperText == value) {
            return
        }
        _helperText = value
        _helperTextEnabled = value?.isNotBlank() ?: false
        setHelperTextOnHelperView(value)
    }

    override fun getHelperText(): CharSequence? = _helperText

    override fun setHelperTextEnabled(value: Boolean) {
        if (_helperTextEnabled == value) return
        _helperTextEnabled = value
        switchHelperText()
    }

    override fun isHelperTextEnabled(): Boolean = _helperTextEnabled

    override fun addView(child: View?, params: ViewGroup.LayoutParams?) {
        super.addView(child, params)
        if (child is EditText) {
            _helperTextView.setPaddingRelative(child.paddingStart, 0, child.paddingEnd, child.paddingBottom)
        }
    }

    override fun setErrorEnabled(enabled: Boolean) {
        if (_errorTextEnabled == enabled) return
        _errorTextEnabled = enabled

        if (_errorTextEnabled && _helperTextEnabled) {
            switchHelperText()
        }

        super.setErrorEnabled(enabled)

        if (!_errorTextEnabled) {
            switchHelperText()
        }
    }

    override fun setError(error: CharSequence?) {
        super.setError(error)
        if (error.isNullOrEmpty() && isErrorEnabled && _helperTextEnabled) {
            isErrorEnabled = false
        }
    }

    private fun setHelperTextOnHelperView(text: CharSequence?) {
        _helperTextAnimator?.cancel()
        if (text.isNullOrBlank()) {
            _helperTextView.visibility = View.GONE
            if (_helperTextView.text == text) return
            _helperTextAnimator = _helperTextView.animate()
                    .setInterpolator(Interpolator)
                    .alpha(0f)
                    .setDuration(200)
                    .withEndAction {
                        _helperTextView.text = null
                    }

        } else {
            _helperTextView.visibility = View.VISIBLE
            _helperTextView.text = text
            _helperTextAnimator = _helperTextView.animate()
                    .setInterpolator(Interpolator)
                    .alpha(1f)
                    .setDuration(200)
        }
        _helperTextAnimator?.start()
    }

    private fun switchHelperText() {
        if (_errorTextEnabled || !_helperTextEnabled) { // hide helper text
            setHelperTextOnHelperView(null)
        } else if (!_errorTextEnabled && _helperTextEnabled) { // if there is a helper text, show it
            if (helperText?.isNotBlank() == true) {
                setHelperTextOnHelperView(helperText)
            } else {
                setHelperTextOnHelperView(null)
            }
        }
    }

    companion object {
        val Interpolator = FastOutSlowInInterpolator()
    }
}