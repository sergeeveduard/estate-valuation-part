package ru.domclick.core.extentions

/**
 * Copied from domclick
 */
fun String.limit(n: Int) = if (length < n) this else substring(0, n)

/**
 * returns ""
 */
fun String.Companion.empty() = ""