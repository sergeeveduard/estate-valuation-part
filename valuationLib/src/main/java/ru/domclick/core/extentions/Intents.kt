package ru.domclick.core.extentions

import android.content.Intent

fun Intent.clearActivityStack(): Intent {
    this.flags = (Intent.FLAG_ACTIVITY_NEW_TASK
            or Intent.FLAG_ACTIVITY_CLEAR_TOP
            or Intent.FLAG_ACTIVITY_CLEAR_TASK)
    return this
}