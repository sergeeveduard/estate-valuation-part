package ru.domclick.core.extentions

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import ru.domclick.valuation.common.analytic.Analytics
import ru.domclick.valuation.common.ui.ValuationApplication
import java.util.concurrent.TimeUnit

fun View.hideKeyboard(): Boolean {
    val imm = this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    return imm.hideSoftInputFromWindow(this.windowToken, 0)
}

fun View.showKeyboard() {
    val imm = this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}

fun TextView.addOnTextChanged(l: (String) -> Unit) {

    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            l(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    })
}

fun View.visibleOrGone(isVisible: Boolean) {
    this.visibility = if (isVisible) View.VISIBLE else View.GONE
}

val View.analytics: Analytics
    get() {
        val app = this.context.applicationContext as ValuationApplication
        return app.analytics
    }

val View.onTouchAndHold: Observable<View>
    get() {
        val repeatRate = 100L
        val observable = PublishSubject.create<View>()
        val interval = Observable.interval(repeatRate, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { observable.onNext(this) }

        var disposable: Disposable? = null

        this.setOnTouchListener { _, event ->

            when (event.actionMasked) {
                MotionEvent.ACTION_DOWN -> disposable = interval.subscribe()

                MotionEvent.ACTION_UP,
                MotionEvent.ACTION_CANCEL -> disposable?.dispose()
            }
            true
        }
        return observable
    }