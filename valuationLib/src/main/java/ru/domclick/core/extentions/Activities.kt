package ru.domclick.core.extentions

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.view.View
import ru.domclick.valuation.lib.R

fun androidx.fragment.app.FragmentActivity.replaceFragment(fragment: androidx.fragment.app.Fragment,
                                                           tag: String,
                                                           addBackStack: Boolean = true,
                                                           useSlideAnimation: Boolean = false) {

    val transaction = this.supportFragmentManager.beginTransaction()

    if (useSlideAnimation) {
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
    } else {
        transaction.setTransition(androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
    }

    transaction.replace(R.id.fragmentContainer, fragment, tag)

    if (addBackStack) {
        transaction.addToBackStack(null)
    }

    transaction.commit()
}

fun Activity.openDial(phoneNumber: String) {
    val uri = "tel:" + phoneNumber.trim()
    val intent = Intent(Intent.ACTION_DIAL).apply {
        data = Uri.parse(uri)
    }
    this.startActivity(intent)
}

fun Activity.hideSystemUI() {
    // Enables regular immersive mode.
    // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
    // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
    window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
            // Set the content to appear under the system bars so that the
            // content doesn't resize when the system bars hide and show.
            or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            // Hide the nav bar and status bar
            or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_FULLSCREEN)
}