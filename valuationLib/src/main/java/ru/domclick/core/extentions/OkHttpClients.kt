package ru.domclick.core.extentions

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import ru.domclick.valuation.lib.BuildConfig

/**
 * Copied from domclick
 */
fun OkHttpClient.Builder.applyDebugOnlyLogging(level: HttpLoggingInterceptor.Level): OkHttpClient.Builder {
    if (BuildConfig.DEBUG) {
        this.addInterceptor(HttpLoggingInterceptor().setLevel(level))
    }
    return this
}
