package ru.domclick.core.extentions

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

fun Disposable.disposeOnDestroyView(disposableHolder: DisposableHolder): Disposable {
    disposableHolder.addDisposable(this)
    return this
}

fun Disposable.addTo(disposable: CompositeDisposable): Disposable {
    disposable.add(this)
    return this
}

interface DisposableHolder {
    fun addDisposable(disposable: Disposable)
}

/**
 * Для создания rx observable с не null параметром
 */
object RxNothing