package ru.domclick.core.extentions

import android.view.View
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar

fun showSnackbar(view: View, text: CharSequence) {
    Snackbar.make(view, text, Snackbar.LENGTH_LONG).show()
}

fun showSnackbar(view: View, @StringRes textId: Int) {
    Snackbar.make(view, textId, Snackbar.LENGTH_LONG).show()
}