package ru.domclick.core.extentions

import java.io.File

fun File.delete(onError: (Exception) -> Unit) {
    try {
        this.delete()
    } catch (e: Exception) {
        onError(e)
    }
}