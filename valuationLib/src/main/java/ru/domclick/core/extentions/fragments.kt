package ru.domclick.core.extentions

import android.view.View
import androidx.fragment.app.Fragment

val Fragment.activityView: View?
    get() = this.activity?.window?.decorView