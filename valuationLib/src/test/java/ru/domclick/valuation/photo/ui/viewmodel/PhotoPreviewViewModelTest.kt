package ru.domclick.valuation.photo.ui.viewmodel

import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyLong
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.RxScheduleRule
import ru.domclick.valuation.photo.data.repo.PhotoRepo
import ru.domclick.valuation.photo.domain.GetPhotosUseCase
import ru.domclick.valuation.photo.domain.model.Photo

/**
 * [PhotoPreviewViewModel]
 */
class PhotoPreviewViewModelTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    val schedulerRule = RxScheduleRule()

    @Mock
    private lateinit var getPhotosUseCase: GetPhotosUseCase

    @Mock
    private lateinit var photoRepo: PhotoRepo

    @InjectMocks
    private lateinit var photoPreviewViewModel: PhotoPreviewViewModel

    private var photoId = 0L
    private val initialPhotos = listOf(
            Photo(++photoId, "file"),
            Photo(++photoId, "file"),
            Photo(++photoId, "file"),
            Photo(++photoId, "file"),
            Photo(++photoId, "file")
    )
    private val currentIndex = 2

    @Before
    fun setUp() {
        `when`(photoRepo.delete(anyLong())).thenReturn(Completable.complete())
        `when`(getPhotosUseCase.execute(anyLong())).thenReturn(Single.just(initialPhotos))

        photoPreviewViewModel.currentPhotoId = initialPhotos[currentIndex].id
    }

    @Test
    fun loadSuccess() {
        val photos = photoPreviewViewModel.photos.test()
        val title = photoPreviewViewModel.title.test()
        val currentPosition = photoPreviewViewModel.currentPosition.test()
        val onEmpty = photoPreviewViewModel.onEmpty.test()
        val onError = photoPreviewViewModel.onError.test()


        photoPreviewViewModel.loadPhotos(123)

        photos.assertValue { l -> l.size == initialPhotos.size }
        title.assertValueCount(1)
        currentPosition.assertValue(currentIndex)
        onEmpty.assertNoValues()
        onError.assertNoValues()
    }

    @Test
    fun loadEmpty() {
        `when`(getPhotosUseCase.execute(anyLong())).thenReturn(Single.just(emptyList()))

        val photos = photoPreviewViewModel.photos.test()
        val title = photoPreviewViewModel.title.test()
        val currentPosition = photoPreviewViewModel.currentPosition.test()
        val onEmpty = photoPreviewViewModel.onEmpty.test()
        val onError = photoPreviewViewModel.onError.test()


        photoPreviewViewModel.loadPhotos(123)

        photos.assertNoValues()
        title.assertNoValues()
        currentPosition.assertNoValues()
        onEmpty.assertValueCount(1)
        onError.assertNoValues()
    }

    @Test
    fun deleteMiddle() {
        photoPreviewViewModel.loadPhotos(123)

        val photos = photoPreviewViewModel.photos.test()
        val title = photoPreviewViewModel.title.test()
        val currentPosition = photoPreviewViewModel.currentPosition.test()
        val onEmpty = photoPreviewViewModel.onEmpty.test()
        val onError = photoPreviewViewModel.onError.test()


        photoPreviewViewModel.delete()

        photos.assertValue { l -> l.size == initialPhotos.size - 1 }
        title.assertValueCount(1)
        currentPosition.assertValue(currentIndex - 1)
        onEmpty.assertNoValues()
        onError.assertNoValues()
    }

    @Test
    fun deleteLast() {
        photoPreviewViewModel.loadPhotos(123)

        val newCurrentIndex = initialPhotos.size - 1
        photoPreviewViewModel.onPageSelected(newCurrentIndex)

        val photos = photoPreviewViewModel.photos.test()
        val title = photoPreviewViewModel.title.test()
        val currentPosition = photoPreviewViewModel.currentPosition.test()
        val onEmpty = photoPreviewViewModel.onEmpty.test()
        val onError = photoPreviewViewModel.onError.test()


        photoPreviewViewModel.delete()

        photos.assertValue { l -> l.size == initialPhotos.size - 1 }
        title.assertValueCount(1)
        currentPosition.assertValue(newCurrentIndex - 1)
        onEmpty.assertNoValues()
        onError.assertNoValues()
    }

    @Test
    fun deleteAll() {
        photoPreviewViewModel.loadPhotos(123)

        for (i: Int in 1 until initialPhotos.size) {
            photoPreviewViewModel.delete()
        }

        val photos = photoPreviewViewModel.photos.test()
        val title = photoPreviewViewModel.title.test()
        val currentPosition = photoPreviewViewModel.currentPosition.test()
        val onEmpty = photoPreviewViewModel.onEmpty.test()
        val onError = photoPreviewViewModel.onError.test()


        photoPreviewViewModel.delete()

        photos.assertNoValues()
        title.assertNoValues()
        currentPosition.assertNoValues()
        onEmpty.assertValueCount(1)
        onError.assertNoValues()
    }
}