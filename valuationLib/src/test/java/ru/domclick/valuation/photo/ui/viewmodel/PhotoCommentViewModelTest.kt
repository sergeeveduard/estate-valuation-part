package ru.domclick.valuation.photo.ui.viewmodel

import io.reactivex.Completable
import io.reactivex.Maybe
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.RxScheduleRule
import ru.domclick.core.extentions.empty
import ru.domclick.valuation.photo.data.repo.CommentRepo

/**
 * [PhotoCommentViewModel]
 */
class PhotoCommentViewModelTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    val schedulerRule = RxScheduleRule()

    @Mock
    private lateinit var commentRepo: CommentRepo

    @InjectMocks
    private lateinit var photoCommentViewModel: PhotoCommentViewModel

    private val photoId1 = 1L
    private val photoComment1 = "photoComment1"
    private val commentMap = mutableMapOf<Long, String>().apply {
        put(photoId1, photoComment1)
    }

    @Before
    fun setUp() {
        `when`(commentRepo.getComment(ArgumentMatchers.anyLong())).thenAnswer { invocation ->

            val id: Long = invocation.getArgument(0) as Long

            return@thenAnswer if (commentMap.containsKey(id)) {
                Maybe.just(commentMap[id])
            } else {
                Maybe.empty()
            }
        }

        `when`(commentRepo.putComment(ArgumentMatchers.anyLong(), ArgumentMatchers.anyString())).thenAnswer { invocation ->

            val id: Long = invocation.getArgument(0) as Long
            val comment: String = invocation.getArgument(1) as String
            commentMap[id] = comment
            Completable.complete()
        }

        //simulate EditText behavior
        photoCommentViewModel.comment.subscribe(photoCommentViewModel::onCommentTextChanged)
    }

    @Test
    fun hasComment() {
        val isCommentButtonVisible = photoCommentViewModel.isCommentButtonVisible.test()
        val commentButtonState = photoCommentViewModel.commentButtonState.test()
        val comment = photoCommentViewModel.comment.test()

        photoCommentViewModel.onPhotoChanged(photoId1)

        isCommentButtonVisible.assertValue(true)
        commentButtonState.assertValue(PhotoCommentViewModel.CommentButtonState.EDIT)
        comment.assertValue(photoComment1)
    }

    @Test
    fun editComment() {
        val newComment = "newComment"
        val isCommentButtonVisible = photoCommentViewModel.isCommentButtonVisible.test()
        val commentButtonState = photoCommentViewModel.commentButtonState.test()
        val comment = photoCommentViewModel.comment.test()

        photoCommentViewModel.onPhotoChanged(photoId1)
        photoCommentViewModel.onCommentTextChanged(newComment)
        photoCommentViewModel.onCommentButtonClick()

        isCommentButtonVisible.assertValues(
                true, //initial
                true, //after edit
                true  //after save
        )
        commentButtonState.assertValues(
                PhotoCommentViewModel.CommentButtonState.EDIT,
                PhotoCommentViewModel.CommentButtonState.ADD,
                PhotoCommentViewModel.CommentButtonState.EDIT
        )
        comment.assertValue(photoComment1)
        assertEquals(newComment, commentMap[photoId1])
    }

    @Test
    fun removeComment() {
        val isCommentButtonVisible = photoCommentViewModel.isCommentButtonVisible.test()
        val commentButtonState = photoCommentViewModel.commentButtonState.test()
        val comment = photoCommentViewModel.comment.test()

        photoCommentViewModel.onPhotoChanged(photoId1)
        photoCommentViewModel.onCommentTextChanged(String.empty())
        photoCommentViewModel.onCommentButtonClick()

        isCommentButtonVisible.assertValues(
                true, //initial
                true, //after edit
                false //after apply removing comment
        )
        commentButtonState.assertValues(
                PhotoCommentViewModel.CommentButtonState.EDIT,
                PhotoCommentViewModel.CommentButtonState.ADD,
                PhotoCommentViewModel.CommentButtonState.EDIT
        )
        comment.assertValue(photoComment1)
        assertEquals(String.empty(), commentMap[photoId1])
    }

    @Test
    fun changeToPhotoWithoutComment() {
        val photoId2 = 2L
        val newComment = "newComment"
        val isCommentButtonVisible = photoCommentViewModel.isCommentButtonVisible.test()
        val commentButtonState = photoCommentViewModel.commentButtonState.test()
        val comment = photoCommentViewModel.comment.test()

        photoCommentViewModel.onPhotoChanged(photoId1)
        photoCommentViewModel.onCommentTextChanged(newComment)
        //do not click 'save'

        photoCommentViewModel.onPhotoChanged(photoId2)

        isCommentButtonVisible.assertValues(
                true, //initial
                true, //edit
                false //changed to photo without comment
        )
        commentButtonState.assertValues(
                PhotoCommentViewModel.CommentButtonState.EDIT, //initial
                PhotoCommentViewModel.CommentButtonState.ADD, //comment was changed
                PhotoCommentViewModel.CommentButtonState.EDIT //on second photo button is invisible, so in fact value doesn't matter
        )
        comment.assertValues(
                photoComment1, //initial
                String.empty()// another without comment
        )
        assertEquals(newComment, commentMap[photoId1])
        assertNull(commentMap[photoId2])
    }

    @Test
    fun changeToPhotoWithComment() {
        val photoId2 = 222L
        val comment2 = "comment2"
        commentMap[photoId2] = comment2
        val newComment = "newComment"

        val isCommentButtonVisible = photoCommentViewModel.isCommentButtonVisible.test()
        val commentButtonState = photoCommentViewModel.commentButtonState.test()
        val comment = photoCommentViewModel.comment.test()

        photoCommentViewModel.onPhotoChanged(photoId1)
        photoCommentViewModel.onCommentTextChanged(newComment)
        //do not click 'save'

        photoCommentViewModel.onPhotoChanged(photoId2)

        isCommentButtonVisible.assertValues(
                true, //initial
                true, //edit
                true //changed to photo with comment
        )
        commentButtonState.assertValues(
                PhotoCommentViewModel.CommentButtonState.EDIT, //initial
                PhotoCommentViewModel.CommentButtonState.ADD, //changed to photo without comment
                PhotoCommentViewModel.CommentButtonState.EDIT //second photo has a comment
        )
        comment.assertValues(
                photoComment1, //initial
                comment2// another comment
        )
        assertEquals(newComment, commentMap[photoId1])
    }
}