package ru.domclick.valuation.photo.domain

import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyLong
import org.mockito.junit.MockitoJUnit
import ru.domclick.valuation.database.data.dao.PhotoDao
import ru.domclick.valuation.database.data.entities.PhotoEntity

/**
 * [GetPhotosUseCase]
 */
class GetPhotosUseCaseTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Mock
    private lateinit var photoDao: PhotoDao

    @InjectMocks
    private lateinit var getPhotosUseCase: GetPhotosUseCase

    private val photo = PhotoEntity(1, 12, "file")

    @Before
    fun setUp() {
        `when`(photoDao.getAllByAlbumId(anyLong())).thenReturn(Single.just(listOf(photo)))
    }

    @Test
    fun success() {
        val result = getPhotosUseCase.execute(123).test()

        result.assertNoErrors()
                .assertValue { l ->
                    l.size == 1 &&
                            l.first().id == photo.id && l.first().file == photo.file
                }
    }
}