package ru.domclick.valuation.authorization.ui.viewmodel

import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.Errors
import ru.domclick.core.RxScheduleRule
import ru.domclick.core.argAny
import ru.domclick.core.extentions.empty
import ru.domclick.valuation.authorization.data.dto.AuthorizeResponseDto
import ru.domclick.valuation.authorization.data.dto.AuthorizeResultDto
import ru.domclick.valuation.authorization.data.service.AuthorizeService
import ru.domclick.valuation.authorization.domain.AuthorizeUseCase
import java.io.IOException


class LoginViewModelTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    val schedulerRule = RxScheduleRule()

    @Mock
    private lateinit var authorizeService: AuthorizeService

    private lateinit var authorizeUseCase: AuthorizeUseCase
    private lateinit var loginViewModel: LoginViewModel

    private val successResult = AuthorizeResponseDto().apply {
        result = AuthorizeResultDto(1L, String.empty(), String.empty(), String.empty(), true, 1L)
    }

    @Before
    fun setUp() {
        authorizeUseCase = AuthorizeUseCase(authorizeService)
        loginViewModel = LoginViewModel(authorizeUseCase)
    }

    @Test
    fun authorized() {
        `when`(authorizeService.authorize(argAny(), argAny())).then { Single.just(successResult) }

        val isLoading = loginViewModel.isLoading.test()
        val onAuthorized = loginViewModel.onAuthorized.test()
        val onLoginFailed = loginViewModel.onLoginFailed.test()
        val onError = loginViewModel.onError.test()

        loginViewModel.authorize(String.empty(), String.empty())

        isLoading
                .assertValues(false, true, false)
                .assertNoErrors()
        onAuthorized
                .assertValueCount(1)
                .assertNoErrors()
        onLoginFailed
                .assertEmpty()
                .assertNoErrors()
        onError
                .assertEmpty()
                .assertNoErrors()
    }

    @Test
    fun loginFailed() {
        `when`(authorizeService.authorize(argAny(), argAny())).then { Single.error<Any>(Errors.createHttpException(401, String.empty())) }

        val isLoading = loginViewModel.isLoading.test()
        val onAuthorized = loginViewModel.onAuthorized.test()
        val onLoginFailed = loginViewModel.onLoginFailed.test()
        val onError = loginViewModel.onError.test()

        loginViewModel.authorize(String.empty(), String.empty())

        isLoading
                .assertValues(false, true, false)
                .assertNoErrors()
        onAuthorized
                .assertNoValues()
                .assertNoErrors()
        onLoginFailed
                .assertValueCount(1)
                .assertNoErrors()
        onError
                .assertEmpty()
                .assertNoErrors()
    }

    @Test
    fun internetConnectionFailed() {
        `when`(authorizeService.authorize(argAny(), argAny())).then { Single.error<Any>(IOException()) }

        val isLoading = loginViewModel.isLoading.test()
        val onAuthorized = loginViewModel.onAuthorized.test()
        val onLoginFailed = loginViewModel.onLoginFailed.test()
        val onError = loginViewModel.onError.test()

        loginViewModel.authorize(String.empty(), String.empty())

        isLoading
                .assertValues(false, true, false)
                .assertNoErrors()
        onAuthorized
                .assertNoValues()
                .assertNoErrors()
        onLoginFailed
                .assertNoValues()
                .assertNoErrors()
        onError
                .assertValueCount(1)
                .assertNoErrors()
    }
}