package ru.domclick.valuation.authorization.data.dto

import org.junit.Assert.*
import org.junit.Test
import ru.domclick.core.utils.GsonFactory

class AuthorizeResponseDtoTest {

    private val gson = GsonFactory.get()

    @Test
    fun parseSuccess() {
        val model = gson.fromJson(jsonSuccess, AuthorizeResponseDto::class.java)
        assertEquals(501889L, model.result!!.casId)
        assertTrue(model.success)
        assertTrue(model.errors.isEmpty())
    }

    @Test
    fun parseError() {
        val model = gson.fromJson(jsonError, AuthorizeResponseDto::class.java)
        assertNull(model.result)
        assertFalse(model.success)
        assertFalse(model.errors.isEmpty())
        assertEquals("Unauthorized", model.errors.first().message)
    }

    private val jsonSuccess = """
        {
            "result": {
                "phone": null,
                "username": "9000000003",
                "company": null,
                "position": null,
                "data": {
                    "cas_id": 501889
                },
                "email": "bezdna_novokuzneck@sberbank.ru",
                "cas_id": 501889,
                "rotting_password": false,
                "sro": null,
                "synced": false,
                "user": null,
                "description": null,
                "roles": [
                    "external_admin"
                ],
                "last_name": "Яковлевич",
                "id": 151,
                "active": true,
                "middle_name": "Попыткадва",
                "seniority": "3",
                "contract": "ыфвыфв",
                "first_name": "СТестовымУкэп",
                "education": "уйцвы",
                "backend": "cas",
                "insurance": null
            },
            "errors": [],
            "success": true
        }
        """

    private val jsonError = """
        {"result": null, "success": false, "errors": [{"message": "Unauthorized", "code": ""}]}
    """
}

