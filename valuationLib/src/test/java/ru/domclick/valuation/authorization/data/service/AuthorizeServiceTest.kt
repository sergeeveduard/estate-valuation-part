package ru.domclick.valuation.authorization.data.service

import io.reactivex.Single
import okhttp3.Headers
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnit
import retrofit2.HttpException
import retrofit2.Response
import ru.domclick.core.RxScheduleRule
import ru.domclick.core.argAny
import ru.domclick.core.extentions.empty
import ru.domclick.valuation.authorization.data.api.AuthApi
import ru.domclick.valuation.authorization.data.dto.AuthorizeResponseDto
import ru.domclick.valuation.authorization.data.dto.AuthorizeResultDto
import ru.domclick.valuation.common.analytic.Analytics
import ru.domclick.valuation.common.data.repo.AccountRepo
import java.io.IOException

/**
 * [AuthorizeService]
 */
class AuthorizeServiceTest {
    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    val schedulerRule = RxScheduleRule()

    @Mock
    private lateinit var api: AuthApi

    @Mock
    private lateinit var accountRepo: AccountRepo

    @Mock
    private lateinit var analytics: Analytics

    @InjectMocks
    private lateinit var service: AuthorizeService

    private val sessionId = "67c05d08ca7940c8a26f924ce3276abd"

    private val successResult = Response.success(
            AuthorizeResponseDto().apply {
                result = AuthorizeResultDto(1L, String.empty(), String.empty(), String.empty(), true, 1L)
            },
            Headers.Builder()
                    .add("Server: nginx/1.13.12" +
                            "Date: Mon, 23 Jul 2018 16:37:23 GMT" +
                            "Content-Type: application/json" +
                            "Transfer-Encoding: chunked" +
                            "Vary: Accept-Encoding" +
                            "Access-Control-Allow-Credentials: true" +
                            "Access-Control-Allow-Origin: http://parline.lc" +
                            "Set-Cookie: session_id=$sessionId; Path=/")
                    .build()
    )

    @Test
    fun authorized() {
        Mockito.`when`(api.authorize(argAny())).then { Single.just(successResult) }

        service.authorize("login", "pwd").test().await()

        Mockito.verify(accountRepo).putAccount("login", "pwd", sessionId)
    }

    @Test
    fun fail() {
        Mockito.`when`(api.authorize(argAny())).then { Single.error<Any>(IOException()) }

        val result = service.authorize("login", "pwd").test()

        result.assertNoValues()
                .assertError(IOException::class.java)

        Mockito.verify(accountRepo, Mockito.never()).putAccount(argAny(), argAny(), argAny())
    }

    @Test
    fun httpException() {
        Mockito.`when`(api.authorize(argAny())).then {
            Single.just<Response<AuthorizeResponseDto>>(Response.error(401, ResponseBody.create(MediaType.parse("application/json"), String.empty())))
        }

        val result = service.authorize("login", "pwd").test()

        result.assertNoValues()
                .assertError(HttpException::class.java)

        Mockito.verify(accountRepo, Mockito.never()).putAccount(argAny(), argAny(), argAny())
    }
}