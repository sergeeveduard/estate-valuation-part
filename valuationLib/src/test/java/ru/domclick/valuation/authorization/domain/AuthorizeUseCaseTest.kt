package ru.domclick.valuation.authorization.domain

import io.reactivex.Single
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.RxScheduleRule
import ru.domclick.core.argAny
import ru.domclick.core.extentions.empty
import ru.domclick.valuation.authorization.data.dto.AuthorizeResponseDto
import ru.domclick.valuation.authorization.data.dto.AuthorizeResultDto
import ru.domclick.valuation.authorization.data.service.AuthorizeService
import java.io.IOException

class AuthorizeUseCaseTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    val schedulerRule = RxScheduleRule()

    @Mock
    private lateinit var authorizeService: AuthorizeService


    private val successResult = AuthorizeResponseDto().apply {
        result = AuthorizeResultDto(1L, String.empty(), String.empty(), String.empty(), true, 1L)
    }

    @Test
    fun authorized() {
        `when`(authorizeService.authorize(argAny(), argAny())).then { Single.just(successResult) }

        val useCase = AuthorizeUseCase(authorizeService)
        val result = useCase.execute("login", "pwd").test()

        result.assertValueCount(1)
                .assertNoErrors()
    }

    @Test
    fun fail() {
        `when`(authorizeService.authorize(argAny(), argAny())).then { Single.error<Any>(IOException()) }

        val useCase = AuthorizeUseCase(authorizeService)
        val result = useCase.execute("login", "pwd").test()

        result.assertNoValues()
                .assertError(IOException::class.java)
    }
}