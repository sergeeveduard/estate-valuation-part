package ru.domclick.valuation.authorization.data.service

import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import ru.domclick.accountmanager.DomClickAccount
import ru.domclick.core.Errors
import ru.domclick.core.RxScheduleRule
import ru.domclick.core.argAny
import ru.domclick.core.extentions.empty
import ru.domclick.valuation.authorization.data.dto.AuthorizeResponseDto
import ru.domclick.valuation.authorization.data.dto.AuthorizeResultDto
import ru.domclick.valuation.common.data.repo.AccountRepo
import java.io.IOException

class RefreshSessionServiceTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    val schedulerRule = RxScheduleRule()

    @Mock
    private lateinit var authorizeService: AuthorizeService

    @Mock
    private lateinit var accountRepo: AccountRepo

    private lateinit var refreshSessionService: RefreshSessionService


    private val successResult = AuthorizeResponseDto().apply {
        result = AuthorizeResultDto(1L, String.empty(), String.empty(), String.empty(), true, 1L)
    }

    @Before
    fun setUp() {
        `when`(accountRepo.getAccount()).thenReturn(DomClickAccount("login", "pwd", "session"))
        refreshSessionService = RefreshSessionService(authorizeService, accountRepo)
    }

    @Test
    fun authorized() {
        Mockito.`when`(authorizeService.authorize(argAny(), argAny())).then { Single.just(successResult) }

        val result = refreshSessionService.refresh().test()

        result.assertValue(true)
                .assertNoErrors()
    }

    @Test
    fun noAccount() {
        Mockito.`when`(authorizeService.authorize(argAny(), argAny())).then { Single.just(successResult) }
        `when`(accountRepo.getAccount()).thenReturn(null)

        val result = refreshSessionService.refresh().test()

        result.assertValue(false)
                .assertNoErrors()
    }

    @Test
    fun fail() {
        Mockito.`when`(authorizeService.authorize(argAny(), argAny())).then { Single.error<Any>(IOException()) }

        val result = refreshSessionService.refresh().test()

        result.assertNoValues()
                .assertError(IOException::class.java)
    }

    @Test
    fun authorization_impossible() {
        Mockito.`when`(authorizeService.authorize(argAny(), argAny())).then { Single.error<Any>(Errors.createHttpException(401, String.empty())) }

        val result = refreshSessionService.refresh().test()

        result.assertValue(false)
                .assertNoErrors()
    }
}