package ru.domclick.valuation.reference.data.repo

import io.reactivex.Single
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.argAny
import ru.domclick.valuation.common.data.dto.ReferenceDto
import ru.domclick.valuation.database.data.dao.ReferenceDao
import ru.domclick.valuation.database.data.entities.ReferenceEntity

/**
 * [PhotoCategoriesRepo]
 */
class PhotoCategoriesRepoTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()

    @Mock
    private lateinit var referenceDao: ReferenceDao

    @InjectMocks
    private lateinit var photoCategoriesRepo: PhotoCategoriesRepo

    @Test
    fun getHardcoded() {
        `when`(referenceDao.getAllByType(argAny())).thenReturn(Single.just(emptyList()))

        val categories = photoCategoriesRepo.getCategories().test()
        categories
                .assertValue(PhotoCategoriesRepo.hardcodedCategories)
                .assertNoErrors()
    }

    @Test
    fun getFromDb() {
        val entities = listOf(ReferenceEntity(1, null, "value", ReferenceEntity.Type.PHOTO_CATEGORY))
        `when`(referenceDao.getAllByType(argAny())).thenReturn(Single.just(entities))

        val categories = photoCategoriesRepo.getCategories().test()
        categories
                .assertValue(listOf(ReferenceDto(1, null, "value")))
                .assertNoErrors()
    }
}