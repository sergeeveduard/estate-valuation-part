package ru.domclick.valuation.reference.data.service

import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.RxScheduleRule
import ru.domclick.core.argAny
import ru.domclick.valuation.common.data.dto.ReferenceDto
import ru.domclick.valuation.common.data.repo.AppPrefs
import ru.domclick.valuation.database.data.dao.ReferenceDao
import ru.domclick.valuation.reference.data.api.CatalogApi
import ru.domclick.valuation.reference.data.dto.CatalogResponseDto
import ru.domclick.valuation.reference.data.dto.CatalogResultDto
import ru.domclick.valuation.reference.data.repo.PhotoCategoriesRepo
import ru.domclick.valuation.reference.data.repo.PhotoSubcategoriesRepo
import ru.domclick.valuation.reference.data.repo.ReferenceRepo
import java.io.IOException

/**
 * [RefreshCatalogService]
 */
class RefreshCatalogServiceTest {
    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()

    @Rule
    @JvmField
    val schedulerRule = RxScheduleRule()

    @Mock
    private lateinit var catalogApi: CatalogApi

    @Mock
    private lateinit var photoCategoriesRepo: PhotoCategoriesRepo

    @Mock
    private lateinit var photoSubcategoriesRepo: PhotoSubcategoriesRepo

    @Mock
    private lateinit var referenceRepo: ReferenceRepo

    @Mock
    private lateinit var referenceDao: ReferenceDao

    @Mock
    private lateinit var prefs: AppPrefs

    @InjectMocks
    private lateinit var refreshCatalogService: RefreshCatalogService

    private val response = CatalogResponseDto().apply {
        result = CatalogResultDto(
                listOf(ReferenceDto(1, null, "category")),
                listOf(ReferenceDto(1, 1, "subcategory")),
                emptyList(),
                emptyList(),
                emptyList(),
                emptyList()
        )
    }

    @Before
    fun setUp() {
        `when`(catalogApi.catalog()).thenReturn(Single.just(response))
        `when`(prefs.getCatalogLastUpdate()).thenReturn(System.currentTimeMillis() - RefreshCatalogService.REFRESH_RATE - 1)
        `when`(referenceDao.getCountByType(argAny())).thenReturn(Single.just(1))
    }

    @Test
    fun update_success() {
        val refresh = refreshCatalogService.refresh().test()

        refresh.assertComplete().assertNoErrors()

        verify(photoCategoriesRepo).setCategories(argAny())
        verify(photoSubcategoriesRepo).setSubcategories(argAny())
        verify(prefs).setCatalogLastUpdate(anyLong())
    }

    @Test
    fun update_notNeeded() {
        `when`(prefs.getCatalogLastUpdate()).thenReturn(System.currentTimeMillis())

        val refresh = refreshCatalogService.refresh().test()

        refresh.assertComplete().assertNoErrors()

        verify(photoCategoriesRepo, never()).setCategories(argAny())
        verify(photoSubcategoriesRepo, never()).setSubcategories(argAny())
        verify(prefs, never()).setCatalogLastUpdate(anyLong())
    }


    @Test
    fun updated_recently_byNoSubcategories() {
        `when`(prefs.getCatalogLastUpdate()).thenReturn(System.currentTimeMillis())
        `when`(referenceDao.getCountByType(argAny())).thenReturn(Single.just(0))


        val refresh = refreshCatalogService.refresh().test()

        refresh.assertComplete().assertNoErrors()

        verify(photoCategoriesRepo).setCategories(argAny())
        verify(photoSubcategoriesRepo).setSubcategories(argAny())
        verify(prefs).setCatalogLastUpdate(anyLong())
    }

    @Test
    fun ioException() {
        `when`(catalogApi.catalog()).thenReturn(Single.error(IOException()))

        val refresh = refreshCatalogService.refresh().test()

        refresh.assertNotComplete().assertError(IOException::class.java)

        verify(photoCategoriesRepo, never()).setCategories(argAny())
        verify(photoSubcategoriesRepo, never()).setSubcategories(argAny())
        verify(prefs, never()).setCatalogLastUpdate(anyLong())
    }
}