package ru.domclick.valuation.reference.data.repo

import io.reactivex.Single
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.argAny
import ru.domclick.core.exception.EmptyDataException
import ru.domclick.valuation.common.data.dto.ReferenceDto
import ru.domclick.valuation.database.data.dao.ReferenceDao
import ru.domclick.valuation.database.data.entities.ReferenceEntity

/**
 * [PhotoSubcategoriesRepo]
 */
class PhotoSubcategoriesRepoTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()

    @Mock
    private lateinit var referenceDao: ReferenceDao

    @InjectMocks
    private lateinit var photoSubcategoriesRepo: PhotoSubcategoriesRepo

    @Test
    fun emptyDataException() {
        `when`(referenceDao.getAllByTypeAndParentId(argAny(), anyInt())).thenReturn(Single.just(emptyList()))

        val categories = photoSubcategoriesRepo.getSubcategories(1).test()
        categories
                .assertError(EmptyDataException::class.java)
                .assertNoValues()
    }

    @Test
    fun getFromDb() {
        val parentId = 10
        val entities = listOf(ReferenceEntity(1, parentId, "value", ReferenceEntity.Type.PHOTO_CATEGORY))
        `when`(referenceDao.getAllByTypeAndParentId(argAny(), anyInt())).thenReturn(Single.just(entities))

        val categories = photoSubcategoriesRepo.getSubcategories(parentId).test()
        categories
                .assertValue(listOf(ReferenceDto(1, parentId, "value")))
                .assertNoErrors()
    }
}