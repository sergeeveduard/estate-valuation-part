package ru.domclick.valuation.reference.data.dto

import org.junit.Assert.assertEquals
import org.junit.Test
import ru.domclick.core.utils.GsonFactory

class ObjectKindResponseDtoTest {
    private val gson = GsonFactory.get()

    @Test
    fun parseSuccess() {
        val model = gson.fromJson(jsonSuccess, ObjectKindResponseDto::class.java)
        assertEquals(5, model.result!!.items.size)
        assertEquals(1, model.result!!.items[0].id)
        assertEquals("Квартира", model.result!!.items[0].name)
    }

    private val jsonSuccess = """
{
    "pagination": {
        "limit": 5,
        "offset": 0
    },
    "errors": [],
    "result": {
        "items": [
            {
                "name": "Квартира",
                "id": 1
            },
            {
                "name": "Доля",
                "id": 2
            },
            {
                "name": "Комната",
                "id": 3
            },
            {
                "name": "Свободный земельный участок",
                "id": 4
            },
            {
                "name": "Участок со строениями",
                "id": 5
            }
        ]
    },
    "success": true
}
    """
}