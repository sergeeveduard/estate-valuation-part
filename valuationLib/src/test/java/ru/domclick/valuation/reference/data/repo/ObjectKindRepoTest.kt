package ru.domclick.valuation.reference.data.repo

import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.RxScheduleRule
import ru.domclick.core.argAny
import ru.domclick.valuation.database.data.dao.ReferenceDao
import ru.domclick.valuation.database.data.entities.ReferenceEntity
import ru.domclick.valuation.reference.data.api.ObjectKindApi
import ru.domclick.valuation.reference.data.dto.ObjectKindDto
import ru.domclick.valuation.reference.data.dto.ObjectKindResponseDto
import ru.domclick.valuation.reference.data.dto.ObjectKindResultDto
import java.io.IOException
import java.util.concurrent.TimeUnit

/**
 * [ObjectKindRepo]
 */
class ObjectKindRepoTest {
    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    val schedulerRule = RxScheduleRule()

    @Mock
    private lateinit var objectKindApi: ObjectKindApi

    @Mock
    private lateinit var referenceDao: ReferenceDao

    private val delayScheduler = TestScheduler()
    private lateinit var objectKindRepo: ObjectKindRepo

    private val successResponse = ObjectKindResponseDto().apply {
        result = ObjectKindResultDto(listOf(ObjectKindDto(100, "kind")))
    }

    @Before
    fun setUp() {
        `when`(referenceDao.getAllByType(argAny())).thenReturn(Single.just(emptyList()))
        objectKindRepo = ObjectKindRepo(objectKindApi, referenceDao, delayScheduler)
    }

    @Test
    fun success() {
        `when`(objectKindApi.objectKindList()).thenReturn(Single.just(successResponse))

        val list = objectKindRepo.getList().test()

        list.assertValue(successResponse.result!!.items)
                .assertNoErrors()
    }

    @Test
    fun timeout_getHardCoded() {
        `when`(objectKindApi.objectKindList()).thenReturn(
                Single.just(successResponse).delay(ObjectKindRepo.MAX_WAIT + 10)
        )

        val list = objectKindRepo.getList().test()

        delayScheduler.advanceTimeBy(ObjectKindRepo.MAX_WAIT + 1, TimeUnit.SECONDS)

        list.assertValue(ObjectKindRepo.hardcodedList)
                .assertNoErrors()
    }

    @Test
    fun timeout_getFromDb() {
        `when`(objectKindApi.objectKindList()).thenReturn(
                Single.just(successResponse).delay(ObjectKindRepo.MAX_WAIT + 10)
        )
        val dbItems = listOf(ReferenceEntity(123, null, "name", ReferenceEntity.Type.OBJECT_KIND))
        `when`(referenceDao.getAllByType(argAny())).thenReturn(Single.just(dbItems))

        val list = objectKindRepo.getList().test()

        delayScheduler.advanceTimeBy(ObjectKindRepo.MAX_WAIT + 1, TimeUnit.SECONDS)
        list
                .assertValue { l -> l.size == 1 && l[0].id == 123 && l[0].name == "name" }
                .assertNoErrors()
    }

    @Test
    fun ioError() {
        `when`(objectKindApi.objectKindList()).thenReturn(Single.error<ObjectKindResponseDto>(IOException()))

        val list = objectKindRepo.getList().test()

        list.assertNoValues()
                .assertError(IOException::class.java)
    }

    @Test
    fun delayed_ioError() {
        `when`(objectKindApi.objectKindList()).thenReturn(
                Single.error<ObjectKindResponseDto>(IOException()).delay(ObjectKindRepo.MAX_WAIT + 10)
        )

        val list = objectKindRepo.getList().test()

        delayScheduler.advanceTimeBy(ObjectKindRepo.MAX_WAIT + 1, TimeUnit.SECONDS)

        list.assertValue(ObjectKindRepo.hardcodedList)
                .assertNoErrors()
    }

    private fun Single<ObjectKindResponseDto>.delay(seconds: Long): Single<ObjectKindResponseDto> {
        return this.delay(seconds, TimeUnit.SECONDS, TestScheduler())
    }
}