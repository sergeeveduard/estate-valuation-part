package ru.domclick.valuation.reference.data.dto

import org.junit.Assert.assertEquals
import org.junit.Test
import ru.domclick.core.utils.GsonFactory
import ru.domclick.valuation.common.data.dto.ReferenceDto

class CatalogResponseDtoTest {
    private val gson = GsonFactory.get()

    @Test
    fun parseSuccess() {
        val model = gson.fromJson(jsonSuccess, CatalogResponseDto::class.java)

        assertEquals(1, model.result!!.photoCategories.size)
        assertEquals(ReferenceDto(1, null, "Дом снаружи"), model.result!!.photoCategories[0])
    }

    private val jsonSuccess = """
    {
        "errors": [],
        "result": {
            "photo_categories": [
                {
                    "name": "Дом снаружи",
                    "id": 1
                }
            ]
        },
        "success": true
    }
    """
}