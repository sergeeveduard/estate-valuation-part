package ru.domclick.valuation.sync.data.service

import io.reactivex.Completable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.ArgumentMatchers.eq
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.argAny
import ru.domclick.valuation.database.data.dao.CommentDao
import ru.domclick.valuation.database.data.entities.CommentEntity
import ru.domclick.valuation.sync.data.api.UploadCommentApi
import java.io.IOException

/**
 * [UploadCommentsService]
 */
class UploadCommentsServiceTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Mock
    private lateinit var commentDao: CommentDao

    @Mock
    private lateinit var uploadCommentApi: UploadCommentApi

    @InjectMocks
    private lateinit var uploadCommentsService: UploadCommentsService

    private val comment = CommentEntity(1L, "comment", 1000L)
    @Before
    fun setUp() {
        `when`(commentDao.getAllWithRemoteIdNotNull()).thenReturn(listOf(comment))
    }

    @Test
    fun success() {
        `when`(uploadCommentApi.uploadComment(anyLong(), argAny())).thenReturn(Completable.complete())

        uploadCommentsService.uploadAll()

        verify(commentDao).deleteByPhotoId(eq(comment.photoId))
    }

    @Test
    fun failure() {
        `when`(uploadCommentApi.uploadComment(anyLong(), argAny())).thenReturn(Completable.error(IOException()))

        uploadCommentsService.uploadAll()

        verify(commentDao, never()).deleteByPhotoId(eq(comment.photoId))
    }
}