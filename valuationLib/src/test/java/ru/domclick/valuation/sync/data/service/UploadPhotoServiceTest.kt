package ru.domclick.valuation.sync.data.service

import io.reactivex.Completable
import io.reactivex.Maybe
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.Spy
import org.mockito.junit.MockitoJUnit
import retrofit2.Call
import retrofit2.Response
import ru.domclick.core.argAny
import ru.domclick.valuation.common.analytic.Analytics
import ru.domclick.valuation.common.data.eventbus.UploadsEventBus
import ru.domclick.valuation.database.data.dao.PhotoDao
import ru.domclick.valuation.database.data.entities.PhotoEntity
import ru.domclick.valuation.database.data.models.PhotoInfo
import ru.domclick.valuation.database.data.models.SendStatus
import ru.domclick.valuation.orders.data.repo.OrderSendStatusRepo
import ru.domclick.valuation.photo.data.repo.CommentRepo
import ru.domclick.valuation.sync.data.api.UploadPhotoApi
import ru.domclick.valuation.sync.data.dto.UploadPhotoResponseDto
import ru.domclick.valuation.sync.data.dto.UploadPhotoResultDto

/**
 * [UploadPhotoService]
 */
class UploadPhotoServiceTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Mock
    private lateinit var orderSendStatusRepo: OrderSendStatusRepo

    @Mock
    private lateinit var commentRepo: CommentRepo

    @Mock
    private lateinit var photoDao: PhotoDao

    @Mock
    private lateinit var uploadPhotoApi: UploadPhotoApi

    @Mock
    private lateinit var call: Call<UploadPhotoResponseDto>

    @Mock
    private lateinit var analytics: Analytics


    @Spy
    private val uploadsEventBus = UploadsEventBus()

    @InjectMocks
    private lateinit var uploadPhotoService: UploadPhotoService

    private val photoInfo = PhotoInfo(
            photoId = 1,
            orderId = 2,
            categoryId = 3,
            subcategoryId = 4,
            file = "file",
            longitude = 0f,
            latitude = 1f,
            height = 3f
    )

    private val photoEntity = PhotoEntity(
            id = photoInfo.photoId,
            galleryId = 1,
            file = photoInfo.file,
            sendStatus = SendStatus.DEFAULT
    )

    private var counter = 0

    @Before
    fun setUp() {
        `when`(photoDao.getFirstPhotoByStatusAndOrderStatus(argAny(), argAny())).thenAnswer {
            if (0 == counter++) {
                photoInfo
            } else {
                null
            }
        }


        `when`(photoDao.getById(anyLong())).thenReturn(Maybe.just(photoEntity))
        `when`(orderSendStatusRepo.setSendStatus(anyLong(), argAny())).thenReturn(Completable.complete())

        `when`(call.execute()).thenReturn(Response.success(UploadPhotoResponseDto().apply {
            result = listOf(UploadPhotoResultDto(123))
            success = true
        }))

        `when`(uploadPhotoApi.upload(argAny(), anyLong(), anyInt(), anyInt(), anyFloat(), anyFloat(), anyFloat()))
                .thenReturn(call)

        `when`(commentRepo.setRemoteId(anyLong(), anyLong())).thenReturn(Completable.complete())
    }

    @Test
    fun success() {
        uploadPhotoService.uploadAll()

        verify(photoDao).updateStatus(anyLong(), argAny())
        verify(photoDao).update(argAny())

        verify(orderSendStatusRepo, never()).setSendStatus(anyLong(), argAny())
    }


    @Test
    fun error() {
        `when`(call.execute()).thenReturn(
                Response.error(
                        400,
                        ResponseBody.create(MediaType.parse("application/json"), "Content"
                        )
                )
        )

        uploadPhotoService.uploadAll()

        verify(photoDao, times(2)).updateStatus(anyLong(), argAny())
        verify(photoDao, never()).update(argAny())

        verify(orderSendStatusRepo).setSendStatus(anyLong(), argAny())
    }
}