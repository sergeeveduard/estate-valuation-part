package ru.domclick.valuation.common.data.converter

import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException
import retrofit2.HttpException
import retrofit2.Response
import ru.domclick.core.exception.HttpWithErrorMessageException
import ru.domclick.core.utils.GsonFactory
import ru.domclick.valuation.common.data.dto.ReferenceDto
import ru.domclick.valuation.common.data.dto.ResponseDto


/**
 * [ErrorResponseHandler]
 */
class ErrorResponseHandlerTest {

    @Rule
    @JvmField
    var thrown = ExpectedException.none()

    private val handler = ErrorResponseHandler(GsonFactory.get())
    private val errorMessage = "Unauthorized"
    private val jsonError = """
        {"result": null, "success": false, "errors": [{"message": "$errorMessage", "code": ""}]}
    """

    @Test
    fun noErrors() {
        val data = ResponseDto(
                ReferenceDto(1, null, "data"),
                true,
                emptyList()
        )
        handler.handle(Response.success(data))
    }

    @Test
    fun code400() {
        val responseBody = ResponseBody.create(MediaType.parse("application/json"), jsonError)
        val response = Response.error<Any>(400, responseBody)

        try {
            handler.handle(response)
        } catch (e: HttpWithErrorMessageException) {
            assertEquals(errorMessage, e.error)
        }
    }

    @Test(expected = HttpException::class)
    fun code5xx() {
        val responseBody = ResponseBody.create(MediaType.parse("application/json"), jsonError)
        val response = Response.error<Any>(500, responseBody)

        handler.handle(response)
    }
}