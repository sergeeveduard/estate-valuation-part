package ru.domclick.valuation.common.data.dto

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Test
import ru.domclick.core.utils.GsonFactory

class ErrorResponseDtoTest {

    private val gson = GsonFactory.get()

    @Test
    fun parseError() {
        val model = gson.fromJson(jsonError, ErrorResponseDto::class.java)
        assertFalse(model.errors.isEmpty())
        assertEquals("Unauthorized", model.errors.first().message)
    }

    private val jsonError = """
        {"result": null, "success": false, "errors": [{"message": "Unauthorized", "code": ""}]}
    """
}