package ru.domclick.valuation.startup

import android.content.Context
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnit
import ru.domclick.accountmanager.DomClickAccount
import ru.domclick.core.RxScheduleRule
import ru.domclick.valuation.authorization.data.service.RefreshSessionService
import ru.domclick.valuation.common.data.repo.AccountRepo
import java.io.IOException
import java.util.concurrent.TimeUnit

class StartupRouterTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    val schedulerRule = RxScheduleRule()

    @Mock
    private lateinit var accountRepo: AccountRepo

    @Mock
    private lateinit var context: Context

    @Mock
    private lateinit var refreshSessionService: RefreshSessionService

    private val timerScheduler = TestScheduler()
    private lateinit var startupRouter: StartupRouter

    @Before
    fun setUp() {
        `when`(accountRepo.getAccount()).thenReturn(DomClickAccount("login", "pwd", "session"))
        startupRouter = spy(StartupRouter(accountRepo, refreshSessionService, timerScheduler))
    }

    @Test
    fun refresh_success() {
        `when`(refreshSessionService.refresh()).thenReturn(Single.just(true))

        val result = startupRouter.getStartActivityIntent(context).test()

        result.assertNoErrors()

        verify(startupRouter).getOrderListIntent(context)
        verify(startupRouter, never()).getAuthIntent(context)
    }

    @Test
    fun noAccount() {
        `when`(accountRepo.getAccount()).thenReturn(null)
        `when`(refreshSessionService.refresh()).thenReturn(Single.just(true))

        val result = startupRouter.getStartActivityIntent(context).test()

        result.assertNoErrors()

        verify(startupRouter, never()).getOrderListIntent(context)
        verify(startupRouter).getAuthIntent(context)
    }

    @Test
    fun hasAccount_noInternet() {
        `when`(refreshSessionService.refresh()).thenReturn(Single.error(IOException()))

        val result = startupRouter.getStartActivityIntent(context).test()

        result.assertNoErrors()

        verify(startupRouter).getOrderListIntent(context)
        verify(startupRouter, never()).getAuthIntent(context)
    }

    @Test
    fun hasAccount_butRefreshReturnFalse() {
        `when`(refreshSessionService.refresh()).thenReturn(Single.just(false))

        val result = startupRouter.getStartActivityIntent(context).test()

        result.assertNoErrors()

        verify(startupRouter, never()).getOrderListIntent(context)
        verify(startupRouter).getAuthIntent(context)
    }

    @Test
    fun hasAccount_refreshImpossible_butTimeout() {
        val pingTime = StartupRouter.MAX_WAIT + 1

        `when`(refreshSessionService.refresh()).thenReturn(Single.just(false).delay(pingTime, TimeUnit.SECONDS, TestScheduler()))

        val result = startupRouter.getStartActivityIntent(context).test()

        timerScheduler.advanceTimeBy(pingTime, TimeUnit.SECONDS)

        result.assertNoErrors()

        verify(startupRouter).getOrderListIntent(context)
        verify(startupRouter, never()).getAuthIntent(context)
    }
}