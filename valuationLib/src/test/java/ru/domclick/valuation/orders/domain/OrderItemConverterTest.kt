package ru.domclick.valuation.orders.domain

import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.RxScheduleRule
import ru.domclick.core.extentions.empty
import ru.domclick.valuation.database.data.models.SendStatus
import ru.domclick.valuation.orders.data.dto.OrderItemDto
import ru.domclick.valuation.orders.domain.model.OrderItem
import ru.domclick.valuation.reference.data.repo.ObjectKindRepo
import java.io.IOException

/**
 * [OrderItemConverter]
 */
class OrderItemConverterTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    val schedulerRule = RxScheduleRule()

    @Mock
    private lateinit var objectKindRepo: ObjectKindRepo

    private lateinit var orderItemConverter: OrderItemConverter
    private lateinit var dtos: List<OrderItemDto>
    private lateinit var converted: List<OrderItem>

    @Before
    fun setUp() {
        orderItemConverter = OrderItemConverter(objectKindRepo)

        val kind = ObjectKindRepo.hardcodedList[0]
        dtos = listOf(
                OrderItemDto(1, kind.id, "address", "FullName", "phoneNum"),
                OrderItemDto(2, -100, "address", null, null),
                OrderItemDto(3, null, "address", null, null)
        )
        converted = listOf(
                OrderItem(1, kind.name, "address", "FullName", "phoneNum", SendStatus.DEFAULT),
                OrderItem(2, null, "address", String.empty(), String.empty(), SendStatus.DEFAULT),
                OrderItem(3, null, "address", String.empty(), String.empty(), SendStatus.DEFAULT)
        )
    }

    @Test
    fun success() {
        `when`(objectKindRepo.getList()).thenReturn(Single.just(ObjectKindRepo.hardcodedList))
        val result = orderItemConverter.convert(dtos).test()

        result.assertValueCount(1)
                .assertValue(converted)
                .assertNoErrors()
    }

    @Test
    fun oiException() {
        `when`(objectKindRepo.getList()).thenReturn(Single.error(IOException()))
        val result = orderItemConverter.convert(dtos).test()

        result.assertNoValues()
                .assertError(IOException::class.java)
    }
}