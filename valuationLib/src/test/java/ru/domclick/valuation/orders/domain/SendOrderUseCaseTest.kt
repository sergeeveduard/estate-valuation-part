package ru.domclick.valuation.orders.domain

import io.reactivex.Completable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.argAny
import ru.domclick.valuation.database.data.dao.PhotoDao
import ru.domclick.valuation.orders.data.repo.OrderSendStatusRepo
import ru.domclick.valuation.questionnarie.data.model.Questionnaire
import ru.domclick.valuation.questionnarie.data.repo.QuestionnaireRepo

/**
 * [SendOrderUseCase]
 */
class SendOrderUseCaseTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Mock
    private lateinit var orderSendStatusRepo: OrderSendStatusRepo

    @Mock
    private lateinit var questionnaireRepo: QuestionnaireRepo

    @Mock
    private lateinit var photoDao: PhotoDao

    @Mock
    private lateinit var questionnaire: Questionnaire

    @InjectMocks
    private lateinit var sendOrderUseCase: SendOrderUseCase

    @Before
    fun setUp() {
        `when`(orderSendStatusRepo.setSendStatus(anyLong(), argAny())).thenReturn(Completable.complete())

        `when`(questionnaireRepo.put(anyLong(), argAny())).thenReturn(Completable.complete())
    }

    @Test
    fun execute() {

        val complete = sendOrderUseCase.execute(orderId = 1, questionnaire = questionnaire).test()

        complete.assertComplete()
        verify(photoDao).updateStatusWhereOrderIdAndCurrentStatus(anyLong(), argAny(), argAny())
    }
}