package ru.domclick.valuation.orders.domain

import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.argAny
import ru.domclick.valuation.database.data.dao.PhotoDao
import ru.domclick.valuation.database.data.entities.OrderStatusEntity
import ru.domclick.valuation.database.data.models.OrderIdWithPhotoCount
import ru.domclick.valuation.database.data.models.SendStatus
import ru.domclick.valuation.orders.data.repo.OrderSendStatusRepo
import ru.domclick.valuation.orders.domain.model.OrderItem

/**
 * [GetActualOrdersUseCase]
 */
class GetActualOrdersUseCaseTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Mock
    private lateinit var photoDao: PhotoDao

    @Mock
    private lateinit var orderSendStatusRepo: OrderSendStatusRepo

    @InjectMocks
    private lateinit var getActualOrdersUseCase: GetActualOrdersUseCase

    private var orderId = 0L
    private val originOrders = listOf(
            OrderItem(orderId++, "type", "addr", "contact", "number", SendStatus.DEFAULT, 0, 0),
            OrderItem(orderId++, "type", "addr", "contact", "number", SendStatus.DEFAULT, 0, 0),
            OrderItem(orderId++, "type", "addr", "contact", "number", SendStatus.DEFAULT, 0, 0)
    )
    private val orderStatuses = listOf(
            OrderStatusEntity(originOrders[0].id, SendStatus.SENT),
            OrderStatusEntity(originOrders[1].id, SendStatus.SENDING),
            OrderStatusEntity(originOrders[2].id, SendStatus.ERROR)
    )

    private val totalPhotosPerOrder = 10

    private val totalPhotos = listOf(
            OrderIdWithPhotoCount(originOrders[0].id, totalPhotosPerOrder),
            OrderIdWithPhotoCount(originOrders[1].id, totalPhotosPerOrder),
            OrderIdWithPhotoCount(originOrders[2].id, totalPhotosPerOrder)
    )
    private val sentPhotos = listOf(
            OrderIdWithPhotoCount(originOrders[0].id, totalPhotosPerOrder),
            OrderIdWithPhotoCount(originOrders[1].id, totalPhotosPerOrder - 1)
    )


    private val expectedOrders = listOf(
            originOrders[0].copy(
                    totalPhotosCount = totalPhotos[0].photoCount,
                    sentPhotosCount = sentPhotos[0].photoCount,
                    sendStatus = orderStatuses[0].sendStatus
            ),
            originOrders[1].copy(
                    totalPhotosCount = totalPhotos[1].photoCount,
                    sentPhotosCount = sentPhotos[1].photoCount,
                    sendStatus = orderStatuses[1].sendStatus
            ),
            originOrders[2].copy(
                    totalPhotosCount = totalPhotos[2].photoCount,
                    sentPhotosCount = 0,
                    sendStatus = orderStatuses[2].sendStatus
            )
    )

    @Before
    fun setUp() {
        `when`(photoDao.getPhotoCountByStatus(argAny())).thenAnswer { invocation ->
            if (invocation.arguments[0] == SendStatus.SENT) {
                Single.just(sentPhotos)
            } else {
                Single.just(totalPhotos)
            }
        }

        `when`(orderSendStatusRepo.getAllSentAndSendingAndError()).thenReturn(Single.just(orderStatuses))
    }

    @Test
    fun success() {
        val result = getActualOrdersUseCase.execute(originOrders).test()

        result.assertValue(expectedOrders)
    }
}