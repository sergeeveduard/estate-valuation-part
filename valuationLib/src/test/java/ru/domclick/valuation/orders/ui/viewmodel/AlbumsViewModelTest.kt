package ru.domclick.valuation.orders.ui.viewmodel

import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.RxScheduleRule
import ru.domclick.core.exception.EmptyDataException
import ru.domclick.valuation.database.data.dao.GalleryDao
import ru.domclick.valuation.database.data.entities.GalleryEntity
import ru.domclick.valuation.orders.domain.CreateAlbumUseCase
import ru.domclick.valuation.orders.domain.GetAlbumListUseCase
import ru.domclick.valuation.orders.domain.model.Album

/**
 * [AlbumsViewModel]
 */
class AlbumsViewModelTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    val schedulerRule = RxScheduleRule()

    @Mock
    private lateinit var getAlbumListUseCase: GetAlbumListUseCase

    @Mock
    private lateinit var createAlbumUseCase: CreateAlbumUseCase

    @Mock
    private lateinit var galleryDao: GalleryDao

    @InjectMocks
    private lateinit var albumsViewModel: AlbumsViewModel

    private val orderId = 1L
    private val categoryId = 1
    private val title = "title"
    private val albumId = 10L
    private val photoCount = 102
    private val galleryEntity = GalleryEntity(100, albumId, 123, "subcategory")

    @Before
    fun setUp() {
        `when`(createAlbumUseCase.execute(anyLong(), anyInt(), anyString()))
                .thenReturn(Single.just(albumId))

        `when`(galleryDao.getAllByAlbumId(anyLong())).thenReturn(Single.just(listOf(galleryEntity)))
    }

    @Test
    fun onAlbumClicked_openAlbum() {
        val onError = albumsViewModel.onError.test()
        val onAlbumLoaded = albumsViewModel.onAlbumLoaded.test()
        val onOpenAlbum = albumsViewModel.onOpenAlbum.test()
        val onOpenCamera = albumsViewModel.onOpenCamera.test()

        val id = albumId
        albumsViewModel.onAlbumClicked(orderId, Album(id, categoryId, title, null, photoCount))

        onError.assertNoValues()
                .assertNoErrors()

        onAlbumLoaded.assertNoValues()
                .assertNoErrors()

        onOpenAlbum.assertValue { it.id == id && it.title == title }
                .assertNoErrors()

        onOpenCamera.assertNoValues()
                .assertNoErrors()
    }

    @Test
    fun onAlbumClicked_openCamera() {
        val onError = albumsViewModel.onError.test()
        val onAlbumLoaded = albumsViewModel.onAlbumLoaded.test()
        val onOpenAlbum = albumsViewModel.onOpenAlbum.test()
        val onOpenCamera = albumsViewModel.onOpenCamera.test()

        val id = null
        albumsViewModel.onAlbumClicked(orderId, Album(id, categoryId, title, null, photoCount))

        onError.assertNoValues()
                .assertNoErrors()

        onAlbumLoaded.assertNoValues()
                .assertNoErrors()

        onOpenAlbum.assertNoValues()
                .assertNoErrors()

        onOpenCamera
                .assertValue {
                    it.currentGalleryId == galleryEntity.id && it.title == title
                }
                .assertNoErrors()
    }

    @Test
    fun onAlbumClicked_EmptyDataException() {

        `when`(createAlbumUseCase.execute(anyLong(), anyInt(), anyString())).thenReturn(Single.error(EmptyDataException()))

        val onError = albumsViewModel.onError.test()
        val onAlbumLoaded = albumsViewModel.onAlbumLoaded.test()
        val onOpenAlbum = albumsViewModel.onOpenAlbum.test()
        val onOpenCamera = albumsViewModel.onOpenCamera.test()

        val id = null
        albumsViewModel.onAlbumClicked(orderId, Album(id, categoryId, title, null, photoCount))

        onError.assertValue { it is EmptyDataException }
                .assertNoErrors()

        onAlbumLoaded.assertNoValues()
                .assertNoErrors()

        onOpenAlbum.assertNoValues()
                .assertNoErrors()

        onOpenCamera
                .assertNoValues()
                .assertNoErrors()
    }
}