package ru.domclick.valuation.orders.domain

import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyLong
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.RxScheduleRule
import ru.domclick.valuation.common.data.dto.ReferenceDto
import ru.domclick.valuation.database.data.dao.AlbumDao
import ru.domclick.valuation.database.data.dao.GalleryDao
import ru.domclick.valuation.database.data.entities.AlbumEntity
import ru.domclick.valuation.database.data.entities.GalleryEntity
import ru.domclick.valuation.database.data.models.CountedGallery
import ru.domclick.valuation.reference.data.repo.PhotoCategoriesRepo

/**
 * [GetAlbumListUseCase]
 */
class GetAlbumListUseCaseTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    val schedulerRule = RxScheduleRule()

    @Mock
    private lateinit var photoCategoriesRepo: PhotoCategoriesRepo

    @Mock
    private lateinit var albumDao: AlbumDao

    @Mock
    private lateinit var galleryDao: GalleryDao

    @InjectMocks
    private lateinit var getAlbumListUseCase: GetAlbumListUseCase

    private val orderId = 11L
    private val category = ReferenceDto(1, null, "category")
    private val subcategory = ReferenceDto(2, 1, "subcategory")
    private val categoryList = listOf(category)
    private val album = AlbumEntity(2, orderId, category.id, category.name)
    private val gallery = GalleryEntity(3, album.id!!, subcategory.id, category.name)

    @Before
    fun setUp() {
        `when`(photoCategoriesRepo.getCategories()).thenReturn(Single.just(categoryList))
        `when`(albumDao.getAllByOrderId(anyLong())).thenReturn(Single.just(listOf(album)))
        `when`(albumDao.getAlbumPreviewsByOrderId(anyLong())).thenReturn(Single.just(emptyList()))
    }

    @Test
    fun noAlbums() {
        `when`(albumDao.getAllByOrderId(anyLong())).thenReturn(Single.just(emptyList()))
        `when`(galleryDao.getCountedByOrderId(anyLong())).thenReturn(Single.just(emptyList()))

        val albums = getAlbumListUseCase.getAlbums(orderId).test()

        albums
                .assertValue { l ->
                    l.size == categoryList.size &&
                            l.first().id == null &&
                            l.first().categoryId == category.id &&
                            l.first().photoCount == 0
                }
                .assertComplete()
    }

    @Test
    fun noGalleries() {
        `when`(galleryDao.getCountedByOrderId(anyLong())).thenReturn(Single.just(emptyList()))

        val albums = getAlbumListUseCase.getAlbums(orderId).test()

        albums
                .assertValue { l ->
                    l.size == categoryList.size &&
                            l.first().id == album.id &&
                            l.first().categoryId == category.id &&
                            l.first().photoCount == 0
                }
                .assertComplete()
    }

    @Test
    fun noPhotos() {
        `when`(galleryDao.getCountedByOrderId(anyLong())).thenReturn(Single.just(listOf(CountedGallery(0, gallery))))

        val albumCategories = getAlbumListUseCase.getAlbums(album.orderId).test()

        albumCategories
                .assertValue { l ->
                    l.size == categoryList.size &&
                            l.first().id == album.id &&
                            l.first().categoryId == category.id &&
                            l.first().photoCount == 0
                }
                .assertComplete()
    }
}