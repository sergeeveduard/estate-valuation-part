package ru.domclick.valuation.orders.domain

import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyInt
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.RxScheduleRule
import ru.domclick.core.argAny
import ru.domclick.core.exception.EmptyDataException
import ru.domclick.valuation.common.data.dto.ReferenceDto
import ru.domclick.valuation.database.data.dao.AlbumDao
import ru.domclick.valuation.database.data.dao.GalleryDao
import ru.domclick.valuation.database.data.entities.AlbumEntity
import ru.domclick.valuation.reference.data.repo.PhotoSubcategoriesRepo

/**
 * [CreateAlbumUseCase]
 */
class CreateAlbumUseCaseTest {
    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    val schedulerRule = RxScheduleRule()

    @Mock
    private lateinit var albumDao: AlbumDao

    @Mock
    private lateinit var galleryDao: GalleryDao

    @Mock
    private lateinit var photoSubcategoriesRepo: PhotoSubcategoriesRepo

    @InjectMocks
    private lateinit var createAlbumUseCase: CreateAlbumUseCase

    private val album = AlbumEntity(1, 0, 1, "title")

    @Before
    fun setUp() {
        `when`(albumDao.insert(argAny<AlbumEntity>())).thenReturn(album.id)
    }

    @Test
    fun success() {
        val subcategories = listOf(ReferenceDto(album.categoryId, 2, "subcat"))
        `when`(photoSubcategoriesRepo.getSubcategories(anyInt())).thenReturn(Single.just(subcategories))

        val albumId = createAlbumUseCase.execute(album.orderId, album.categoryId, album.title).test()

        albumId
                .assertValue(album.id)
                .assertNoErrors()
    }

    @Test
    fun whenSubcategoriesAreEmpty_getException() {
        `when`(photoSubcategoriesRepo.getSubcategories(anyInt())).thenReturn(Single.error(EmptyDataException()))

        val albumId = createAlbumUseCase.execute(album.orderId, album.categoryId, album.title).test()

        albumId
                .assertNoValues()
                .assertError(EmptyDataException::class.java)
    }
}