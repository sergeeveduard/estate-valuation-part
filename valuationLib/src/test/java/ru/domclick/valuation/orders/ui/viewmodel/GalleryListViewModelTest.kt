package ru.domclick.valuation.orders.ui.viewmodel

import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyLong
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.RxScheduleRule
import ru.domclick.valuation.orders.domain.GetGalleryListUseCase
import ru.domclick.valuation.orders.domain.model.GalleryItem
import ru.domclick.valuation.orders.domain.model.GalleryItemType
import ru.domclick.valuation.photo.data.repo.PhotoRepo

/**
 * [GalleryListViewModel]
 */
class GalleryListViewModelTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    val schedulerRule = RxScheduleRule()

    @Mock
    private lateinit var getGalleryListUseCase: GetGalleryListUseCase

    @Mock
    private lateinit var photoRepo: PhotoRepo

    @InjectMocks
    private lateinit var galleryListViewModel: GalleryListViewModel

    private val openCameraPreviewId: Long? = null
    private val galleryId = 2L
    private val galleryItemCaption = GalleryItem(1, galleryId, "title", GalleryItemType.CAPTION, null)
    private val galleryItemOpenCamera = GalleryItem(openCameraPreviewId, galleryId, "title", GalleryItemType.PREVIEW, null)
    private val galleryItemPreview = GalleryItem(1, galleryId, "title", GalleryItemType.PREVIEW, "file")
    private val isEditMode = false

    @Before
    fun setUp() {
        `when`(getGalleryListUseCase.execute(anyLong())).thenReturn(Single.just(listOf(
                galleryItemCaption,
                galleryItemOpenCamera,
                galleryItemPreview
        )))
    }

    @Test
    fun load_success() {
        val onError = galleryListViewModel.onError.test()
        val onSuccess = galleryListViewModel.onGalleriesLoaded.test()
        val openCamera = galleryListViewModel.openCamera.test()
        val openPreview = galleryListViewModel.openPreview.test()

        galleryListViewModel.loadGalleries(1, isEditMode)

        onError.assertNoValues().assertNoErrors()

        openCamera.assertNoValues().assertNoErrors()
        openPreview.assertNoValues().assertNoErrors()

        onSuccess.assertValue { l ->
            l.size == 3 && l.first().type == GalleryItemType.CAPTION
        }
    }

    @Test
    fun clickOnCaption_doNothing() {
        galleryListViewModel.loadGalleries(1, isEditMode)

        val onError = galleryListViewModel.onError.test()
        val onSuccess = galleryListViewModel.onGalleriesLoaded.test()
        val openCamera = galleryListViewModel.openCamera.test()
        val openPreview = galleryListViewModel.openPreview.test()
        val removeAt = galleryListViewModel.removeAt.test()

        galleryListViewModel.onItemClicked("title", galleryItemCaption)

        onError.assertNoValues().assertNoErrors()

        openCamera.assertNoValues().assertNoErrors()
        openPreview.assertNoValues().assertNoErrors()
        removeAt.assertNoValues()

        onSuccess.assertNoValues().assertNoErrors()
    }

    @Test
    fun clickOnPlus_openCamera() {
        galleryListViewModel.loadGalleries(1, isEditMode)

        val onError = galleryListViewModel.onError.test()
        val onSuccess = galleryListViewModel.onGalleriesLoaded.test()
        val openCamera = galleryListViewModel.openCamera.test()
        val openPreview = galleryListViewModel.openPreview.test()
        val removeAt = galleryListViewModel.removeAt.test()

        val title = "title1"
        galleryListViewModel.onItemClicked(title, galleryItemOpenCamera)

        onError.assertNoValues().assertNoErrors()

        openCamera
                .assertValue {
                    it.currentGalleryId == galleryId &&
                            it.title == title &&
                            it.galleries.size == 1
                }
                .assertNoErrors()
        openPreview.assertNoValues().assertNoErrors()
        removeAt.assertNoValues()

        onSuccess.assertNoValues().assertNoErrors()
    }

    @Test
    fun clickOnPlus_openPreview() {
        galleryListViewModel.loadGalleries(1, isEditMode)

        val onError = galleryListViewModel.onError.test()
        val onSuccess = galleryListViewModel.onGalleriesLoaded.test()
        val openCamera = galleryListViewModel.openCamera.test()
        val openPreview = galleryListViewModel.openPreview.test()
        val removeAt = galleryListViewModel.removeAt.test()

        val title = "title1"
        galleryListViewModel.onItemClicked(title, galleryItemPreview)

        onError.assertNoValues().assertNoErrors()

        openCamera.assertNoValues().assertNoErrors()
        openPreview
                .assertValue {
                    it == galleryItemPreview.id
                }
                .assertNoErrors()
        removeAt.assertNoValues()

        onSuccess.assertNoValues().assertNoErrors()
    }

    @Test
    fun clickOnPlus_openPreview_onEditMode() {
        galleryListViewModel.loadGalleries(1, isEditMode = true)

        val onError = galleryListViewModel.onError.test()
        val onSuccess = galleryListViewModel.onGalleriesLoaded.test()
        val openCamera = galleryListViewModel.openCamera.test()
        val openPreview = galleryListViewModel.openPreview.test()
        val removeAt = galleryListViewModel.removeAt.test()

        val title = "title1"
        galleryListViewModel.onItemClicked(title, galleryItemPreview)

        onError.assertNoValues().assertNoErrors()

        openCamera.assertNoValues().assertNoErrors()
        openPreview.assertNoValues().assertNoErrors()

        removeAt.assertValueCount(1)

        onSuccess.assertNoValues().assertNoErrors()
    }
}