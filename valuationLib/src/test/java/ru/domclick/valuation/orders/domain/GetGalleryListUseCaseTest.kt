package ru.domclick.valuation.orders.domain

import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyLong
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.RxScheduleRule
import ru.domclick.valuation.database.data.dao.GalleryDao
import ru.domclick.valuation.database.data.dao.PhotoDao
import ru.domclick.valuation.database.data.entities.GalleryEntity
import ru.domclick.valuation.database.data.entities.PhotoEntity
import ru.domclick.valuation.orders.domain.model.GalleryItemType

/**
 * [GetGalleryListUseCase]
 */
class GetGalleryListUseCaseTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    val schedulerRule = RxScheduleRule()

    @Mock
    private lateinit var galleryDao: GalleryDao

    @Mock
    private lateinit var photoDao: PhotoDao

    @InjectMocks
    private lateinit var getGalleryListUseCase: GetGalleryListUseCase

    private val albumId = 1L
    private val galleries = listOf(
            GalleryEntity(1L, albumId, 1, "title"),
            GalleryEntity(2L, albumId, 1, "title")
    )

    private val photos = listOf(
            PhotoEntity(1L, galleries[0].id!!, "file"),
            PhotoEntity(2L, galleries[0].id!!, "file"),

            PhotoEntity(3L, galleries[1].id!!, "file"),
            PhotoEntity(4L, galleries[1].id!!, "file")
    )

    @Before
    fun setUp() {
        `when`(galleryDao.getAllByAlbumId(anyLong())).thenReturn(Single.just(galleries))
        `when`(photoDao.getAllByAlbumId(anyLong())).thenReturn(Single.just(photos))
    }

    @Test
    fun success() {
        val list = getGalleryListUseCase.execute(albumId).test()

        list.assertNoErrors()
                .assertValue { l ->
                    l.size == galleries.size + galleries.size + photos.size &&
                            l.first().type == GalleryItemType.CAPTION &&
                            l[1].type == GalleryItemType.PREVIEW && l[1].id == null
                }
    }
}