package ru.domclick.valuation.orders.ui.viewmodel

import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyLong
import org.mockito.Spy
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.RxScheduleRule
import ru.domclick.core.argAny
import ru.domclick.core.extentions.empty
import ru.domclick.valuation.common.data.eventbus.UploadsEventBus
import ru.domclick.valuation.orders.domain.GetActualOrdersUseCase
import ru.domclick.valuation.orders.domain.GetOrderListUseCase
import ru.domclick.valuation.orders.domain.model.OrderItem
import ru.domclick.valuation.reference.data.service.RefreshCatalogService
import java.io.IOException

/**
 * [OrderListViewModel]
 */
class OrderListViewModelTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    val schedulerRule = RxScheduleRule()

    @Mock
    private lateinit var getOrderListUseCase: GetOrderListUseCase

    @Mock
    private lateinit var getActualOrdersUseCase: GetActualOrdersUseCase

    @Mock
    private lateinit var refreshCatalogService: RefreshCatalogService

    @Spy
    private val uploadsEventBus = UploadsEventBus()

    @InjectMocks
    private lateinit var orderListViewModel: OrderListViewModel

    private val orderItem = OrderItem(
            id = 1,
            address = "address",
            type = "flat",
            contactFullName = String.empty(),
            phoneNumber = String.empty()
    )

    @Before
    fun setUp() {
        `when`(refreshCatalogService.refresh()).thenReturn(Completable.complete())
        `when`(getActualOrdersUseCase.execute(argAny())).thenAnswer { invocation ->
            val list = invocation.arguments[0] as List<*>
            Single.just(list)
        }
    }

    @Test
    fun success() {
        val items = listOf(orderItem)
        `when`(getOrderListUseCase.execute(anyLong(), anyLong())).thenReturn(Single.just(items))
        `when`(refreshCatalogService.refresh()).thenReturn(Completable.error(IOException()))//must ignore error

        val isLoading = orderListViewModel.isLoading.test()
        val isRefreshing = orderListViewModel.isRetrying.test()
        val onError = orderListViewModel.onError.test()
        val onSuccess = orderListViewModel.onSuccess.test()

        orderListViewModel.onViewCreated()

        isLoading
                .assertValues(false, true, false)
                .assertNoErrors()

        isRefreshing
                .assertValues(false, false)
                .assertNoErrors()

        onError.assertNoValues()

        onSuccess.assertValue(items)
    }

    @Test
    fun ioException() {
        `when`(getOrderListUseCase.execute(anyLong(), anyLong())).thenReturn(Single.error(IOException()))

        val isLoading = orderListViewModel.isLoading.test()
        val isRefreshing = orderListViewModel.isRetrying.test()
        val onError = orderListViewModel.onError.test()
        val onSuccess = orderListViewModel.onSuccess.test()

        orderListViewModel.onViewCreated()

        isLoading
                .assertValues(false, true, false)
                .assertNoErrors()

        isRefreshing
                .assertValues(false, false)
                .assertNoErrors()

        onError.assertValueCount(1)

        onSuccess.assertNoValues()
    }

    @Test
    fun success_retry() {
        val items = listOf(orderItem)
        `when`(getOrderListUseCase.execute(anyLong(), anyLong())).thenReturn(Single.just(items))

        val isLoading = orderListViewModel.isLoading.test()
        val isRefreshing = orderListViewModel.isRetrying.test()
        val onError = orderListViewModel.onError.test()
        val onSuccess = orderListViewModel.onSuccess.test()

        orderListViewModel.onViewCreated()
        orderListViewModel.retry()

        isLoading
                .assertValues(false, true, false, false)
                .assertNoErrors()

        isRefreshing
                .assertValues(false, false, true, false)
                .assertNoErrors()

        onError.assertNoValues()

        onSuccess.assertValues(items, items)
    }

    @Test
    fun uploadEvents_success() {
        val items = listOf(orderItem)
        `when`(getOrderListUseCase.execute(anyLong(), anyLong())).thenReturn(Single.just(items))
        `when`(refreshCatalogService.refresh()).thenReturn(Completable.error(IOException()))//must ignore error

        val isLoading = orderListViewModel.isLoading.test()
        val isRefreshing = orderListViewModel.isRetrying.test()
        val onError = orderListViewModel.onError.test()
        val onSuccess = orderListViewModel.onSuccess.test()

        orderListViewModel.onViewCreated()

        uploadsEventBus.postOrderSentStatusChanged(1L)
        uploadsEventBus.postPhotoUploadedEvent(1L)

        isLoading
                .assertValues(false, true, false)
                .assertNoErrors()

        isRefreshing
                .assertValues(false, false)
                .assertNoErrors()

        onError.assertNoValues()

        onSuccess.assertValues(
                items, //onViewCreated
                items, //postOrderSentStatusChanged
                items  //postPhotoUploadedEvent
        )
    }
}