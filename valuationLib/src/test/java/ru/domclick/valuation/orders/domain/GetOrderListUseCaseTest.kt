package ru.domclick.valuation.orders.domain

import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnit
import ru.domclick.core.RxScheduleRule
import ru.domclick.core.argAny
import ru.domclick.valuation.database.data.models.SendStatus
import ru.domclick.valuation.orders.data.api.OrderApi
import ru.domclick.valuation.orders.data.dto.OrderListResponseDto
import ru.domclick.valuation.orders.data.dto.OrderListResultDto
import ru.domclick.valuation.orders.domain.model.OrderItem
import java.io.IOException

/**
 * [GetOrderListUseCase]
 */
class GetOrderListUseCaseTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    val schedulerRule = RxScheduleRule()

    @Mock
    private lateinit var orderApi: OrderApi

    @Mock
    private lateinit var orderItemConverter: OrderItemConverter

    @InjectMocks
    private lateinit var getOrderListUseCase: GetOrderListUseCase

    private val orderItem = OrderItem(1, "type", "address", "name", "phone", SendStatus.DEFAULT)
    @Before
    fun setUp() {
        `when`(orderApi.tasks(anyLong(), anyLong(), anyString(), anyString(), anyInt()))
                .thenReturn(Single.just(OrderListResponseDto().apply { result = OrderListResultDto(emptyList()) }))

        `when`(orderItemConverter.convert(argAny())).thenReturn(Single.just(listOf(orderItem)))
    }

    @Test
    fun success() {
        val orders = getOrderListUseCase.execute(0, 10).test()

        orders
                .assertValue { l -> l.size == 1 && l.first().id == orderItem.id }
                .assertNoErrors()
    }

    @Test
    fun apiFailed() {
        `when`(orderApi.tasks(anyLong(), anyLong(), anyString(), anyString(), anyInt()))
                .thenReturn(Single.error(IOException()))

        val orders = getOrderListUseCase.execute(0, 10).test()

        orders
                .assertNoValues()
                .assertError(IOException::class.java)
    }


    @Test
    fun converterFailed() {
        `when`(orderItemConverter.convert(argAny())).thenReturn(Single.error(NullPointerException()))


        val orders = getOrderListUseCase.execute(0, 10).test()

        orders
                .assertNoValues()
                .assertError(NullPointerException::class.java)
    }
}