package ru.domclick.core

import org.mockito.Mockito


fun <T> argAny(): T {
    Mockito.any<T>()
    return uninitialized()
}

private fun <T> uninitialized(): T = null as T