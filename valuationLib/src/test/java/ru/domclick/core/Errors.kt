package ru.domclick.core


import okhttp3.MediaType
import okhttp3.ResponseBody
import retrofit2.HttpException
import retrofit2.Response

object Errors {
    fun createHttpException(code: Int, content: String): HttpException {
        val responseBody = ResponseBody.create(MediaType.parse("application/json"), content)
        return HttpException(Response.error<Any>(code, responseBody))
    }
}