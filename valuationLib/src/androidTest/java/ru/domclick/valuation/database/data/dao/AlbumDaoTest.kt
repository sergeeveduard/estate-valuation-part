package ru.domclick.valuation.database.data.dao

import org.junit.Test
import ru.domclick.core.BaseDataBaseTest
import ru.domclick.valuation.database.data.entities.AlbumEntity
import ru.domclick.valuation.database.data.entities.GalleryEntity
import ru.domclick.valuation.database.data.entities.PhotoEntity

/**
 * [AlbumDao]
 */
class AlbumDaoTest : BaseDataBaseTest() {

    private lateinit var albumDao: AlbumDao
    private lateinit var galleryDao: GalleryDao
    private lateinit var photoDao: PhotoDao

    override fun setUp() {
        super.setUp()
        albumDao = db.getAlbumDao()
        galleryDao = db.getGalleryDao()
        photoDao = db.getPhotoDao()
    }

    @Test
    fun getAlbumPreviewsByOrderId() {
        val orderId1 = 1L
        val orderId2 = 2L
        var categoryOffset = 0
        var albumOffset = 0L
        var galleryOffset = 0L
        var photoOffset = 0L

        val albums = listOf(
                generateAlbum(orderId1, ++albumOffset, ++categoryOffset),
                generateAlbum(orderId1, ++albumOffset, ++categoryOffset),

                generateAlbum(orderId2, ++albumOffset, ++categoryOffset),
                generateAlbum(orderId2, ++albumOffset, ++categoryOffset)
        )

        val galleries = ArrayList<GalleryEntity>()
        albums.forEach {
            galleries.add(generateGallery(it, ++galleryOffset))
            galleries.add(generateGallery(it, ++galleryOffset))
        }

        val photos = ArrayList<PhotoEntity>()
        galleries.forEach {
            photos.add(generatePhoto(it, ++photoOffset))
            photos.add(generatePhoto(it, ++photoOffset))
        }

        albumDao.insert(albums)
        galleryDao.insert(galleries)
        photoDao.insert(photos)

        val previews1 = albumDao.getAlbumPreviewsByOrderId(orderId1).test()
        val previews2 = albumDao.getAlbumPreviewsByOrderId(orderId2).test()

        previews1
                .assertValue { l ->
                    l.size == 2 &&
                            l.find { it.photoId == 4L && it.albumId == 1L } != null &&
                            l.find { it.photoId == 8L && it.albumId == 2L } != null
                }
                .assertNoErrors()

        previews2
                .assertValue { l ->
                    l.size == 2 &&
                            l.find { it.photoId == 12L && it.albumId == 3L } != null &&
                            l.find { it.photoId == 16L && it.albumId == 4L } != null
                }
                .assertNoErrors()
    }

    private fun generateAlbum(orderId: Long, offsetId: Long, offsetCat: Int): AlbumEntity {
        return AlbumEntity(offsetId, orderId, offsetCat, "orderId $orderId; albumId $offsetId")
    }

    private fun generateGallery(album: AlbumEntity, offsetId: Long): GalleryEntity {
        return GalleryEntity(offsetId, album.id!!, 0, "${album.title}; galleryId $offsetId")
    }

    private fun generatePhoto(gallery: GalleryEntity, offsetId: Long): PhotoEntity {
        return PhotoEntity(offsetId, gallery.id!!, "${gallery.title}; photoId $offsetId")
    }
}