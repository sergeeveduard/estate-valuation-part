package ru.domclick.valuation.database.data.dao

import org.junit.Test
import ru.domclick.core.BaseDataBaseTest
import ru.domclick.core.extentions.empty
import ru.domclick.valuation.database.data.entities.AlbumEntity
import ru.domclick.valuation.database.data.entities.GalleryEntity
import ru.domclick.valuation.database.data.entities.PhotoEntity

/**
 * [GalleryDao]
 */
class GalleryDaoTest : BaseDataBaseTest() {

    private lateinit var albumDao: AlbumDao
    private lateinit var galleryDao: GalleryDao
    private lateinit var photoDao: PhotoDao

    private val orderId = 333L
    private val albumId = 222L
    private val categoryId = 111

    override fun setUp() {
        super.setUp()
        albumDao = db.getAlbumDao()
        galleryDao = db.getGalleryDao()
        photoDao = db.getPhotoDao()

        albumDao.insert(AlbumEntity(albumId, orderId, categoryId, "title"))
    }

    @Test
    fun empty() {
        val items = galleryDao.getCountedByOrderId(orderId).test()

        items
                .assertValue(emptyList())
                .assertComplete()
    }

    @Test
    fun hasAlbum_noPhotos() {
        val count = 0
        val galleryId = 2L
        val entity = GalleryEntity(galleryId, albumId, 3, "title")
        galleryDao.insert(entity)

        val items = galleryDao.getCountedByOrderId(orderId).test()
        items
                .assertValue { l ->
                    l.size == 1 &&
                            l.first().photoCount == count &&
                            l.first().gallery.id == entity.id &&
                            l.first().gallery.title == entity.title
                }
                .assertComplete()
    }

    @Test
    fun hasPhotos() {
        val file = String.empty()
        val count = 1
        val galleryId = 2L

        val gallery = GalleryEntity(galleryId, albumId, 3, "title")
        galleryDao.insert(gallery)
        photoDao.insert(PhotoEntity(null, galleryId, file))

        val items = galleryDao.getCountedByOrderId(orderId).test()
        items
                .assertValue { l ->
                    l.size == 1 && l.first().photoCount == count && l.first().gallery.id == gallery.id
                }
                .assertComplete()
    }
}