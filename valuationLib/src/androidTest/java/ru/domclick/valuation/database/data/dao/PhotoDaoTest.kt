package ru.domclick.valuation.database.data.dao

import org.junit.Test
import ru.domclick.core.BaseDataBaseTest
import ru.domclick.valuation.database.data.entities.AlbumEntity
import ru.domclick.valuation.database.data.entities.GalleryEntity
import ru.domclick.valuation.database.data.entities.PhotoEntity

/**
 * [PhotoDao]
 */
class PhotoDaoTest : BaseDataBaseTest() {

    private lateinit var albumDao: AlbumDao
    private lateinit var galleryDao: GalleryDao
    private lateinit var photoDao: PhotoDao

    private val album = AlbumEntity(1, 1, 1, "album")
    private val gallery1 = GalleryEntity(1, album.id!!, 1, "gallery1")
    private val gallery2 = GalleryEntity(2, album.id!!, 1, "gallery2")
    private var photoId = 0L
    private val photos = listOf(
            PhotoEntity(++photoId, gallery2.id!!, "file"),
            PhotoEntity(++photoId, gallery2.id!!, "file"),

            PhotoEntity(++photoId, gallery1.id!!, "file"),
            PhotoEntity(++photoId, gallery1.id!!, "file")
    )

    override fun setUp() {
        super.setUp()
        albumDao = db.getAlbumDao()
        galleryDao = db.getGalleryDao()
        photoDao = db.getPhotoDao()

        albumDao.insert(album)
        galleryDao.insert(gallery1)
        galleryDao.insert(gallery2)
        photoDao.insert(photos)
    }

    @Test
    fun descOrder() {
        val result = photoDao.getAllByAlbumId(album.id!!).test()

        result.assertNoErrors()
                .assertValue { l ->
                    l.size == photos.size &&
                            l[0].id == photos[3].id &&
                            l[1].id == photos[2].id &&
                            l[3].id == photos[0].id
                }
    }

    @Test
    fun getLastFromAlbum() {
        val result = photoDao.getLastByAlbumId(album.id!!).test()

        result.assertValue { entity -> entity.id == photos.last().id }
    }
}