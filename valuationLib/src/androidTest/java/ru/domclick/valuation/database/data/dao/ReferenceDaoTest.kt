package ru.domclick.valuation.database.data.dao

import org.junit.Test
import ru.domclick.core.BaseDataBaseTest
import ru.domclick.valuation.database.data.entities.ReferenceEntity

class ReferenceDaoTest : BaseDataBaseTest() {

    protected lateinit var referenceDao: ReferenceDao

    override fun setUp() {
        super.setUp()
        referenceDao = db.getReferenceDao()
    }

    @Test
    fun putAndGet() {
        val parentId = 1
        referenceDao.insert(listOf(ReferenceEntity(1, null, "kind1", ReferenceEntity.Type.OBJECT_KIND)))
        referenceDao.insert(listOf(ReferenceEntity(1, null, "kind2", ReferenceEntity.Type.OBJECT_KIND)))
        referenceDao.insert(listOf(ReferenceEntity(1, null, "category", ReferenceEntity.Type.PHOTO_CATEGORY)))
        referenceDao.insert(listOf(ReferenceEntity(2, parentId, "subcategory", ReferenceEntity.Type.PHOTO_SUBCATEGORY)))

        val kind = referenceDao.getAllByType(ReferenceEntity.Type.OBJECT_KIND).test()
        kind
                .assertValue { l -> l.size == 1 && l.first().name == "kind2" }
                .assertNoErrors()

        val category = referenceDao.getAllByType(ReferenceEntity.Type.PHOTO_CATEGORY).test()
        category
                .assertValue { l -> l.size == 1 && l.first().name == "category" }
                .assertNoErrors()

        val subcategory = referenceDao.getAllByTypeAndParentId(ReferenceEntity.Type.PHOTO_SUBCATEGORY, parentId).test()
        subcategory
                .assertValue { l -> l.size == 1 && l.first().name == "subcategory" }
                .assertNoErrors()
    }
}