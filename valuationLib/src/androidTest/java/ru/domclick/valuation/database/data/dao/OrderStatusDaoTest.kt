package ru.domclick.valuation.database.data.dao

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import ru.domclick.core.BaseDataBaseTest
import ru.domclick.valuation.database.data.entities.OrderStatusEntity
import ru.domclick.valuation.database.data.models.SendStatus

/**
 * [OrderStatusDao]
 */
class OrderStatusDaoTest : BaseDataBaseTest() {

    private lateinit var orderStatusDao: OrderStatusDao

    private val orderIdSending = 10L
    private val statusDefault = OrderStatusEntity(orderIdSending, SendStatus.DEFAULT)
    private val statusSending = OrderStatusEntity(orderIdSending, SendStatus.SENDING)
    private val statusError = OrderStatusEntity(11, SendStatus.ERROR)
    private val statusSent = OrderStatusEntity(12, SendStatus.SENT)

    @Before
    override fun setUp() {
        super.setUp()
        orderStatusDao = db.getOrderStatusDao()

        orderStatusDao.insert(statusDefault)
        orderStatusDao.insert(statusSending)
        orderStatusDao.insert(statusError)
        orderStatusDao.insert(statusSent)
    }

    @Test
    fun insertReplace() {

        val result = orderStatusDao.getByOrderId(orderIdSending).test()
        result
                .assertValue {
                    it.orderId == statusSending.orderId &&
                            it.sendStatus == statusSending.sendStatus &&
                            it.updated > 0
                }
                .assertNoErrors()
    }

    @Test
    fun getByStatus() {
        val result = orderStatusDao.getAllWithStatus(SendStatus.SENDING, SendStatus.ERROR).test()
        result
                .assertValue { l ->
                    l.size == 2 &&
                            l.find { it.sendStatus == SendStatus.SENDING } != null &&
                            l.find { it.sendStatus == SendStatus.ERROR } != null &&

                            l.find { it.sendStatus == SendStatus.DEFAULT } == null &&
                            l.find { it.sendStatus == SendStatus.SENT } == null
                }
                .assertNoErrors()
    }

    @Test
    fun deleteByStatus() {
        val count = orderStatusDao.deleteWithUpdatedBeforeAndStatusIn(
                System.currentTimeMillis(),
                SendStatus.ERROR,
                SendStatus.SENT
        )

        assertEquals(2, count)

        val items = orderStatusDao.getAllWithStatus(
                SendStatus.DEFAULT,
                SendStatus.SENDING,
                SendStatus.ERROR,
                SendStatus.SENT
        ).test()

        items.assertValue { l -> l.size == 1 && l.first().sendStatus == SendStatus.SENDING }
    }

    @Test
    fun getEmpty() {
        val empty = orderStatusDao.getByOrderId(2345).test()

        empty.assertNoErrors()
                .assertComplete()
                .assertNoErrors()
    }
}