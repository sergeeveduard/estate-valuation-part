package ru.domclick.valuation.questionnarie.data.repo

import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import ru.domclick.core.BaseDataBaseTest
import ru.domclick.core.utils.GsonFactory
import ru.domclick.valuation.database.data.dao.QuestionnaireDao
import ru.domclick.valuation.database.data.entities.QuestionnaireEntity
import ru.domclick.valuation.questionnarie.data.model.Questionnaire

/**
 * [QuestionnaireRepo]
 */
class QuestionnaireRepoTest : BaseDataBaseTest() {

    private val gson = GsonFactory.get()
    private lateinit var questionnaireDao: QuestionnaireDao
    private lateinit var questionnaireRepo: QuestionnaireRepo

    private val orderId = 12L
    private val questionnaire = Questionnaire(
            true,
            "anino",
            1,
            5,

            9, 22,

            mutableSetOf(1, 2),
            mutableSetOf(3, 4)
    )

    @Before
    override fun setUp() {
        super.setUp()

        questionnaireDao = db.getQuestionnaireDao()

        val json = gson.toJson(questionnaire)
        questionnaireDao.insert(QuestionnaireEntity(orderId, json)).blockingAwait()

        questionnaireRepo = QuestionnaireRepo(questionnaireDao, gson)
    }

    @Test
    fun get() {
        val q = questionnaireRepo.get(orderId).test()

        q.assertValue(questionnaire)
    }

    @Test
    fun put() {
        val newQ = Questionnaire(
                false,
                "new metro",
                3,
                7,

                3, 20,

                mutableSetOf(4, 5),
                mutableSetOf(6, 7)
        )
        questionnaireRepo.put(orderId, newQ).blockingAwait()

        val q = questionnaireRepo.get(orderId).test()

        q.assertValue(newQ)
    }

    @Test
    fun blockingGet() {
        val result = questionnaireRepo.get(-123L).blockingGet()
        assertNull(result)
    }
}