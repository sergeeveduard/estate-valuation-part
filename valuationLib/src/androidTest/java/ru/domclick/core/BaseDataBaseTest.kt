package ru.domclick.core

import androidx.room.Room
import androidx.test.InstrumentationRegistry
import org.junit.After
import org.junit.Before
import ru.domclick.valuation.database.data.DataBase

open class BaseDataBaseTest {
    protected lateinit var db: DataBase

    @Before
    open fun setUp() {
        val context = InstrumentationRegistry.getTargetContext()
        db = Room.inMemoryDatabaseBuilder(context, DataBase::class.java).build()
    }

    @After
    open fun tearDown() {
        db.close()
    }
}