package ru.domclick.valuation.common.data.repo

import ru.domclick.core.repo.HostRepo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HostRepoImpl
@Inject constructor(private val appPrefs: AppPrefs) : HostRepo {

    override var currentHost: String
        get() = appPrefs.getHost()
        set(value) {
            appPrefs.setHost(value)
        }

    override val hostList: List<Pair<String, String>>
        get() = listOf(
                Pair("prod", "https://eva.domclick.ru/"),
                Pair("qa", "https://qa-eva.domclick.ru/"),
                Pair("local", "http://localhost:23300/")
        )
}