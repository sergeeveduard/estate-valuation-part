package ru.domclick.valuation.common.data.repo

import ru.domclick.core.repo.HostRepo
import ru.domclick.valuation.lib.BuildConfig
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HostRepoImpl
@Inject constructor(private val appPrefs: AppPrefs) : HostRepo {

    override var currentHost: String = BuildConfig.PROD_API_BASE_URL

    override val hostList = emptyList<Pair<String, String>>()
}