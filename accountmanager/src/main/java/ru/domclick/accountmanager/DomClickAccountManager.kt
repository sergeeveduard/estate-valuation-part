package ru.domclick.accountmanager

import android.accounts.Account
import android.accounts.AccountManager
import android.content.Context
import android.os.Bundle

private const val USER_DATA_SESSION_ID = "user_data_session_id"

class DomClickAccountManager(private val context: Context) {

    private val accountType: String by lazy { context.getString(R.string.account_type) }

    fun getAccounts(): List<DomClickAccount> {
        val accountManager = AccountManager.get(context)
        return accountManager
                .getAccountsByType(accountType)
                .map { convertAccount(accountManager, it) }
    }

    fun putAccount(login: String, password: String, sessionId: String): Boolean {
        val accountManager = AccountManager.get(context)
        val existAccount = accountManager
                .getAccountsByType(accountType)
                .find { it.name == login }

        return if (existAccount != null) {
            accountManager.setPassword(existAccount, password)
            accountManager.setUserData(existAccount, USER_DATA_SESSION_ID, sessionId)
            true
        } else {
            val account = Account(login, accountType)
            val userData = Bundle().apply {
                putString(USER_DATA_SESSION_ID, sessionId)
            }

            accountManager.addAccountExplicitly(account, password, userData)
        }
    }

    private fun convertAccount(accountManager: AccountManager, account: Account): DomClickAccount {
        return DomClickAccount(
                account.name,
                accountManager.getPassword(account),
                accountManager.getUserData(account, USER_DATA_SESSION_ID)
        )
    }
}