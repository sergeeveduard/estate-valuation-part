package ru.domclick.accountmanager

import android.app.Service
import android.content.Intent
import android.os.IBinder


internal class AuthenticatorService : Service() {

    private var authenticator: Authenticator? = null

    override fun onCreate() {
        // Create a new authenticator object
        authenticator = Authenticator(this)
    }

    override fun onBind(intent: Intent): IBinder {
        return authenticator!!.iBinder
    }
}