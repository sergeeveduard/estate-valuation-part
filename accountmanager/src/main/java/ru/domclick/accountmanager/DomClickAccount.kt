package ru.domclick.accountmanager

class DomClickAccount(
        val login: String,
        val password: String,
        val sessionId: String
)