package ru.domclick.accountmanager

import android.accounts.*
import android.content.Context
import android.os.Bundle


internal class Authenticator(private val context: Context) : AbstractAccountAuthenticator(context) {
    @Throws(NetworkErrorException::class)
    override fun addAccount(
            response: AccountAuthenticatorResponse,
            accountType: String,
            authTokenType: String,
            requiredFeatures: Array<String>,
            options: Bundle): Bundle? {
        return null
    }

    @Throws(NetworkErrorException::class)
    override fun confirmCredentials(arg0: AccountAuthenticatorResponse,
                                    arg1: Account, arg2: Bundle): Bundle? {
        return null
    }

    override fun editProperties(arg0: AccountAuthenticatorResponse, arg1: String): Bundle? {
        return null
    }

    @Throws(NetworkErrorException::class)
    override fun getAuthToken(
            response: AccountAuthenticatorResponse,
            account: Account?,
            authTokenType: String,
            options: Bundle): Bundle? {
        // Extract the username and password from the Account Manager, and ask
        // the server for an appropriate AuthToken.
        val am = AccountManager.get(context)

        val authToken: String? = am.peekAuthToken(account, authTokenType)

        // If we get an authToken - we return it
        return if (authToken.isNullOrEmpty()) {
            null
        } else {
            val result = Bundle()
            result.putString(AccountManager.KEY_ACCOUNT_NAME, account!!.name)
            result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type)
            result.putString(AccountManager.KEY_AUTHTOKEN, authToken)
            return result
        }
    }

    override fun getAuthTokenLabel(arg0: String): String? {
        return null
    }

    @Throws(NetworkErrorException::class)
    override fun hasFeatures(arg0: AccountAuthenticatorResponse, arg1: Account,
                             arg2: Array<String>): Bundle? {
        return null
    }

    @Throws(NetworkErrorException::class)
    override fun updateCredentials(arg0: AccountAuthenticatorResponse,
                                   arg1: Account, arg2: String, arg3: Bundle): Bundle? {
        return null
    }

}