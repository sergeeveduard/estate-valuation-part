package ru.domclick.valuation.splash.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import ru.domclick.core.extentions.addTo
import ru.domclick.valuation.app.R
import ru.domclick.valuation.common.ui.ValuationApplication

class SplashActivity : AppCompatActivity() {
    private val lifeCycleDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        (application as ValuationApplication)
                .getStartActivityIntent(this)
                .subscribe { i -> startActivity(i) }
                .addTo(lifeCycleDisposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        lifeCycleDisposable.clear()
    }
}
